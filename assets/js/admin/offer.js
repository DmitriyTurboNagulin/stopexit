function preview() {
	var linkSave = $('#linkSave').serialize();
	var url = $('#showOfferLink').text();
	$.ajax({
		type: 'POST',
		url: url,
		async: false,
		data: ({data : linkSave}),
		success: function(data) {
			$('#show').html(data);
		}
	});
	$('#save-template').removeClass("btn-success").addClass("btn-primary");
	$('#save-template i').removeClass("fa-check").addClass("fa-floppy-o");
	var totalHeight = 0;
	$("#stopexit_checkout div").each(function(){
		totalHeight += $(this).outerHeight(true);
	});
	var maxHeight = $('input[name="offer_setting[offer_setting_height]"]').val();
	var diff = maxHeight - totalHeight
	if (diff <= 5) {
		$('.bootstrap-touchspin-up').each(function() {
			$(this).prop('disabled', true);
			if ($('input[name="offer_setting[offer_setting_logo]"]').val() == '') {
				$('input[name="offer_setting[offer_setting_logo]"]').prop('disabled', true);
			}
		});
	} else {
		$('.bootstrap-touchspin-up').each(function() {
			$(this).prop('disabled', false);
			$('input[name="offer_setting[offer_setting_logo]"]').prop('disabled', false);
		});
	}
}

preview();

$(document).on("change", "#width-sel input", function() {
	var k =$(this).val();
	var a = $(this).parent().find("span").text();
	$('#im-width').text(a);
	preview();
});


$(document).on("change", "#template-select", function() {
	var id = $('#template-select').val();
	var url = $('#showTemplateLink').text();
	$.ajax({
		type: 'POST',
		url: url,
		async: false,
		data: ({id : id}),
		success: function(data) {
			data = parse_str(decodeURIComponent(data));
			for(var key in data) {
				var val = data[key];
				$('select[name="' + key + '"]').val(val);
				$('input[name="' + key + '"]').val(val);
				$(' input[name="' + key + '"] ').parent(".input-colorpicker").colorpicker('setValue', val);
			}
			preview();

		}
	});
	return false;
});

$(".radio-show_all").change(function() {
	$(".urls textarea").val('');
	if ($(this).val() == 'optional') {
		$(".urls").show();
	} else {
		$(".urls").hide();
	}
});

$(document).on("click", "#deleteTemplate", function() {
	var send = $('.template-check:checked');
	var url = $('#deleteTemplateLink').text();
	ids = new Array();
	send.each(function(index) {
		ids[index] = $(this).val();
	});
	$.ajax({
		type: 'POST',
		url: url,
		async: false,
		data: ({ids : ids}),
		success: function(data) {
			ids.forEach(function(item, i, arr) {
				$('#templateModal [value="' + item + '"]').parent().remove();
				$('#template-select [value="' + item + '"]').remove();
			});
		}
	});
	return false;
});

$("#show").stick_in_parent();

$(document).on("click", "#checkDraft", function() {
	var data = $(this).data('data');
	if (data) {
		data = parse_str(decodeURIComponent(data));
		for(var key in data) {
			var val = data[key];
			$('input[name="' + key + '"]').val(val);
			$('select[name="' + key + '"]').val(val);
			$(' input[name="' + key + '"] ').parent(".input-colorpicker").colorpicker('setValue', val);
		}
	}
	save_offer();
	preview();
});

$(document).on("click", ".showDraft", function() {
	var data = $(this).data('data');
	$('.showDraft').removeClass('active');
	$(this).addClass('active');
	$('#showText').show();
	$('#checkDraft').data('data', data);
	var url = $('#showOfferLink').text();
	$.ajax({
		type: 'POST',
		url: url,
		async: false,
		data: ({data : data}),
		success: function(data) {
			$('#show2').html(data);
			$('#show2 .stopexit').css('position', 'relative');
		},
	});
	return false;
});

function show_image() {
	$("#spinner").show();
	var url = $('#showLink').text();
	$.ajax({
		type: "POST",
		async: false,
		url: url,
		success: function(data) {
			$("#imgList").html(data);
		},
		complete: function() {
			$("#spinner").hide();
		},
	});
}

function timeFormat(time) {
	return ('0' + (time)).slice(-2);
}

function save_offer() {
	$('.draft').hide();
	var id = $("input[name=offer_id]").val();
	var serialize = $('#linkSave').serialize();
	localStorage.setItem('offer' + id, serialize);
	var today = new Date();
	// timeFormat(today.getDate()) + '-' + timeFormat(today.getMonth() + 1) + '-' + today.getFullYear() + ' ' +
	$('.draft span').text(timeFormat(today.getHours()) + ':' + timeFormat(today.getMinutes()) + ':' + timeFormat(today.getSeconds()));
	$('.draft').fadeIn(300);
}

function fill_offer() {
	var id = $("input[name=offer_id]").val();
	var serialize = localStorage.getItem('offer' + id);
	if (serialize) {
		$("#linkSave").deserialize(serialize);
	}
}

function delete_offer() {
	var id = $("input[name=offer_id]").val();
	localStorage.removeItem('offer' + id);
}

var id = $("input[name=offer_id]").val();
var serialize = localStorage.getItem('offer' + id);
if (serialize) {
	$('#draftNotice').modal('show');
}

$(document).on("click", "#draftNotice .open", function() {
	$('#draftNotice').modal('hide');
	fill_offer();
	preview();
});

$(document).on("click", "#draftNotice .cancel, .cancelDraft", function() {
	$('#draftNotice').modal('hide');
	delete_offer();
});

$(document).on("click", ".saveToDraft", function() {
	save_offer();
});

$(document).on("click", "#save-template", function() {
	var linkSave = $('#linkSave').serialize();
	var url = $('#linkSave').data('url');
	$.ajax({
		type: 'POST',
		async: false,
		data: ({data:linkSave}),
		url: url,
		success: function(data) {
			var template = $('#template-name').val();
			if (template.length === 0) {
				alert('Please type template name!');
				return false;
			}
			$('#save-template').removeClass("btn-primary").addClass("btn-success");
			$('#save-template i').removeClass("fa-floppy-o").addClass("fa-check");
			$('#template-name').val('');

		}
	});
	return false;
});

$(function() {
	var btnUpload = $('#upload');
	var url = $('#upload').data('url');
	new AjaxUpload(btnUpload, {
		action: url,
		name: 'uploadfile',
		onSubmit: function(file, ext) {
			 if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
				alert('Разрешено только jpg|png|jpeg|gif файлы');
				return false;
			}
			$("#spinner").show();
		},
		onComplete: function(file, response) {
			data = JSON.parse(response);
			if (data.close === true) {
				var close = '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
			} else {
				var close = '';
			}
			var random = Math.round(Math.random() * 1000);
			$("#imgLogs").append('<div id="close' + random + '" class="alert alert-' + data.type + '">' + data.text + close + '</div>');
			show_image();
			$("#spinner").hide();
			setTimeout(function() {
				$('#close' + random).slideUp(550);
			}, 6000);
		}
	});
});

$(function() {
	$('.input-colorpicker').colorpicker({'format' : 'hex'}).on('changeColor', function(ev) {
		preview();
	});
	$('#linkSave select').change(function() {
		preview();
	});
	$('#linkSave input').keyup(function() {
		preview();
	});
});

$('#offer_setting_view').change(function() {
		var selected = $("#offer_setting_view :selected").val();
		if (selected == 'x_days') {
			$('#offer_setting_view_x_days').attr('readonly', false);
		} else {
			$('#offer_setting_view_x_days').attr('readonly', true);
		}
	});


var TouchFontSize = $(".TouchFontSize").TouchSpin({
	verticalupclass: 'glyphicon glyphicon-plus',
	verticaldownclass: 'glyphicon glyphicon-minus',
	buttonup_class: "btn btn-xs btn-default TouchFontSize",
	buttondown_class: "btn btn-xs btn-default TouchFontSize",
	min: 16,
	step: 2,
	max: 40,
});

TouchFontSize.on("touchspin.on.stopspin", function() {
	preview();
});

var Spin = $(".TouchSpin").TouchSpin({
	verticalbuttons: true,
	verticalupclass: 'glyphicon glyphicon-plus',
	verticaldownclass: 'glyphicon glyphicon-minus',
	step: 2,
	min: 250,
	max: 800,
});

Spin.on("touchspin.on.stopspin", function() {
	preview();
});

$(document).on("click", ".chooseImage", function() {
	var inp = $(this).data('inp');
	$('#gridSystemModal').modal('show').data('inp', inp);
	$("#imgList .img-preview").removeClass("active");
	$('.modal-footer #check').hide();
	$('.modal-footer #delete').hide();
});

show_image();

$(document).on("click", ".img-preview", function() {
	$("#imgList .img-preview").removeClass("active");
	$(this).addClass("active");
	var src = $(this).attr('src');
	$('.modal-footer #check').data('src', src).show();
	$('.modal-footer #delete').data('src', src).show();
});

$(document).on("click", '.modal-footer #check', function() {
	var src = window.location.protocol + '//' + location.hostname + $(this).data('src');
	var inp = $('#gridSystemModal').data('inp');
	$("input[name='offer_setting[" + inp + "]']").val(src);
	$('#gridSystemModal').modal('hide');
	preview();
});

$(document).on("click", '.modal-footer #delete', function() {
	var src = $(this).data('src');
	var url = $('#delete').data('url');
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		async: false,
		data: ({src:src}),
		url: url,
		success: function(data) {
			if (data.close === true) {
				var close = '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
			} else {
				var close = '';
			}
			var random = Math.round(Math.random() * 1000);
			$("#imgLogs").append('<div id="close' + random + '" class="alert alert-' + data.type + '">' + data.text + close + '</div>');
			show_image();
			setTimeout(function() {
				$('#close' + random).slideUp(550);
			}, 6000);
		}
	});
});

var saveDraft = function(i) {
	if (i == 1) {
		i = 2;
	} else {
		var display = $('#draftNotice').css('display');
		if (display == 'none') {
			save_offer();
		}
	}
	setTimeout(saveDraft, 30000);
};
saveDraft(1);

lang = $('.selectLang .btn-success').text();

$('.input-daterange').datepicker({
	format: "dd-mm-yyyy",
	language: lang,
	todayBtn: "linked",
	todayHighlight: true
});

$(document).on("click", '#table-hours .date-select', function() {
	var cl = $(this).data('day');
	var done = $(this).data('done');
	if(done == "true"){
		$('.'+cl+' input[type=checkbox]' ).prop('checked', false);
		$(this).data('done', 'false');
	}
	else{
		$('.'+cl+' input[type=checkbox]' ).prop('checked', true);
		$(this).data('done', 'true');
	}
	return false;
});

$(document).on("click", '#table-hours .select-all', function() {
	var done = $(this).data('done');
	if(done == "true"){
		$('input[type=checkbox]' ).prop('checked', false);
		$(this).data('done', 'false');
		$('#table-hours .date-select').data('done', 'false');
	}
	else{
		$('input[type=checkbox]' ).prop('checked', true);
		$(this).data('done', 'true');
		$('#table-hours .date-select').data('done', 'true');
	}
	return false;
});