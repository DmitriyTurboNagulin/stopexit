$(function() {
	$(".switch").bootstrapSwitch();
});



$(document).ready(function() {
    $("#sms_service").change(function() {
        var sms_service = $("#sms_service option:selected").val();
        var login = true;
        var pass = true;
        var api = false;
        if (sms_service == 'mobizon') {
            login = false;
            pass = false;
            api = true;
        }
        if (login) {
            $("#sms_login").removeAttr("readonly");
        } else {
            $("#sms_login").attr("readonly", "readonly");
        }
        if (pass) {
            $("#sms_pass").removeAttr("readonly");
        } else {
            $("#sms_pass").attr("readonly", "readonly");
        }
        if (api) {
            $("#sms_api").removeAttr("readonly");
        } else {
            $("#sms_api").attr("readonly", "readonly");
        }
    });


    $("input[name='setting[sms_status]']").on("switchChange.bootstrapSwitch", function (event, state) {
        if (state == false) {
            $("#sms_set").hide();
        } else {
            $("#sms_set").show();
        }
    });

    $("input[name='setting[smtp_status]']").on("switchChange.bootstrapSwitch", function (event, state) {
        if (state == false) {
            $("#smtp_set").hide();
        } else {
            $("#smtp_set").show();
        }
    });

    $('.nav.nav-tabs a').click(function(e) {
        e.preventDefault();
        tab = $(this).data('tab');
        url = $(this).parent().parent().data('url');
        if (tab && url) {
            url = url + tab;
            console.log(url);
            if (url != window.location) {
                window.history.pushState(null, null, url);
            }
        }
        $(this).tab('show');
    });
});
