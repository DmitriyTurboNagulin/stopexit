$(function() {
	$('#side-menu').metisMenu();

	$(window).bind("load resize", function() {
		if ($(this).width() < 768) {
			$('div.sidebar-collapse').addClass('collapse');
		} else {
			$('div.sidebar-collapse').removeClass('collapse');
		}
		$('#page-wrapper').css('min-height', $(this).height() - $('.navbar').outerHeight());
	});

	$('.selectLang button').click(function() {
		document.cookie = 'lang=' + $(this).text() + '; path=/;';
		window.location.href = window.location.href;
	});

	$('tr.trash').click(function() {
		var url = $(this).data('url');
		$('#confirm').modal('show');
		$('#confirm #btn-confirm').attr('href', url);
		return false;
	});

	$('.linkShow').click(function() {
		$('#show').show();
		var url = $(this).attr('href');
		var id = $(this).data('id');
		$.ajax({
			type: 'POST',
			async: false,
			url: url,
			data: ({id : id}),
			success: function(data) {
				$('#show').html(data);
				console.log(data);
			}
		});
		// $('#show:first-child').css('z-index', "99999");
		return false;
	});

	$('.delete').click(function() {
		var url = $(this).attr('href');
		$('#delete').modal('show');
		$('#delete #btn-delete').attr('href', url);
		return false;
	});

	$('#offer_setting_event').change(function() {
		var selected = $("#offer_setting_event :selected").val();
		if (selected == 'x_sec_page') {
			$('#offer_setting_x_sec').attr('readonly', false);
		} else {
			$('#offer_setting_x_sec').attr('readonly', true);
		}
	});

	$('#only-main').change(function() {
		var input = document.getElementById("only-main");
		var templates = $('#offer_setting_templates');
		if (input.checked) {
			templates.val('main').hide();
		} else {
			templates.val('').show();
		}
	});

	$("#sms_service").change(function() {
		var sms_service = $("#sms_service option:selected").val();
		var login = true;
		var pass = true;
		var api = false;
		if (sms_service == 'mobizon') {
			login = false;
			pass = false;
			api = true;
		}
		if (login) {
			$("#sms_login").removeAttr("readonly");
		} else {
			$("#sms_login").attr("readonly", "readonly");
		}
		if (pass) {
			$("#sms_pass").removeAttr("readonly");
		} else {
			$("#sms_pass").attr("readonly", "readonly");
		}
		if (api) {
			$("#sms_api").removeAttr("readonly");
		} else {
			$("#sms_api").attr("readonly", "readonly");
		}
	});

	$("#showChart").on("show.bs.modal", function(e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});

});

function parse_str(str, array) {

	var glue1 = '=';
	var glue2 = '&';

	var array2 = str.split(glue2);
	var array3 = [];
	for (var x=0; x<array2.length; x++) {
		var tmp = array2[x].split(glue1);
		array3[unescape(tmp[0])] = unescape(tmp[1]).replace(/[+]/g, ' ');
	}

	if (array) {
		array = array3;
	} else {
		return array3;
	}
}