function preview() {
    var anchSave = $('#anchSave').serialize();
    var url = $('#anchSave').data('url');
    $.ajax({
        type: 'POST',
        url: url,
        async: false,
        data: ({data : anchSave}),
        success: function(data) {
            $('#show').html(data);
        }
    });
        if ($("input[name='anchor_setting[anchor_setting_icon_ref]']").val() != '') {
            $("input[name='anchor_setting[anchor_setting_icon_color]']").prop('disabled', true);
            $('#icons-optional').prop('disabled', true);
        } else {
            $("input[name='anchor_setting[anchor_setting_icon_color]']").prop('disabled', false);
            $('#icons-optional').prop('disabled', false);
        }
        if ($('#make-own').is(':selected')) {
        }
}


function timeFormat(time) {
    return ('0' + (time)).slice(-2);
}

function save_anchor() {
    $('.draft').hide();
    var id = $("input[name=anchor_id]").val();
    var serialize = $('#anchSave').serialize();
    localStorage.setItem('anchor' + id, serialize);
    var today = new Date();
    $('.draft span').text(timeFormat(today.getHours()) + ':' + timeFormat(today.getMinutes()) + ':' + timeFormat(today.getSeconds()));
    $('.draft').fadeIn(300);
}

function fill_anchor() {
    var id = $("input[name=anchor_id]").val();
    var serialize = localStorage.getItem('anchor' + id);
    if (serialize) {
        $("#anchSave").deserialize(serialize);
    }
}



function delete_anchor() {
    var id = $("input[name=anchor_id]").val();
    localStorage.removeItem('anchor' + id);
}

var id = $("input[name=anchor_id]").val();
var serialize = localStorage.getItem('anchor' + id);
if (serialize) {
    $('#draftNotice').modal('show');
}

$(document).on("click", "#draftNotice .open", function() {
    $('#draftNotice').modal('hide');
    fill_anchor();
    preview();
});


$(document).on("click", "#checkDraft", function() {
    var data = $(this).data('data');
    if (data) {
        data = parse_str(decodeURIComponent(data));
        for(var key in data) {
            var val = data[key];
            if (key.indexOf('switch') != -1 || key.indexOf('enable') != -1) {
                $('input[name="' + key + '"]').bootstrapSwitch('state', val);
            }
            $('select[name="' + key + '"]').val(val);
            $('input[name="' + key + '"]').val(val);
            $('input[name="' + key + '"] ').parent(".input-colorpicker").colorpicker('setValue', val);
        }
    }
    save_anchor();
    preview();
});

$(document).on("click", ".showDraft", function() {
    var id = $(this).data('id');
    var data = $(this).data('data');
    $('.showDraft').removeClass('active');
    $(this).addClass('active');
    $('#showText').show();
    $('#checkDraft').data('data', data);
    var url = $('#showAnchHistory').text();
    $.ajax({
        type: 'POST',
        url: url,
        async: false,
        data: ({id : id}),
        success: function(data) {
            // data = parse_str(decodeURIComponent(data));
            // console.log(data);return false;
            $('#show2').html(data);
            $('#show2 div:first').css('position', 'absolute');
            // $('#show2 .anchor').css('position', 'absolute');
        },
    });
    return false;
});

$(document).on("click", "#draftNotice .cancel, .cancelDraft", function() {
    $('#draftNotice').modal('hide');
    delete_anchor();
});

$(document).on("click", ".saveToDraft", function() {
    save_anchor();
});

$(document).on("click", "#draftNotice .open", function() {
    $('#draftNotice').modal('hide');
    fill_anchor();
    preview();
});

$(function() {
    $('.input-colorpicker').colorpicker({'format' : 'hex'}).on('changeColor', function(ev) {
        preview();
    });
    $('#linkSave select').change(function() {
        preview();
    });
    $('#linkSave input').keyup(function() {
        preview();
    });
});


$('#opacity').slider({
    formatter: function(value) {
        preview();
        return 'Current value: ' + value;
    }
});

$('#radius').slider({
    formatter: function(value) {
        preview();
        return 'Current value: ' + value;
    }
});

$('#border-line').slider({
    formatter: function(value) {
        preview();
        return 'Current value: ' + value;
    }
});

$('#shadow').slider({
    formatter: function(value) {
        preview();
        return 'Current value: ' + value;
    }
});

$('#size').slider({
    formatter: function(value) {
        preview();
        return 'Current value: ' + value;
    }
});

$('.margin').slider({
    formatter: function(value) {
        preview();
        return 'Current value: ' + value;
    }
});

$(document).on("click", ".saveToDraft", function() {
    save_anchor();
});

$(document).on("switchChange.bootstrapSwitch", ".switch", function() {
    preview();
});

$(document).on("change", "#icons", function() {
    preview();
});

$(document).on("change", "#icons-optional", function() {
    preview();
});

$(document).on("change", '#position', function() {
    preview();
});

$(document).on("change", '#margin', function() {
    preview();
});

$('#animation').slider({
    formatter: function(value) {
        preview();
        return 'Current value: ' + value;
    }
});

function show_image() {
    $("#spinner").show();
    var url = $('#showLink').text();
    $.ajax({
        type: "POST",
        async: false,
        url: url,
        success: function(data) {
            $("#imgList").html(data);
        },
        complete: function() {
            $("#spinner").hide();
        },
    });
}

$(function() {
    var btnUpload = $('#upload');
    var url = $('#upload').data('url');
    new AjaxUpload(btnUpload, {
        action: url,
        name: 'uploadfile',
        onSubmit: function(file, ext) {
             if (!(ext && /^(jpg|png|jpeg|gif|svg)$/.test(ext))) {
                alert('Разрешено только jpg|png|jpeg|gif|svg файлы');
                return false;
            }
            $("#spinner").show();
        },
        onComplete: function(file, response) {
            data = JSON.parse(response);
            if (data.close === true) {
                var close = '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
            } else {
                var close = '';
            }
            var random = Math.round(Math.random() * 1000);
            $("#imgLogs").append('<div id="close' + random + '" class="alert alert-' + data.type + '">' + data.text + close + '</div>');
            show_image();
            $("#spinner").hide();
            setTimeout(function() {
                $('#close' + random).slideUp(550);
            }, 6000);
        }
    });
});

$(document).on("click", ".chooseImage", function() {
    var inp = $(this).data('inp');
    $('#gridSystemModal').modal('show').data('inp', inp);
    $("#imgList .img-preview").removeClass("active");
    $('.modal-footer #check').hide();
    $('.modal-footer #delete').hide();
});

show_image();

$(document).on("click", ".img-preview", function() {
    $("#imgList .img-preview").removeClass("active");
    $(this).addClass("active");
    var src = $(this).attr('src');
    $('.modal-footer #check').data('src', src).show();
    $('.modal-footer #delete').data('src', src).show();
});

$(document).on("click", '.modal-footer #check', function() {
    var src = window.location.protocol + '//' + location.hostname + $(this).data('src');
    var inp = $('#gridSystemModal').data('inp');
    $("input[name='anchor_setting[" + inp + "]']").val(src);
    $('#gridSystemModal').modal('hide');
    preview();
});




$(document).on("click", "#clear", function() {
    $("input[name='anchor_setting[anchor_setting_icon_ref]']").val('');
    preview();
});

$(".switch").bootstrapSwitch({
    onText : 'On',
    offText : 'Off'
});

$(document).on("click", '.modal-footer #delete', function() {
    var src = $(this).data('src');
    var url = $('#delete').data('url');
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        async: false,
        data: ({src:src}),
        url: url,
        success: function(data) {
            if (data.close === true) {
                var close = '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
            } else {
                var close = '';
            }
            var random = Math.round(Math.random() * 1000);
            $("#imgLogs").append('<div id="close' + random + '" class="alert alert-' + data.type + '">' + data.text + close + '</div>');
            show_image();
            setTimeout(function() {
                $('#close' + random).slideUp(550);
            }, 6000);
        }
    });
});

preview();