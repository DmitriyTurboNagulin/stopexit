<?php

class Phone_model extends Model {
	protected $table = 'phone';
	protected $primaryKey = 'phone_id';
	protected $fillable = array(
        'phone_number',
        'call_id',
    );

	public function __construct($attributes = array()) {
		parent::__construct($attributes);
	}

    public function getPhoneNumberAttribute($phone_number) {
        return Crypt::decode($phone_number);
    }

}
