<?php

use \Config;

class Openid_model {
	protected static $_instance = NULL;

	private static $openid;
	private static $openid_config;

	public static function getInstance() {
		if (self::$_instance === NULL) {
			$object = new self;
			self::$_instance = new self;
		}
		return self::$_instance;
	}

	private function __clone() {}

	private function __wakeup() {}

	private function __construct() {
		$openid_config = Config::get('openid');
		if (!isset($openid_config['realm'])) {
			throw new ErrorException('Not set variable realm in config');
		}
		self::$openid = new LightOpenID($openid_config['realm']);
		self::$openid_config = $openid_config;
	}

	public function openid_get_login($params = array()) {
		$openid = self::$openid;
		$openid_config = self::$openid_config;
		if (empty($openid_config['identity'])) {
			throw new ErrorException('Not set variable identity in config');
		}
		$this->openid_set_identity($openid_config['identity']);
		if (!isset($openid_config['required'])) {
			throw new ErrorException('Not set variable required in config');
		}
		$this->openid_set_required($openid_config['required']);
		if (!empty($params['return_url'])) {
			$this->openid_set_return_url($params['return_url']);
		}
		return $openid;
	}

	public function openid_get() {
		return self::$openid;
	}

	private function openid_set_identity($identity) {
		self::$openid->identity = $identity;
	}

	private function openid_set_required($required = array()) {
		self::$openid->required = $required;
	}

	private function openid_set_return_url($return_url = '') {
		self::$openid->returnUrl = $return_url;
	}

	public function openid_get_mode() {
		return self::$openid->mode;
	}

	public function openid_validate() {
		return self::$openid->validate();
	}

	public function openid_get_attributes() {
		return self::$openid->getAttributes();
	}

	public function openid_set_config($key, $value) {
		self::$openid_config[$key] = $value;
	}

}
