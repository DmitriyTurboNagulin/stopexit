<?php

class CallSetting_model extends Model {
	protected $table = 'call_setting';
	protected $primaryKey = 'call_setting_id';
	protected $fillable = array(
		'call_setting_email',
		'call_setting_company',
		'call_setting_text',
		'call_setting_text_size',
		'call_setting_logo',
		'call_setting_yes',
		'call_setting_num_error',
		'call_setting_yes_size',
		'call_setting_color_btn',
		'call_setting_no',
		'call_setting_background',
		'call_setting_background_image',
		'call_setting_background_position',
		'call_setting_color',
		'call_setting_color_btn',
		'call_setting_border_type',
		'call_setting_border_color',
		'call_setting_border_color2',
		'call_setting_width',
		'call_setting_height',
		'call_setting_view',
		'call_setting_event',
		'call_setting_x_sec',
		'call_setting_position',
		'call_setting_effect',
		'call_setting_templates',
		'call_setting_templates_white',
		'call_setting_templates_black',
		'call_setting_domain',
		'call_setting_days',
		'call_setting_view',
		'call_setting_view_x_days',
		'call_id',
		'call_setting_start',
		'call_setting_stop',
	);
	public $timestamps = FALSE;
	protected $softDelete = TRUE;

	public function __construct($attributes = array()) {
		parent::__construct($attributes);
	}

	public static function ColorLuminance ($colourstr, $steps) {
		$colourstr = str_replace('#','',$colourstr);
		$rhex = substr($colourstr,0,2);
		$ghex = substr($colourstr,2,2);
		$bhex = substr($colourstr,4,2);

		$r = hexdec($rhex);
		$g = hexdec($ghex);
		$b = hexdec($bhex);

		$r = dechex(max(0,min(255,$r + $steps)));
		$g = dechex(max(0,min(255,$g + $steps)));
		$b = dechex(max(0,min(255,$b + $steps)));
		$r = strlen($r) == 1 ? '0'.$r : $r;
		$g = strlen($g) == 1 ? '0'.$g : $g;
		$b = strlen($b) == 1 ? '0'.$b : $b;
		return '#'.$r.$g.$b;
	}

	public static function style($data) {
				if (!empty($data['call_setting_start'])) {
			$tmp = strtotime($data['call_setting_start']);
			// 1451602800 это timestamp для первого января 2016 года
			if ($tmp > 1451602800){
				$data['call_setting_start'] = date('d-m-Y', $tmp);
			} else {
				$data['call_setting_start'] = NULL;
			}
		}
		if (!empty($data['call_setting_stop'])) {
			$tmp = strtotime($data['call_setting_stop']);
			if ($tmp > 1451602800){
				$data['call_setting_stop'] = date('d-m-Y', $tmp);
			} else {
				$data['call_setting_stop']  = NULL;
			}
		}
		if (!empty($data['call_setting_days'])) {
			$data['call_setting_days'] = json_decode($data['call_setting_days'], TRUE);
		} else {
			$data['call_setting_days']  = NULL;
		}
		if (!empty($data['call_setting_position'])) {
			switch ($data['call_setting_position']) {
				case 'left_top':
					$data['position'] = 'position:fixed;left:10px;top:10px;';
				break;
				case 'left_bottom':
					$data['position'] = 'position:fixed;left:10px;bottom:10px;';
				break;
				case 'right_top':
					$data['position'] = 'position:fixed;right:10px;top:10px;';
				break;
				case 'right_bottom':
					$data['position'] = 'position:fixed;right:10px;bottom:10px;';
				break;
				default:
					$data['position'] = 'position:fixed;left:50%;top:50%;margin:-' . $data['call_setting_height'] / 2 . 'px 0 0 -' . $data['call_setting_width'] / 2 . 'px;';
			}
		}
		if (!empty($data['call_setting_border_type'])) {
			if ($data['call_setting_border_type'] == 'double') {
				$data['border'] = 'outline:2px solid ' . $data['call_setting_border_color2'] .';border:2px solid ' . $data['call_setting_border_color'] .';';
			} else {
				$data['border'] = 'border:2px ' . $data['call_setting_border_type'] . ' ' . $data['call_setting_border_color'] .';';
			}
		}
		if (!empty($data['call_setting_num_error'])) {
			$error_len = strlen($data['call_setting_num_error']);
		}
		switch ($data['call_setting_manager_position']) {
			case 'right':
				$data['call_setting_input_position'] = 'width:90%; margin: 0 auto 10px auto;';
				$data['call_setting_container_position'] = 'display:inline-block; text-align:center;float:right; width:45%;';
				$data['call_setting_company_position'] = 'display:block; margin: 0 0 10px 0;';
				$data['call_setting_text_position'] = 'display:block;width:100%; margin: 0 0 10px 0;';
				$data['call_setting_button_position'] = 'display:block;width:90%; margin: 0 auto;';
				$data['error_setting'] = 'width:140px; height:50px;margin-left:10px;';
				break;
			case 'left':
				$data['call_setting_input_position'] = 'width:90%; margin: 0 auto 10px auto;';
				$data['call_setting_container_position'] = 'display:inline-block; text-align:center;float:left; width:45%;';
				$data['call_setting_company_position'] = 'display:block; margin: 0 0 10px 0;';
				$data['call_setting_text_position'] = 'display:block;width:100%; margin: 0 0 10px 0';
				$data['call_setting_button_position'] = 'display:block;width:90%; margin: 0 auto;';
				$data['error_setting'] = 'width:140px; height:50px;margin-left:10px;';
				break;
			case 'none':
				$data['call_setting_input_position'] = 'width:50%; margin: 0 auto 15px auto;';
				$data['call_setting_container_position'] = 'width:100%; display:inline-block; text-align:center;float:left;';
				$data['call_setting_company_position'] = 'display:block; margin: 15px 0 15px 0;';
				$data['call_setting_text_position'] = 'display:block;width:100%; margin: 0 0 15px 0';
				$data['call_setting_button_position'] = 'display:block;width:50%; margin: 0 auto;';
				$data['error_setting'] = 'margin-left:20% !important;width:280px;';
				break;
		}
		if (!empty($data['call_setting_effect'])) {
			switch ($data['call_setting_effect']) {
				case 'instantly':
					$data['speed'] = '0';
					break;
				case 'slow':
					$data['speed'] = '1.5';
					break;
			}
		}
		if (!empty($data['call_setting_background'])) {
			$data['call_setting_background_step_30'] = self::ColorLuminance($data['call_setting_background'], 30);
			$data['call_setting_background_step_20'] = self::ColorLuminance($data['call_setting_background'], 20);
			$data['call_setting_background_step_5'] = self::ColorLuminance($data['call_setting_background'], 5);
		}
		$data['remove'] = "this.parentNode.parentNode.parentNode.parentNode.style.display='none'";
		return $data;
	}

	public function getCallSettingEmailAttribute($call_setting_email) {
		return Crypt::decode($call_setting_email);
	}


	public function getCallSettingCompanyAttribute($call_setting_company) {
		return Crypt::decode($call_setting_company);
	}

	public function getCallSettingTextAttribute($call_setting_text) {
		return Crypt::decode($call_setting_text);
	}

}
