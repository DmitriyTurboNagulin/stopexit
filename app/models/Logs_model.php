<?php

class Logs_model extends Model {
	protected $table = 'logs';
	protected $primaryKey = 'id';
	protected $fillable = array('id');
	public $timestamps = FALSE;

	private $event = array();

	public function __construct($attributes = array()) {
		$this->event = array(
			'login' => l('Пользователь залогинился'),
			'offer_create' => l('Создан\отредактирован StopExit '),
			'offer_delete' => l('Удален StopExit'),
			'offer_restore' => l('Восстановлен StopExit'),
			'create_offer_template' => l('Создан шаблон StopExit'),
			'edit_offer_template' => l('Отредактирова шаблон StopExit'),
			'delete_offer_template' => l('Удален шаблон StopExit'),
			'create_anchor' => l('Создана иконка'),
			'edit_anchor' => l('Отредактирована иконка'),
			'delete_anchor' => l('Удалена иконка'),
			'anchor_restore' => l('Восстановлена иконка'),
			'create_call' => l('Создан\отредактирован GetMoreCalls'),
			'delete_call' => l('Удален GetMoreCalls'),
			'call_restore' => l('Восстановлен GetMoreCalls'),
			'create_call_template' => l('Создан шаблон GetMoreCalls'),
			'edit_call_template' => l('Создан шаблон GetMoreCalls'),
			'delete_call_template' => l('Удален шаблон GetMoreCalls'),
			'setting_update' => l('Изменены настройки'),
		);
		parent::__construct($attributes);
	}

	public function getEvent() {
		return $this->event;
	}

}