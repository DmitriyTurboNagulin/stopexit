<?php if (!defined('APP_PATH')) exit('No direct script access allowed');

class Image_model extends Model {

	private $extType = array(
		'jpg',
		'jpeg',
		'gif',
		'png',
	);

	public function getExtType() {
		return $this->extType;
	}

	public function check($file) {
		$file = explode('.', strtolower($file));
		$ext = end($file);
		return in_array($ext, $this->getExtType());
	}

	public function scandir($dir) {
		$echo = '';
		$files = scandir(ROOT_PATH . $dir);
		foreach ($files as $file) {
			if (!is_dir(ROOT_PATH . $dir . $file) and $this->check($file)) {
				$echo .= '<img src="/stopexit' . $dir . $file . '" class="img-thumbnail img-preview">';
			}
		}
		return $echo;
	}

	public function upload($uploaddir, $uploadfile = 'uploadfile') {
		$uploaddir = ROOT_PATH . $uploaddir;
		if (!$this->check($_FILES[$uploadfile]["name"])) {
			$output = array(
				'type' => 'warning',
				'close' => TRUE,
				'text' => l('Разрешены изображения в форматах') . ': ' . implode(', ', $this->getExtType()),
			);
		} else {
			if ($_FILES[$uploadfile]["size"] > 1024*1*1024) {
				$output = array(
					'type' => 'warning',
					'close' => TRUE,
					'text' => l('Размер файла превышает 1 мегабайт'),
				);
			} else {
				// Проверяем загружен ли файл
				if (is_uploaded_file($_FILES[$uploadfile]["tmp_name"])) {
					// Если файл загружен успешно, перемещаем его
					// из временной директории в конечную
					$file = strtolower($_FILES[$uploadfile]["name"]);
					$file = explode('.', $file);
					$ext = end($file);
					$fileName = time() . '_' . rand(10,99) . '.' . $ext;
					move_uploaded_file($_FILES[$uploadfile]["tmp_name"], $uploaddir . $fileName);
					$output = array(
						'type' => 'success',
						'close' => TRUE,
						'text' => l('Изображение успешно загружено'),
					);
				} else {
					$output = array(
						'close' => TRUE,
						'text' => l('Ошибка загрузки изображения'),
						'status' => 'danger',
					);
				}
			}
		}
		return $output;
	}

	public function delete($src, $dir) {
		$file = ROOT_PATH . $dir . basename($src);
		if (file_exists($file)) {
			if (!unlink($file)) {
				$output = array(
					'type' => 'danger',
					'close' => TRUE,
					'text' => l('Ошибка при удалении изображения'),
				);
			} else {
				$output = array(
					'type' => 'success',
					'close' => TRUE,
					'text' => '<i class="fa fa-trash-o"></i> ' . l('Изображение успешно удалено'),
				);
			}
		} else {
			$output = array(
				'type' => 'danger',
				'close' => TRUE,
				'text' => l('Неправильный путь к файлу'),
			);
		}
		return $output;
	}

}
