<?php

require_once(APP_PATH . 'models/sms/main.php');

class model_sms_turbosms extends model_sms_main {

	private static $database = NULL;

	private function connect() {
		if (empty(self::$database)) {
			$config = array(
				PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES `utf8`',
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			);
			try {
				$pdo = new pdo('mysql:host=94.249.146.189;dbname=users', $this->setting['sms_login'], $this->setting['sms_pass'], $config);
			} catch(PDOException $ex) {
				return FALSE;
			}
			self::$database = new model_db($pdo);
		}
	}

	public function balance() {
		$balance = FALSE;
		if ($this->connect() === FALSE) {
			return FALSE;
		}
		$sql = 'SELECT * FROM ' . $this->setting['sms_login'] . ' ORDER BY id DESC LIMIT 1';
		$rs = self::$database->queryRow($sql);
		if (!empty($rs['id'])) {
			$balance = $rs['balance'] . ' кредитов';
		}
		return $balance;
	}

	public function send($phone, $msg) {
		if ($this->connect() === FALSE) {
			return FALSE;
		}
		$data = array(
			'sign' => $this->setting['sms_alphaname'],
			'number' => $phone,
			'message' => $msg,
		);
		$insert = self::$database->insert($this->setting['sms_login'], $data);
		if (!empty($insert)) {
			return 1;
		} else {
			return 0;
		}
	}

}
