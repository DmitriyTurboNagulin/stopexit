<?php

require_once(APP_PATH . 'models/sms/main.php');

class model_sms_smsfeedback extends model_sms_main {

	public function balance() {
		$res = $this->curl('http://api.smsfeedback.ru/messages/v2/balance/');
		if ($res == 'error authorization') {
			return FALSE;
		} elseif(strpos($res, ';') !== FALSE) {
			$res = explode(';', $res);
			if (count($res) == 3) {
				return $res[1] . $res[0] . '<br>Кредит: ' . $res[2] . $res[0];
			}
		}
		return FALSE;
	}

	public function send($to, $msg) {
		$msg = urlencode($msg);
		$sender = $this->setting['sms_alphaname'];
		$res = $this->curl('http://api.smsfeedback.ru/messages/v2/send/?sender=' . $sender . '&phone=%2B' . $to . '&text=' . $msg);
		$res = explode(';', $res);
		if (count($res) == 2 and !empty($res[0]) and $res[0] == 'accepted') {
			return 1;
		} else {
			return 0;
		}
	}

	private function curl($url) {
		$login = $this->setting['sms_login'];
		$password = $this->setting['sms_pass'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_USERPWD, $login . ':' . $password); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$res = curl_exec($ch);
		curl_close($ch);
		return $res;
	}

}
