<?php

require_once(APP_PATH . 'models/sms/main.php');

class model_sms_websms extends model_sms_main {

	public function balance() {
		if (empty($this->setting['sms_login']) or empty($this->setting['sms_pass'])) {
			return FALSE;
		}
		$login = $this->setting['sms_login'];
		$password = $this->setting['sms_pass'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, 'http://cab.websms.ru/http_credit.asp?http_username=' . $login . '&http_password=' . $password);
		$res = curl_exec($ch);
		curl_close($ch);
		if (empty($res)) {
			return FALSE;
		} else {
			return $res . ' руб';
		}
	}

	public function send($to, $msg) {
		$login = $this->setting['sms_login'];
		$password = $this->setting['sms_pass'];
		$alphaname = $this->setting['sms_alphaname'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'Http_username=' . urlencode($login) . '&Http_password=' . urlencode($password) . '&fromPhone=' . $alphaname . '&Phone_list=' . $to . '&Message=' . urlencode($msg));
		curl_setopt($ch, CURLOPT_URL, 'http://www.websms.ru/http_in6.asp');
		$res = trim(curl_exec($ch));
		curl_close($ch);

		if (strpos($res, 'error_num=OK') !== FALSE) {
			return 1;
		} else {
			return 0;
		}
	}

}
