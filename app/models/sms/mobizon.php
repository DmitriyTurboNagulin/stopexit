<?php

require_once(APP_PATH . 'models/sms/main.php');

class model_sms_mobizon extends model_sms_main {
	/**
	 * @var string Ключ API
	 */
	protected $apiKey;

	/**
	 * @var string URL доступа к API
	 */
	protected $apiUrl = 'https://mobizon.kz/service/';

	/**
	 * @var string Версия API
	 */
	protected $apiVersion = 'v1';

	/**
	 * @var string Время ожидания ответа API в секундах
	 */
	protected $timeout = 30;

	/**
	 * @var string Формат ответа API
	 */
	protected $format = 'json';

	/**
	 * @var array Возможные форматы ответа API
	 */
	protected $allowedFormats = array('xml', 'json');

	/**
	 * @var resource
	 */
	protected $curl;

	/**
	 * @var integer Код последней выполненной операции call
	 */
	protected $code = -1;

	/**
	 * @var mixed Данные, возвращенные последним запросом к API
	 */
	protected $data = array();

	/**
	 * @var string Сообщение, полученное с последним ответом сервера
	 */
	protected $message = '';

	/**
	 * @param string $apiKey Ключ API
	 * @param array  $params Параметры API
	 * <dl>
	 *   <dt>(string) format</dt><dd>Формат ответа API</dd>
	 *   <dt>(int) timeout</dt><dd>Время ожидания ответа API в секундах</dd>
	 *   <dt>(string) apiVersion</dt><dd>Версия API</dd>
	 *   <dt>(string) apiUrl</dt><dd>Адрес API</dd>
	 * </dl>
	 */
	public function __construct($setting) {
		parent::__construct($setting);

		$this->apiKey = $this->setting['sms_api'];

		$this->curl = curl_init();
		/* @todo: Сделать необходимость проверки сертификата принимаемым параметром */
		/* curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0); */
		curl_setopt($this->curl, CURLOPT_USERAGENT, 'Mobizon-PHP/1.0.0');
		/* @todo: не работает на шаред хостингах, так как включен open_basedir или safe_mode */
		/* curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true); */
		curl_setopt($this->curl, CURLOPT_HEADER, false);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, $this->timeout);
		curl_setopt($this->curl, CURLOPT_TIMEOUT, 600);
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded'));
	}

	/**
	 * Установка параметров
	 * @param string $key Параметр
	 * @param mixed  $value Хначение параметра
	 */
	public function __set($key, $value) {
		switch ($key) {
			case 'format':
				$value = strtolower($value);
				if (!in_array($value, $this->allowedFormats)) {
					exit('Format should be one of the following: ' . implode(', ', $this->allowedFormats) . '; ' . $value . ' provided');
				}
				break;
			case 'timeout':
				$value = (int)$value;
				if ($value < 0) {
					exit('Timeout can not be less than 0, ' . $value . ' provided');
				}
				break;
			case 'apiVersion':
				if (substr($value, 0, 1) !== 'v' || (int)substr($value, 1) < 1) {
					exit('Incorrect api version: ' . $value);
				}
				break;
			case 'apiUrl':
				if (!in_array(parse_url($value, PHP_URL_SCHEME), array('http', 'https'))) {
					exit('Incorrect api url: ' . $value);
				}
				$value = rtrim($value, '/');
				break;
			case 'code':
				$value = (int)$value;
				if ($value < 0 || $value > 999) {
					exit('Result code can not be handled: ' . $value);
				}
				break;
			default:
				exit('Incorrect class param or you can not set protected param: ' . $key);
				break;
		}

		$this->{$key} = $value;
	}

	/**
	 * Вызов методов API
	 * @param string $provider Название провайдера
	 * @param string $method Название метода
	 * @param array  $postParams Передаваемый в API массив параметров POST
	 * @param array  $queryParams Передаваемый в API массив GET параметров
	 * @param bool   $returnData Возвращать data вместо code
	 * @throws Mobizon_Http_Error
	 * @throws Mobizon_Param_Required
	 * @return mixed
	 */
	public function call(
		$provider,
		$method,
		array $postParams = array(),
		array $queryParams = array(),
		$returnData = false
	) {
		if (empty($provider)) {
			exit('You must provide "provider" parameter to MobizonApi::call');
		}

		if (empty($method)) {
			exit('You must provide "method" parameter to MobizonApi::call');
		}

		$queryDefaults = array(
			'api'    => $this->apiVersion,
			'apiKey' => $this->apiKey,
			'output' => $this->format
		);

		$queryParams = $this->applyParams($queryDefaults, $queryParams);
		$url = $this->apiUrl . '/' . strtolower($provider) . '/' . strtolower($method) . '?';
		curl_setopt($this->curl, CURLOPT_URL, $url . http_build_query($queryParams));
		if (!empty($postParams)) {
			curl_setopt($this->curl, CURLOPT_POST, true);
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($postParams));
		} else {
			curl_setopt($this->curl, CURLOPT_POST, false);
		}

		$result = curl_exec($this->curl);
		$error = curl_error($this->curl);
		if ($error) {
			exit('API call failed: ' . $error);
		}

		$result = $this->decode($result);

		$this->code = $result->code;
		$this->data = $result->data;
		$this->message = $result->message;

		return $returnData ? $this->getData() : (in_array($this->getCode(), array(0, 100)));
	}

	public function __destruct() {
		curl_close($this->curl);
	}

	/**
	 * Накладывает новые значения на дефолтные параметры настроек API
	 *
	 * @param $defaults
	 * @param $params
	 * @return array
	 */
	protected function applyParams($defaults, $params) {
		return array_merge(
			$defaults,
			array_intersect_key(
				$params,
				$defaults
			)
		);
	}

	public function getCode() {
		return $this->code;
	}

	/**
	 * Возвращает все данные, полученные из API во время последнего запроса
	 * или определенную их часть, если такой элемент существует
	 *
	 * @param string $subParam
	 * @return mixed
	 */
	public function getData($subParam = null) {
		if (!empty($subParam)) {
			if (!is_object($this->data)) {
				return false;
			}

			$subQuery = explode('.', $subParam);
			$data = $this->data;
			foreach ($subQuery as $subKey) {
				if (is_object($data) && property_exists($data, $subKey)) {
					$data = $data->{$subKey};
				} elseif (is_array($data) && array_key_exists($subKey, $data)) {
					$data = $data[$subKey];
				} else {
					return null;
				}
			}

			return $data;
		}

		return !empty($this->data) ? $this->data : null;
	}

	/**
	 * Проверяет, есть ли такие данные в ответе
	 *
	 * Можно проверить как весь ответ на наличие данных, так и определенный элемент данных
	 *
	 * @param bool $subParam
	 * @return bool
	 */
	public function hasData($subParam = null) {
		return null !== $this->getData($subParam);
	}

	/**
	 * @return string Сообщение, полученное с последним ответом сервера
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * Возвращает декодированный ответ API в зависимости от формата обмена
	 * @param mixed $responseData
	 * @return mixed Декодированный ответ API
	 */
	public function decode($responseData) {
		switch ($this->format) {
			case 'json':
				$result = $this->jsonDecode($responseData);
				break;

			case 'xml':
				$result = $this->xmlDecode($responseData);
				break;

			default:
				$result = false;
				break;
		}

		return $result;
	}

	/**
	 * Возвращает объект ответа API из JSON
	 * @param string $json
	 * @return mixed
	 */
	protected function jsonDecode($json) {
		return json_decode($json);
	}

	/**
	 * Возвращает объект ответа API из XML
	 * @param string $string
	 * @return object
	 * @throws Mobizon_XML_Error
	 */
	protected function xmlDecode($string) {
		$xml = simplexml_load_string($string);
		if (!$xml) {
			exit('Incorrect XML response');
		}
		return $xml;
	}

	public function balance() {
		if ($this->call('User', 'GetOwnBalance') && $this->hasData('balance')) {
			return round($this->getData('balance'), 2) . ' ' . $this->getData('currency');
		} else {
			return FALSE;
		}
	}

	public function send($phone, $msg) {
		$return = $this->call('message', 'sendSMSMessage', array(
			'recipient' => $phone,
			'text' => $msg,
			'from' => $this->setting['sms_alphaname'],
		));
		die;
	}

}
