<?php

abstract class model_sms_main extends Model {

	public $setting;

	function __construct($setting) {
		$this->setting = $setting;
	}

	public abstract function balance();
	public abstract function send($phone, $msg);

}
