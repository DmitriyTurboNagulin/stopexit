<?php

require_once(APP_PATH . 'models/sms/main.php');

class model_sms_smsc extends model_sms_main {

	public function balance() {
		$data = array(
			'url' => 'http://smsc.ua/sys/balance.php?login=' . $this->setting['sms_login'] . '&psw=' . $this->setting['sms_pass'],
		);
		$res = $this->curl($data);
		if (empty($res)) {
			$res = NULL;
		}
		if (strpos($res, 'ERROR') === FALSE) {
			return $res . ' <i rel="tooltip" data-placement="top" data-original-title="Валюта указана в настройках сервиса SMS рассылок" class="fa fa-question-circle"></i>';
		} else {
			return FALSE;
		}
	}

	public function send($phone, $msg) {
		$msg = urlencode($msg);
		$data = array(
			'url' => 'http://smsc.ua/sys/send.php?login=' . $this->setting['sms_login'] . '&psw=' . $this->setting['sms_pass'] . '&sender=' . $this->setting['sms_alphaname'] . '&phones=' . $phone . '&mes=' . $msg . '&charset=utf-8',
		);
		$res = $this->curl($data);
		if (strpos($res, 'OK - ') !== FALSE and strpos($res, 'SMS, ID') !== FALSE) {
			return 1;
		} else {
			return 0;
		}
	}

	public function curl($data = array()) {
		if (empty($data['url'])) {
			return FALSE;
		}
		$ch = curl_init();
		// GET запрос указывается в строке URL
		curl_setopt($ch, CURLOPT_URL, $data['url']);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Netpeak StopExit');
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}

}
