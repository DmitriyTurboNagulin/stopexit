<?php

use \Crypt;

class Settings_model extends Model {
	protected $table = 'settings';
	protected $primaryKey = 'setting_id';
	protected $fillable = array('setting_key', 'setting_value');
	public $timestamps = TRUE;

	private $field = array();
	private $sms_field = array();
	private $mail_field = array();

	public function __construct($attributes = array()) {
		$this->field = array(
			'reciever_email' => l('E-mail получателя'),
			'sender_name' => l('Имя отправителя'),
			'sender_email' => l('E-mail отправителя'),
			'smtp_status' => l('Рассылка через SMTP'),
			'smtp_server' => l('SMTP сервер'),
			'smtp_port' => l('Порт'),
			'smtp_login' => l('Логин от SMTP сервера'),
			'smtp_password' => l('Пароль от SMTP сервера'),
			'sms_status' => l('SMS рассылка'),
			'sms_service' => l('SMS рассылка'),
			'sms_reciever' => l('Получатель SMS'),
			'sms_alphaname' => l('Альфа-имя'),
			'sms_login' => l('Логин от SMS сервера'),
			'sms_pass' => l('Пароль от SMS сервера'),
			'sms_api' => l('API key'),
			'sms_test' => 1,
			'call_mask' => l('Маска поля ввода номера'),
		);

		$this->sms_field = array(
			'sms_status' => l('статус SMS'),
			'sms_service' => l('сервис SMS'),
			'sms_reciever' => l('Получатель SMS'),
			'sms_alphaname' => l('Альфа-имя'),
			'sms_login' => l('Логин от SMS сервера'),
			'sms_pass' => l('Пароль от SMS сервера'),
			'sms_api' => l('API key'),
			'sms_test' => 1,
		);

		$this->mail_field = array(
			'reciever_email' => l('E-mail получателя'),
			'sender_name' => l('Имя отправителя'),
			'sender_email' => l('E-mail отправителя'),
			'smtp_status' => l('Рассылка через SMTP'),
			'smtp_server' => l('SMTP сервер'),
			'smtp_port' => l('Порт'),
			'smtp_login' => l('Логин от SMTP сервера'),
			'smtp_password' => l('Пароль от SMTP сервера'),
		);

		parent::__construct($attributes);
	}

	public function getField($type = null) {
		if ($type == 'sms') {
			return $this->sms_field;
		} elseif ($type == 'mail') {
			return $this->mail_field;
		}
		return $this->field;
	}

	public static function get_setting_by_key($key) {
		$data = static::where('setting_key', $key)->first();
		if (is_null($data)) {
			return FALSE;
		}
		return $data->setting_value;
	}

	public static function get_settings_by_keys($keys = array()) {
		$ret = array();
		$keys = array_filter((array) $keys, 'strlen');
		if (empty($keys)) {
			return $ret;
		}

		$query = static::select(array('setting_key', 'setting_value'))->whereIn('setting_key', $keys)->get();
		$ret = array();
		foreach ($query->toArray() as $value) {
			$v_key = $value['setting_key'];
			$ret[$v_key] = $value['setting_value'];
		}
		return $ret;
	}

	public static function setting_add($key, $value) {
		$settings_model = new static();
		$settings_model->setting_key = $key;
		$settings_model->setting_value = (string) $value;
		return $settings_model->save();
	}

	public static function setting_edit($key, $value) {
		if (empty($key)) {
			return FALSE;
		}
		$setting = static::where('setting_key', $key)->first();
		$setting->setting_value = $value;
		return $setting->save();
	}

	public function setSettingValueAttribute($setting_value) {
		$this->attributes['setting_value'] = Crypt::encode($setting_value);
	}

	public function getSettingValueAttribute($setting_value) {
		return Crypt::decode($setting_value);
	}

}
