<?php

class Call_model extends Model {
	protected $table = 'call';
	protected $primaryKey = 'call_id';
	protected $fillable = array('call_name');
	public $timestamps = FALSE;
    protected $softDelete = TRUE;

	public function __construct($attributes = array()) {
		parent::__construct($attributes);
	}

}
