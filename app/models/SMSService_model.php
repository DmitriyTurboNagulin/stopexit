<?php

use \Settings_model;

/**
* 
*/
class SMSService_model extends Model
{
	private $sms;
	private $service = array(
		'turbosms',
		'smsc',
		'websms',
		'smsfeedback',
		'mobizon',
		'plivo',
		'clicksms',
	);
	private $settings = array();
	

	public function __construct()
	{
		$settings_model = new Settings_model;
		$getField = $settings_model->getField('sms');
		$this->settings = $settings_model->get_settings_by_keys(array_keys($getField));
	}

	public function balance($clear = FALSE) {
		$cacheBalance = STORAGE_PATH . 'cache/cacheBalance.json';
		$check = file_exists($cacheBalance);
		if ($check) {
			$filemtime = filemtime($cacheBalance);
			$filemtime1 = time() - $filemtime;
			if ($filemtime1 > 60) {
				$check = FALSE;
			}
		}
		if (!empty($filemtime1) && $filemtime1 < 60 && isset($_GET['cache'])) {
			$data = file_get_contents($cacheBalance);
			$data = json_decode($data, 1);
			$balance = $data['balance'];
			return $balance;
		}
		$balance = FALSE;
		if ($check === FALSE or isset($_GET['cache']) or $clear !== FALSE) {
			@unlink($cacheBalance);
			if (!empty($this->settings['sms_service'])) {
				if (!in_array($this->settings['sms_service'], $this->service)) {
					return FALSE;
				}
				$path = APP_PATH . 'models/sms/' . $this->settings['sms_service'] . '.php';
				if (file_exists($path)) {
					require_once($path);
				}
				$class = 'model_sms_' . $this->settings['sms_service'];
				$model = new $class($this->settings);
				$balance = $model->balance();
			}
			if ($balance === FALSE) {
				return FALSE;
			}
			if (empty($balance)) {
				$balance = 'Нет данных!';
			}
			$cache = json_encode(array('balance' => $balance));
			file_put_contents($cacheBalance, $cache);
		} else {
			$data = file_get_contents($cacheBalance);
			$data = json_decode($data, 1);
			$balance = $data['balance'];
		}
		return $balance;
	}

	public function send($phone, $msg) {
		
		$return = NULL;
		if (
			// !empty($this->settings['sms_alphaname'])
		!empty($this->settings['sms_test'])
		and $this->settings['sms_test'] == 1
		and !empty($this->settings['sms_service'])
		and !empty($phone)
		and !empty($msg)) {
			// if (SLIM_MODE == 'development') {
			// 	$return = 1;
			// } else {
				$path = APP_PATH . 'models/sms/' . $this->settings['sms_service'] . '.php';
				if (file_exists($path)) {
					require_once($path);
				}
				$class = 'model_sms_' . $this->settings['sms_service'];
				$model = new $class($this->settings);
				$return = (int) $model->send($phone, $msg);
			// }
		}
		return $return;
	}
}