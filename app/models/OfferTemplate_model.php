<?php

class OfferTemplate_model extends Model {
    protected $table = 'offer_template';
    protected $primaryKey = 'offer_template_id';
    protected $fillable = array(
        'offer_template_name',
        'offer_setting_background',
        'offer_setting_background_btn',
        'offer_setting_background_image',
        'offer_setting_color',
        'offer_setting_color_btn',
        'offer_setting_border_type',
        'offer_setting_border_color',
        'offer_setting_border_color2',
    );
    public $timestamps = FALSE;

    public function __construct($attributes = array()) {
        parent::__construct($attributes);
    }

    public function getList(){
        $fields = array(
        'offer_template_name',
        'offer_setting_background',
        'offer_setting_background_btn',
        'offer_setting_background_image',
        'offer_setting_color',
        'offer_setting_color_btn',
        'offer_setting_border_type',
        'offer_setting_border_color',
        'offer_setting_border_color2',
    );
        return $fields;
    }

}
