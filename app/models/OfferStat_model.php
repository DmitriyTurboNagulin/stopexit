<?php

class OfferStat_model extends Model {
	protected $table = 'offer_stat';
	protected $primaryKey = 'offer_stat_id';
	protected $fillable = array(
        'offer_views',
        'offer_yes',
        'offer_no',
        'offer_id',
    );
	public $timestamps = TRUE;

	public function __construct($attributes = array()) {
		parent::__construct($attributes);
	}

}
