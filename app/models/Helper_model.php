<?php
use Illuminate\Database\Capsule\Manager as DB;

class Helper_model extends Model {
	
	public static function getMigrationFile() {
		return glob(APP_PATH . 'migrations/m*.php');
	}

	public static function migrationNumberBD() {
		$migrate = 0;
		if (DB::schema()->hasTable('settings') !== FALSE) {
			$setting = \Settings_model::get_settings_by_keys('migrations');
			if (!empty($setting['migrations'])) {
				$migrate = $setting['migrations'];
			}
		}
		return $migrate;
	}

	public static function lastMigrationNumber() {
		$glob = self::getMigrationFile();
		if (empty($glob)) {
			return 0;
		}
		return count($glob);
	}

	public static function setFKCheckOff() {
		switch(DB::getDriverName()) {
			case 'mysql':
				DB::statement('SET FOREIGN_KEY_CHECKS=0');
				break;
			case 'sqlite':
				DB::statement('PRAGMA foreign_keys = OFF');
				break;
		}
	}

	public static function setFKCheckOn() {
		switch(DB::getDriverName()) {
			case 'mysql':
				DB::statement('SET FOREIGN_KEY_CHECKS=1');
				break;
			case 'sqlite':
				DB::statement('PRAGMA foreign_keys = ON');
				break;
		}
	}

}
