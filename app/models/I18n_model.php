<?php

class I18n_model extends Model {
	private static $i18n = array();
	private static $lang;
	private static $allow_lang = array(
		'ru',
		'en',
		'bg',
	);

	public static function init() {
		self::$lang = LANG;
		if (!empty($_COOKIE['lang']) and in_array($_COOKIE['lang'], self::$allow_lang)) {
			self::$lang = $_COOKIE['lang'];
		}
		$i18n = array();
		if (self::$lang != 'ru') {
			include(APP_PATH . '/i18n/' . self::$lang . '.php');
		}
		self::$i18n = $i18n;
	}

	public static function getLang() {
		return self::$lang;
	}

	public static function getAllowLang() {
		return self::$allow_lang;
	}

	public static function l($key) {
		if (!empty(self::$i18n[$key])) {
			$return = self::$i18n[$key];
		} else {
			// генерация файла для перевода
			if (SLIM_MODE == 'development' and !empty(self::$lang) and self::$lang != 'ru') {
				$k = str_replace("'", "\'", $key);
				file_put_contents(APP_PATH . '/i18n/' . self::$lang . '.php', '$i18n[\'' . $k . '\'] = \'' . $k . '\';' . "\n", FILE_APPEND);
			}
			$return = $key;
		}
		return $return;
	}

	public static function str_replace_first($search_for, $replace_with, $in) {
		$pos = strpos($in, $search_for);
		if ($pos === FALSE) {
			return $in;
		} else {
			return substr($in, 0, $pos) . $replace_with . substr($in, $pos + strlen($search_for), strlen($in));
		}
	}

}
