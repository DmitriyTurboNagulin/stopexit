<?php

use \Crypt;

class OfferSetting_model extends Model {
	protected $table = 'offer_setting';
	protected $primaryKey = 'offer_setting_id';
	protected $fillable = array(
		'offer_setting_email',
		'offer_setting_text',
		'offer_setting_text_size',
		'offer_setting_logo',
		'offer_setting_company_size',
		'offer_setting_company',
		'offer_setting_free_size',
		'offer_setting_yes_size',
		'offer_setting_background_image',
		'offer_setting_background_size',
		'offer_setting_free',
		'offer_setting_yes',
		'offer_setting_yes_link',
		'offer_setting_no',
		'offer_setting_background',
		'offer_setting_background_btn',
		'offer_setting_color',
		'offer_setting_color_btn',
		'offer_setting_border_type',
		'offer_setting_border_color',
		'offer_setting_border_color2',
		'offer_setting_width',
		'offer_setting_height',
		'offer_setting_view',
		'offer_setting_event',
		'offer_setting_x_sec',
		'offer_setting_position',
		'offer_setting_effect',
		'offer_setting_templates',
		'offer_setting_templates_black',
		'offer_setting_templates_white',
		'offer_setting_domain',
		'offer_setting_days',
		'offer_setting_start',
		'offer_setting_stop',
		'offer_setting_view',
		'offer_setting_view_x_days',
		'offer_id',
		'anchor_id',
	);
	public $timestamps = FALSE;
	protected $softDelete = TRUE;

	public function __construct($attributes = array()) {
		parent::__construct($attributes);
	}

	public static function getDays() {
		$days = array(
			'Mon' => l('Понедельник'),
			'Tue' => l('Вторник'),
			'Wed' => l('Среда'),
			'Thu' => l('Четверг'),
			'Fri' => l('Пятница'),
			'Sat' => l('Суббота'),
			'Sun' => l('Воскресенье'),
		);
		return $days;
	}

	public function style($data) {
		if (!$data) {
			return array();
		}
		if ($data['offer_setting_border_type'] == 'double') {
			$data['border'] = 'outline:2px solid ' . $data['offer_setting_border_color2'] .';border:2px solid ' . $data['offer_setting_border_color'] .';';
		} else {
			$data['border'] = 'border:2px ' . $data['offer_setting_border_type'] . ' ' . $data['offer_setting_border_color'] .';';
		}
		if (!empty($data['offer_setting_start'])) {
			$tmp = strtotime($data['offer_setting_start']);
			// 1451602800 это timestamp для первого января 2016 года
			if ($tmp > 1451602800) {
				$data['offer_setting_start'] = date('d-m-Y', $tmp);
			} else {
				$data['offer_setting_start'] = NULL;
			}
		}
		if (!empty($data['offer_setting_stop'])) {
			$tmp = strtotime($data['offer_setting_stop']);
			if ($tmp > 1451602800) {
				$data['offer_setting_stop'] = date('d-m-Y', $tmp);
			} else {
				$data['offer_setting_stop'] = NULL;
			}
		}
		if (!empty($data['offer_setting_days'])) {
			$data['offer_setting_days'] = json_decode($data['offer_setting_days'], TRUE);
		} else {
			$data['offer_setting_days']  = NULL;
		}
		switch ($data['offer_setting_position']) {
			case 'left_top':
				$data['position'] = 'left:10px;top:10px;';
			break;
			case 'left_bottom':
				$data['position'] = 'left:10px;bottom:10px;';
			break;
			case 'right_top':
				$data['position'] = 'right:10px;top:10px;';
			break;
			case 'right_bottom':
				$data['position'] = 'right:10px;bottom:10px;';
			break;
			default:
				$data['position'] = 'left:50%;top:50%;margin:-' . $data['offer_setting_height'] / 2 . 'px 0 0 -' . $data['offer_setting_width'] / 2 . 'px;';
			break;
		}
		if (!empty($data['offer_setting_background_image'])) {
			switch ($data['offer_setting_background_size']) {
				case 'repeat':
					$data['background_setting'] = '';
				break;
				case 'top':
					$data['background_setting'] = 'background-size:100%;background-repeat:no-repeat;
					background-position:top center;';
				break;
				case 'mid':
					$data['background_setting'] = 'background-size:100%; background-repeat:no-repeat;
					background-position:center;';
				break;
				case 'bottom':
					$data['background_setting'] = 'background-size:100%; background-repeat:no-repeat;
					background-position:bottom center;';
					break;
				default:
					$data['background_setting'] = 'background-size:cover;';
			}
		}
		if (!empty($data['offer_setting_effect'])) {
			switch ($data['offer_setting_effect']) {
				case 'instantly':
					$data['speed'] = '0';
					break;
				case 'slow':
					$data['speed'] = '1.5';
					break;
			}
		}
		$data['remove'] = "this.parentNode.parentNode.parentNode.style.display='none'";
		return $data;
	}

	public function getOfferSettingEmailAttribute($offer_setting_email) {
		return Crypt::decode($offer_setting_email);
	}


	public function getOfferSettingCompanyAttribute($offer_setting_company) {
		return Crypt::decode($offer_setting_company);
	}

	public function getOfferSettingTextAttribute($offer_setting_text) {
		return Crypt::decode($offer_setting_text);
	}

}
