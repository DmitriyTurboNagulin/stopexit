<?php 

use \Settings_model;

class Mail_model extends Model {

    private $charset;
    private $additional_headers;
    private $setting = array();

    function __construct() {
        $this->charset = 'utf-8';
        $settings_model = new Settings_model;
        $getField = $settings_model->getField('mail');
        $this->setting = $settings_model->get_settings_by_keys(array_keys($getField));
        if (SLIM_MODE == 'development') {
             //Если Вы хотите видеть сообщения ошибок, укажите TRUE вместо FALSE
            $this->setting['smtp_debug'] = TRUE;
        }
        $from = '=?' . $this->charset . '?b?' . base64_encode($this->setting["sender_name"]) . '?=<' . $this->setting["sender_email"] . '>';
        $this->additional_headers = 'From: ' . $from . "\r\n" .
                                    'Reply-To: ' . $from . "\r\n" .
                                    'MIME-Version: 1.0' . "\r\n" .
                                    'Content-Type: text/html; charset=' . $this->charset;
    }

    private function mail($to, $subject, $message) {
        $subject = '=?' . $this->charset . '?b?' . base64_encode($subject) . '?=';
        $return = mail($to, $subject, $message, $this->additional_headers);
        if (SLIM_MODE == 'development') {
            return TRUE;
        }
        return $return;
    }

    private function smtpmail($mail_to, $subject, $message) {
        $SEND = "Date: " . date("D, d M Y H:i:s") . " UT\r\n";
        $SEND .= 'Subject: =?' . $this->charset . '?B?' . base64_encode($subject) . "=?=\r\n";
        $SEND .= $this->additional_headers . "\r\n\r\n";
        $SEND .= $message . "\r\n";
         if(!$socket = fsockopen($this->setting['smtp_server'], $this->setting['smtp_port'], $errno, $errstr, 30) ) {
            if (!empty($this->setting['smtp_debug'])) echo $errno . "<br>" . $errstr;
            return NULL;
        }
        if (!$this->server_parse($socket, "220", __LINE__)) return FALSE;
        fputs($socket, "HELO " . $this->setting['smtp_server'] . "\r\n");
        if (!$this->server_parse($socket, "250", __LINE__)) {
            if (!empty($this->setting['smtp_debug'])) echo '<p>Не могу отправить HELO!</p>';
            fclose($socket);
            return NULL;
        }
        fputs($socket, "AUTH LOGIN\r\n");
        if (!$this->server_parse($socket, "334", __LINE__)) {
            if (!empty($this->setting['smtp_debug'])) echo '<p>Не могу найти ответ на запрос авторизаци.</p>';
            fclose($socket);
            return NULL;
        }
        fputs($socket, base64_encode($this->setting['smtp_login']) . "\r\n");
        if (!$this->server_parse($socket, "334", __LINE__)) {
            if (!empty($this->setting['smtp_debug'])) echo '<p>Логин авторизации не был принят сервером!</p>';
            fclose($socket);
            return NULL;
        }
        fputs($socket, base64_encode($this->setting['smtp_password']) . "\r\n");
        if (!$this->server_parse($socket, "235", __LINE__)) {
            if (!empty($this->setting['smtp_debug'])) echo '<p>Пароль не был принят сервером как верный! Ошибка авторизации!</p>';
            fclose($socket);
            return NULL;
        }
        fputs($socket, "MAIL FROM: <" . $this->setting['smtp_login'].">\r\n");
        if (!$this->server_parse($socket, "250", __LINE__)) {
            if (!empty($this->setting['smtp_debug'])) echo '<p>Не могу отправить комманду MAIL FROM: </p>';
            fclose($socket);
            return NULL;
        }
        fputs($socket, "RCPT TO: <" . $mail_to . ">\r\n");
        if (!$this->server_parse($socket, "250", __LINE__)) {
            if (!empty($this->setting['smtp_debug'])) echo '<p>Не могу отправить комманду RCPT TO: </p>';
            fclose($socket);
            return FALSE;
        }
        fputs($socket, "DATA\r\n");
        if (!$this->server_parse($socket, "354", __LINE__)) {
            if (!empty($this->setting['smtp_debug'])) echo '<p>Не могу отправить комманду DATA</p>';
            fclose($socket);
            return NULL;
        }
        fputs($socket, $SEND . "\r\n.\r\n");
        if (!$this->server_parse($socket, "250", __LINE__)) {
            if (!empty($this->setting['smtp_debug'])) echo '<p>Не смог отправить тело письма. Письмо не было отправленно!</p>';
            fclose($socket);
            return NULL;
        }
        fputs($socket, "QUIT\r\n");
        fclose($socket);
        return TRUE;
    }

    private function server_parse($socket, $response, $line = __LINE__) {
        while (@substr($server_response, 3, 1) != ' ') {
            if (!($server_response = fgets($socket, 256))) {
                if (!empty($this->setting['smtp_debug'])) {
                    echo "<p>Проблемы с отправкой почты!</p>$response<br>$line<br>";
                }
                return FALSE;
            }
        }
        if (!(substr($server_response, 0, 3) == $response)) {
            if (!empty($this->setting['smtp_debug'])) echo "<p>Проблемы с отправкой почты!</p>$response<br>$line<br>";
            return FALSE;
        }
        return TRUE;
    }

    public function sendmail($to, $subject, $message) {
        // if (SLIM_MODE == 'development') {
        //     $return = 1;
        // } else {
            if ($this->setting['smtp_status'] == 'on') {
                $return = $this->smtpmail($to, $subject, $message);
                var_dump($return);die;
            } else {
                $return = $this->mail($to, $subject, $message);
            }
        // }
        return $return;
    }

}
