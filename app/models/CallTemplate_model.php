<?php

class CallTemplate_model extends Model {
	protected $table = 'call_template';
	protected $primaryKey = 'call_template_id';
	protected $fillable = array(
		'call_template_name',
		'call_setting_background',
		'call_setting_background_btn',
		'call_setting_background_image',
		'call_setting_color',
		'call_setting_color_btn',
		'call_setting_border_type',
		'call_setting_border_color',
		'call_setting_border_color2',
		'call_setting_manager_position',
		'call_setting_manager_photo',
		'call_setting_photo_position',
		'call_setting_radius',
	);
	public $timestamps = FALSE;

	public function __construct($attributes = array()) {
		parent::__construct($attributes);
	}

	public function getList() {
		$fields = array(
			'call_template_name',
			'call_setting_background',
			'call_setting_background_btn',
			'call_setting_background_image',
			'call_setting_color',
			'call_setting_color_btn',
			'call_setting_border_type',
			'call_setting_border_color',
			'call_setting_border_color2',
			'call_setting_manager_position',
			'call_setting_manager_photo',
			'call_setting_photo_position',
			'call_setting_radius',
		);
		return $fields;
	}

}