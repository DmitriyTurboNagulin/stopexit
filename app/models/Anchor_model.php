<?php

class Anchor_model extends Model {
    protected $table = 'anchor';
    protected $primaryKey = 'anchor_id';
    protected $fillable = array('anchor_name');
    public $timestamps = FALSE;
    protected $softDelete = TRUE;

    public function __construct($attributes = array()) {
        parent::__construct($attributes);
    }

}
