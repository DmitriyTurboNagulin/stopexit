<?php

class Offer_model extends Model {
	protected $table = 'offer';
	protected $primaryKey = 'offer_id';
	protected $fillable = array(
        'offer_name',
        );
	public $timestamps = FALSE;
    protected $softDelete = TRUE;

	public function __construct($attributes = array()) {
		parent::__construct($attributes);
	}

}
