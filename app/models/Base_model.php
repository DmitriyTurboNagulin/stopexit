<?php

use \Settings_model;

class Base_model {

    public static $all_groups = array(
        'company',  // Сотрудник компании
        'client',   // Клиент
        'contact',  // Контакт клиента
        'guest',    // Гость
    );

    public static $default_group = 'guest';

    public static function user_access() {
        if (isset($_SESSION['client_access']) and $_SESSION['client_access'] == 1) {
            return TRUE;
        }
        return FALSE;
    }

    public static function user_is_auth() {
        $auth_token = static::get_auth_token();
        $app_enable = static::check_app_enable();
        return (isset($_SESSION['auth']) && $_SESSION['auth'] == $auth_token && $app_enable) ? TRUE : FALSE;
    }

    public static function get_user_group() {
        $all_groups = self::$all_groups;

        $user_group = self::$default_group;
        if (isset($_SESSION['group'])) {
            if (in_array($_SESSION['group'], $all_groups)) {
                $user_group = $_SESSION['group'];
            }
        }
        return $user_group;
    }

    /**
     * Установка сессии пользователя
     *
     * @param array $params параметры
     * array $params['email'] почта пользователя
     * array $params['group'] группа пользователя
     */
    public static function set_user_auth($params = array()) {
        $all_groups = self::$all_groups;
        $client_access = 0;
        if (empty($params['email'])) {
            return FALSE;
        }
        $user_group = self::$default_group;
        if (isset($params['group']) && in_array($params['group'], $all_groups)) {
            $user_group = $params['group'];
        }
        if ($user_group == 'company') {
            $client_access = 1;
        }
        if ($user_group == 'client' or $user_group == 'contact') {
            $client_access = Settings_model::get_settings_by_keys('client_access');
            if (!empty($client_access['client_access'])) {
                $client_access = 1;
            }
        }
        $_SESSION['client_access'] = $client_access;
        $_SESSION['email'] = $params['email'];
        $_SESSION['group'] = $user_group;
        $_SESSION['auth'] = static::get_auth_token();
        return TRUE;
    }

    public static function get_user() {
        if (self::user_is_auth()) {
            return $_SESSION['email'];
        }
        return FALSE;
    }

    public static function logout() {
        unset($_SESSION['auth']);
        session_destroy();
        return TRUE;
    }

    public static function get_auth_token() {
        return substr(md5(INSTALL_EXPIRE_DATE_YMD), 0, 10);
    }

    public static function check_app_enable() {
        $settings_model = new Settings_model();
        $tmp_settings = $settings_model->get_settings_by_keys('expire_date');
        $setting_expire_date = (isset($tmp_settings['expire_date']) ? $tmp_settings['expire_date'] : FALSE);
        $setting_expire_date_unix = strtotime($setting_expire_date);
        return ($setting_expire_date_unix > time());
    }

    public static function get_copyright() {
        $company_copyright = Cache::crypt_get('company_copyright');
        if (is_null($company_copyright)) {
            $company_copyright = Settings_model::get_setting_by_key('company_copyright');
            if ($company_copyright === FALSE) {
                $company_copyright = '';
            } else {
                Cache::crypt_put('company_copyright', $company_copyright, '60'); // 60 m
            }
        }
        return $company_copyright;
    }

}