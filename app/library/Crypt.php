<?php if (!defined('APP_PATH')) exit('No direct script access allowed');

use \GibberishAES;
use \Config;

class Crypt {

	public static function encode($str) {
		GibberishAES::size(Config::get('crypt.size'));
		return GibberishAES::enc($str, APP_SECRET);
	}

	public static function decode($str) {
		GibberishAES::size(Config::get('crypt.size'));
		return GibberishAES::dec($str, APP_SECRET);
	}

}
