<?php
/**
 * jsonAPI - Slim extension to implement fast JSON API's
 *
 * @package Slim
 * @subpackage View
 * @author Jonathan Tavares <the.entomb@gmail.com>
 * @license GNU General Public License, version 3
 * @filesource
 *
 *
*/
use \App;
use \View;
/**
 * JsonApiView - view wrapper for json responses (with error code).
 *
 * @package Slim
 * @subpackage View
 * @author Jonathan Tavares <the.entomb@gmail.com>
 * @license GNU General Public License, version 3
 * @filesource
 */
class JsonApiView extends \Slim\View {

	/**
	 * Bitmask consisting of <b>JSON_HEX_QUOT</b>,
	 * <b>JSON_HEX_TAG</b>,
	 * <b>JSON_HEX_AMP</b>,
	 * <b>JSON_HEX_APOS</b>,
	 * <b>JSON_NUMERIC_CHECK</b>,
	 * <b>JSON_PRETTY_PRINT</b>,
	 * <b>JSON_UNESCAPED_SLASHES</b>,
	 * <b>JSON_FORCE_OBJECT</b>,
	 * <b>JSON_UNESCAPED_UNICODE</b>.
	 * The behaviour of these constants is described on
	 * the JSON constants page.
	 * @var int
	 */
	public $encodingOptions = 0;

	/**
	 * Content-Type sent through the HTTP header.
	 * Default is set to "application/json",
	 * append ";charset=UTF-8" to force the charset
	 * @var string
	 */
	public $contentType = 'application/json';

	public function render($status = 200, $data = NULL) {
		$status = intval($status);
		$json = array(
			'secure' => TRUE,
			'status' => $status,
			'data' => array(),
		);
		$response = $this->all();
		if (isset($response['data'])) {
			$json['data'] = $response['data'];
		}
		if ($this->has('error')) {
			$json['error'] = TRUE;
		}
		if (!$this->has('secure')) {
			$json['secure'] = FALSE;
		}
		if ($this->has('error_message')) {
			$json['error_message'] = $response['error_message'];
		}

		//add flash messages
		if (isset($this->data->flash) && is_object($this->data->flash)) {
			$flash = $this->data->flash->getMessages();
			if (count($flash)) {
				$json['flash'] = $flash;
			}
		}

		App::response()->status($status);
		App::response()->header('Content-Type', $this->contentType);
		App::response()->body(json_encode($json, $this->encodingOptions));
		App::stop();
	}

	/**
	* @param array $data
	*/
	public function reset($data = array()) {
		$this->clear();
		$this->setData($data);
	}

}
