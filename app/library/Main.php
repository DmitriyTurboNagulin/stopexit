<?php if (!defined('APP_PATH')) exit('No direct script access allowed');
class Main {

	public static function base_url() {
		$path = dirname($_SERVER['SCRIPT_NAME']);
		$path = trim($path, '/');
		$baseUrl = Request::getUrl();
		$baseUrl = trim($baseUrl, '/');
		return $baseUrl . '/' . $path . ($path ? '/' : '');
	}

	public static function framework_location() {
		$path = dirname($_SERVER['SCRIPT_NAME']);
		$path = trim($path, '/') . '/';
		return '/' . $path;
	}

	public static function get_database_name_file() {
		$host = Main::get_host();
		$host = ltrim('www.');
		$host_hash = md5($host);
		$config_name = 'database_' . substr($host_hash, 0, 10) . '.php';
		return $config_name;
	}

	public static function get_sqlite_database($database_name) {
		return STORAGE_PATH . 'db/' . $database_name . '.sqlite';
	}

	public static function pre_make_connection_database($config) {
		if (isset($config['driver']) && $config['driver'] == 'sqlite') {
			$config['database'] = Main::get_sqlite_database($config['database']);
		}
		return $config;
	}

	public static function get_host() {
		if (isset($_SERVER['HTTP_HOST'])) {
			if (strpos($_SERVER['HTTP_HOST'], ':') !== FALSE) {
				$hostParts = explode(':', $_SERVER['HTTP_HOST']);
				return $hostParts[0];
			}
			return $_SERVER['HTTP_HOST'];
		}
		return $_SERVER['SERVER_NAME'];
	}

	/**
	 * Get Host with Port
	 * @return string
	 */
	public static function get_host_with_port() {
		$h_host = Main::get_host();
		$h_port = Main::get_port();
		if ($h_port != '80' && $h_port != '0') {
			return sprintf('%s:%s', $h_host, $h_port);
		}
		return $h_host;
	}

	/**
	 * Get Port
	 * @return int
	 */
	public static function get_port() {
		return (int) $_SERVER['SERVER_PORT'];
	}

}
