<?php if (!defined('APP_PATH')) exit('No direct script access allowed');

$config['aliases'] = array(
	'Slim' => 'Slim\Slim',
	'Middleware' => 'Slim\Middleware',
	'Model' => 'Illuminate\Database\Eloquent\Model',
	'Cache' => 'udpup\Facade\CacheFacade',
	'App' => 'SlimFacades\App',
	'Menu' => 'udpup\Facade\MenuManagerFacade',
	'Config' => 'SlimFacades\Config',
	'Input' => 'SlimFacades\Input',
	'Log' => 'SlimFacades\Log',
	'Request' => 'SlimFacades\Request',
	'View' => 'SlimFacades\View',
	'Response' => 'udpup\Facade\ResponseFacade',
	'Route' => 'udpup\Facade\RouteFacade',
	'Module' => 'udpup\Facade\ModuleManagerFacade',
	'GibberishAES' => 'GibberishAES\GibberishAES',
	'HttpBasicAuth' => 'Slim\Extras\Middleware\HttpBasicAuth',
	'StrongAuth' => 'Slim\Extras\Middleware\StrongAuth',
);
$config['cache'] = array(
	'cache.driver' => 'file',
	'cache.path' => STORAGE_PATH . 'cache/',
	'cache.prefix' => APP_NAME,
);
$config['cookies'] = array(
	'expires' => '120 minutes',
	'path' => '/',
	'domain' => NULL,
	'secure' => FALSE,
	'httponly' => FALSE,
	'name' => 'slim_session',
);
$config['log'] = array(
	'path' => STORAGE_PATH . 'logs',
	'name_format' => 'Y-m-d',
	'message_format' => '%date% - %message%',
);

$config['openid'] = array(
	'realm' => Main::get_host_with_port(),
	'identity' => '',
	'required' => array(
		'contact/email'
	),
	'optional' => array(),
);
$config['slim'] = array(
	'modular' => TRUE,
	'mode' => SLIM_MODE,
	'debug' => FALSE,
	'log.level' => \Slim\Log::DEBUG,
	'log.enabled' => TRUE,
	'view' => new \Slim\Views\Twig(),
	'templates.path' => APP_PATH . 'views',
	'http.version' => '1.1',
	'routes.case_sensitive' => FALSE,
);
$config['twig'] = array(
	'debug' => FALSE,
	'cache' => STORAGE_PATH . 'cache_template',
);

return $config;
