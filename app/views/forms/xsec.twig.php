setTimeout(function() {
	{{ clear | raw }}
	document.getElementsByTagName("body")[0].innerHTML += "
		<div class=\"{{ displayClass }}\" style=\"position:fixed;left:0;top:0;z-index:2147483645;width:100%;height:100%;background:rgba(0,0,0,0.6);\">
			{{ view | raw }}
		</div>";
		opacity(document.getElementsByClassName("{{ class }}")[0]);
        postProceed({{event_id}}, "{{class}}");
        sendStatGA("{{action}}", "{{class}}{{event_id}}", "{{interaction}}");
}, {{ timeout * 1000 }});