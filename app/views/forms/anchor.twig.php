{{ clear | raw }}
function send() {
	sendStatGA("{{action}}", "{{class}}{{event_id}}", "{{interaction}}");
	show("{{ displayClass }}");
}
document.getElementsByTagName("body")[0].innerHTML += "
		<div class=\"{{ anchorClass }}\" id=\"{{ anchorClass }}{{ anchor_setting_id }}\" onclick=show(\"{{ displayClass }}\") draggable=\"true\" style=\"display:block; height:{{anchor_setting_size + 135}}px;width:{{anchor_setting_size + 135}}px;position:fixed;{{anchor_setting_position}}\" >
			{{ view_anchor | raw }}
		</div>
		<div class=\"{{ displayClass }}\" style=\"position:fixed;display:none;left:0;top:0;z-index:2147483645;width:100%;height:100%;background:rgba(0,0,0,0.6);\">
			{{ view | raw }}
		</div>";
		document.getElementsByClassName("{{ class }}")[0].style.opacity = 1;
		postProceed({{event_id}}, "{{class}}");
		var element_{{event_id}} = document.getElementById("{{ anchorClass }}{{ anchor_setting_id }}");
		setTimeout("drag_n_drop(\"{{ anchorClass }}{{ anchor_setting_id }}\")", 1000);
