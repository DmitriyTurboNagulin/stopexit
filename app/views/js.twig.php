function stopexit_ajax(callback) {
	var xhr = new XMLHttpRequest();
	var body = 'href=' + encodeURIComponent(window.location.href);
	xhr.open('POST', '/stopexit/show/', true);
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			callback(this.responseText);
		}
	};
	xhr.send(body);
}

function postProceed(id, setclass) {
	var xhr = new XMLHttpRequest();
	var body = 'type=' + setclass + '&id=' + id;
	xhr.open('POST', '/stopexit/proceed/', true);
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send(body);
}

function postGetClick(id) {
	var a = event.target.id;
	if (a == 'stopexit_button' + id) {
		var body = 'type=stopexit' + '&id=' + id + '&yesclick=true';
		sendStatGA('Netpeak Stopexit', 'Pressing button', 'Yes', 'false');
		sendStatGA('Netpeak Stopexit', 'Send form', 'Done', 'false');
	} else {
		sendStatGA('Netpeak Stopexit', 'Pressing button', 'No', 'false');
		var body = 'type=stopexit' + '&id=' + id + '&noclick=true';
	}
	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/stopexit/offerCTR/', true);
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send(body);
}

function closeCount(id) {
	var a = event.target.id;
	if (a == 'stopexit_close' + id) {
		sendStatGA('Netpeak Stopexit', 'Pressing button', 'Close', 'false')
	} else {
		sendStatGA('Netpeak GetMoreCalls', 'Pressing button', 'Close', 'false')
	}
}

function stopexit_RemoveElementsByClass(className) {
	var elements = document.getElementsByClassName(className);
	while (elements.length > 0) {
		elements[0].parentNode.removeChild(elements[0]);
	}
}

function getCoords(elem) {
	var box = elem.getBoundingClientRect();
	return {
		top: box.top + pageYOffset,
		left: box.left + pageXOffset
	};
}

function drag_n_drop(elementId) {
	var element = document.getElementById(elementId);
	element.onmousedown = function(e) {
		var coords = getCoords(element);
		var shiftX = e.pageX - coords.left;
		var shiftY = e.pageY - coords.top;
		document.body.appendChild(element);
		moveAt(e);
		function moveAt(e) {
			element.style.left = e.pageX - shiftX + 'px';
			element.style.top = e.pageY - shiftY + 'px';
		};
		document.onmousemove = function(e) {
			moveAt(e);
		};
		element.onmouseup = function() {
			document.onmousemove = null;
			element.onmouseup = null;
		};
	};
	element.ondragstart = function() {
		return false;
	};
}

function postPhone(id) {
	var phone = document.getElementById('getMoreCall' + id + 'phone').value;
	if (phone.length < 6) {
		sendStatGA('Netpeak GetMoreCalls', 'Pressing button', 'Yes', 'false');
		sendStatGA('Netpeak GetMoreCalls', 'Send form', 'Error', 'false');
		document.getElementById('getcalls_error'+id).style.display='block';
	} else {
		sendStatGA('Netpeak GetMoreCalls', 'Pressing button', 'Yes', 'false');
		sendStatGA('Netpeak GetMoreCalls', 'Send form', 'Done', 'false');
		stopexit_RemoveElementsByClass("getCallsBackground");
	}
	var xhr = new XMLHttpRequest();
	var body = 'phone=' + phone + '&id=' + id;
	xhr.open('POST', '/stopexit/phone/', true);
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			var responce = this.responseText;
			if (responce) {
				console.log(responce);
			}
		}
	};
	xhr.send(body);
}


function show(target){
	document.getElementsByClassName(target)[0].style.display = 'block';
}

function hide(target){
	document.getElementsByClassName(target)[0].style.display = 'none';
}


function loadScript(url, callback)
{
	var head = document.getElementsByTagName('head')[0];
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = url;
	head.appendChild(script);
	script.onreadystatechange = callback;
	script.onload = callback;
}

function opacity(target) {
	target.style.opacity = 0;
	setTimeout(function(){
		target.style.opacity = 1;
	}, 100);
	loadScript('{{ maskSrc }}', function() {
		var elementExists = document.getElementsByClassName("getCallsBackground")[0];
		if (elementExists) {
			VMasker(document.querySelector("input")).maskPattern("{{ mask }}");
		}
	});
}

function sendStatGA(category, action, label, noninteraction) {
	if (noninteraction == 'auto') {
		noninteraction = true;
	} else {
		noninteraction = false;
	}
	if (action == 'Open form') {
		if (noninteraction == true) {
			label = label + '-auto';
		} else {
			label = label + '-icon';
		}
	}
	console.log(action, label, noninteraction);
	if (window.dataLayer) {
		dataLayer.push({
			'event':'gtm-ua-event',
			'gtm-event-category' : category,
			'gtm-event-action' : action,
			'gtm-event-label' : label,
			'gtm-event-non-interaction' : noninteraction,
		});
	} else if (typeof ga == 'function') {
		if (noninteraction == true) {
			ga('send', 'event', category, action, label, 1);
		} else {
			ga('send', 'event', category, action, label);
		}
	}
}



var userAgent = navigator.userAgent.toLowerCase();
if (userAgent.indexOf('iphone') == -1 && userAgent.indexOf('android') == -1 && userAgent.indexOf('blackberry') == -1 && userAgent.indexOf('webos') == -1 && userAgent.indexOf('mobile') == -1) {
	var data = stopexit_ajax(function(data) {
		eval(data);
	});
}