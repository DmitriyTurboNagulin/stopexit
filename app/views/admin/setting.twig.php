{% extends 'admin/index.twig.php' %}
{% block content %}
<div class="row page-header-box">
	<div class="col-xs-12">
		<h3>{{ title }}</h3>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<form method="post">
			<div class="form-group">
				<label class="control-label">{{ l('Кодировка выводимых ссылок:') }}</label>
				<select class="form-control" name="encoding">
					{% for encod in allow_encodings %}
						<option value="{{ encod }}" {% if encod == setting.encoding %}selected="selected"{% endif %}>{{ encod|upper }}</option>
					{% endfor %}
				</select>
			</div>
			<div class="form-group">
				<br>
				<input type="submit" class="btn btn-primary ajax_submit" value="{{ l('Сохранить') }}" />
			</div>
		</form>
	</div>
</div>
{% endblock %}
