<div class="stopexit" id="stopexit" style="position:absolute;opacity:0;transition:opacity {{ speed }}s;font-family:arial;{{ position }}z-index:2147483647;display:block;color:{{ offer_setting_color }};background:{{ offer_setting_border_color2 }};{{ border }}width:{{ offer_setting_width }}px;height:{{ offer_setting_height }}px;padding:0;">
	<div id="stopexit_checkout" style="position:relative;display:block;text-align:center;background:{{ offer_setting_background }} {% if offer_setting_background_image != '' %}url({{ offer_setting_background_image }}){% endif %};{{ background_setting }}margin:0;width:100%;height:100%;padding:0%;">
		<br>
		{% if offer_setting_logo != '' %}
		<div><img src="{{ offer_setting_logo }}" style="max-width:98%;margin:0px 1% 15px 1%;"></div>
		{% endif %}
		{% if offer_setting_company != '' %}
		<div style="display:block;font-size:{{ offer_setting_company_size }}px;line-height:{{ offer_setting_company_size }}px;">{{ offer_setting_company }}</div>
		{% endif %}
		<div onclick="{{ remove }}; closeCount({{ offer_id }})" id="stopexit_close{{ offer_id }}" style="display:block;cursor:pointer;position:absolute;right:0;top:0;margin:-7px -7px 0 0;color:{{ offer_setting_background }};background:{{ offer_setting_color }};height:20px;width:20px;text-align:center;border-radius:18px;font-size:20px;line-height:20px;">&times;</div>
		<div style="display:block;margin:10px 0 20px 0;font-size:{{ offer_setting_text_size }}px;line-height:{{ offer_setting_text_size }}px;text-align:center;">{{ offer_setting_text | raw }}</div>
		<div style="display:block;font-size:{{ offer_setting_free_size }}px;line-height{{ offer_setting_free_size }}px;text-align:center;">{{ offer_setting_free | raw }}</div>
		<div id ="stopexit_button{{offer_id}}" onclick="location.href='{{ offer_setting_yes_link }}';postGetClick({{ offer_id }})" style="text-align:center;line-height:{{ offer_setting_yes_size }}px;font-size:{{ offer_setting_yes_size }}px;padding:4px;cursor:pointer;border-radius:5px;background:{{ offer_setting_background_btn }};color:{{ offer_setting_color_btn }};display:block;margin:10px auto 10px auto;width:70%;">{{ offer_setting_yes | raw }}</div>
		{% if offer_setting_no != '' %}
		<span id="stopexit_no{{offer_id}}" onclick="{{ remove }}; postGetClick({{ offer_id }})" style="display:inline;text-decoration:underline;cursor:pointer;">{{ offer_setting_no | raw }}</span>
		{% endif %}
	</div>
</div>
