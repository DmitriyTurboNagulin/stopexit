{% extends 'main.twig.php' %}
{% block body %}
	<div id="wrapper">
		{{ include('admin/topbar.twig.php') }}
		{{ include('admin/sidebar.twig.php') }}
		{{ style | raw }}
		<div id="page-wrapper">
			<div class="row">
				<h3>{{ header }}</h3>
				<button class="btn btn-default btn-xs" style="margin-bottom:10px;" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">{{ l('История редактирования') }}</button>
				<div class="collapse" id="collapseExample">
					<div class="well">
						<div class="row">
							<div class="col-md-4">
								<div class="hidden" id="showAnchor">{{ showAnchor }}</div>
									{% for trash in trashed %}
										<a class="showDraft" data-data="{{ trash.data }}" href="#{{ trash.id }}" data-id="{{ trash.id }}">{{ trash.email }}: {{ trash.deleted }}</a><br>
									{% endfor %}
								</div>
								<div class="col-md-8">
									<div id="showText">
										<button id="checkDraft" class="btn btn-xs btn-default">{{ l('Применить') }}</button>
										<p>{{ l('Будут измнеены все настройки формы: дизайн, текст, видимость и временя показа') }}</p>
									</div>
									<div id="show2" style="position:relative; height:250px; border: 2px"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="hidden" id="showAnchHistory">{{ showAnchHistory }}</div>
				<div class="row" data-sticky_parent>
					<form action="{{ anchSave }}" id="anchSave" method="post" data-url="{{ showAnchor }}">
						<div class="col-md-4" data-sticky_column>
						{% set btn %}
							<div class="form-group">
								<p class="text-muted draft">{{ l('Сохранено в черновиках, время:') }} <span></span></p>
								<input type="submit" class="btn btn-xs btn-primary cancelDraft" value="{{ l('Сохранить') }}">
								<a href="{{ cancelSave }}" class="btn btn-xs btn-default cancelDraft">{{ l('Отменить') }}</a>
								<input type="button" class="btn btn-xs btn-info saveToDraft" value="{{ l('В черновик') }}">
							</div>
						{% endset %}
						{% if access == true %} {{ btn }} {% endif %}
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#style" aria-controls="style" role="tab" data-toggle="tab">{{ l('Дизайн') }}</a></li>
							<li role="presentation"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">{{ l('Видимость и показ') }}</a></li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="row tab-pane active" id="style">
								<input type="hidden" id="anchor_id" name="anchor_id" value="{{ anchor.anchor_id }}">
								<div class="form-group col-sm-12 col-md-12">
									<div class="form-group">
										<label>{{ l('Название иконки') }}</label>
										<input type="text" class="form-control" name="anchor_name" value="{{ anchor.anchor_name }}" placeholder="{{ l('Название иконки') }}">
									</div>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Вид иконки') }}</label>
									<select class="form-control" name="anchor_setting[anchor_setting_icon]" id="icons">
										<option value="whatsapp-round" {% if anchor_setting.anchor_setting_icon == 'whatsapp-round' %}selected{% endif %}>{{ l('Whatsapp') }}</option>
										<option value="viber" {% if anchor_setting.anchor_setting_icon == 'viber' %}selected{% endif %}>{{ l('Viber') }}</option>
										<option value="greenphone" {% if anchor_setting.anchor_setting_icon == 'greenphone' %}selected{% endif %}>{{ l('Phone') }}</option>
										<option value="firephone" {% if anchor_setting.anchor_setting_icon == 'firephone' %}selected{% endif %}>{{ l('Firephone') }}</option>
										<option value="viber2" {% if anchor_setting.anchor_setting_icon == 'viber2' %}selected{% endif %}>{{ l('Viber №2') }}</option>
										<option value="whatsapp" {% if anchor_setting.anchor_setting_icon == 'whatsapp' %}selected{% endif %}>{{ l('Whatsapp2') }}</option>
										<option value="consult-male" {% if anchor_setting.anchor_setting_icon == 'consult-male' %}selected{% endif %}>{{ l('Консультант(мужчина)') }}</option>
										<option value="comments" {% if anchor_setting.anchor_setting_icon == 'comments' %}selected{% endif %}>{{ l('Сообщение') }}</option>
										<option value="fax" {% if anchor_setting.anchor_setting_icon == 'fax' %}selected{% endif %}>{{ l('Факс') }}</option>
										<option value="consult-female" {% if anchor_setting.anchor_setting_icon == 'consult-female' %}selected{% endif %}>{{ l('Консультант(женщина)') }}</option>
										<option value="analog" {% if anchor_setting.anchor_setting_icon == 'analog' %}selected{% endif %}>{{ l('Аналоговый телефон') }}</option>
										<option value="cellphone" {% if anchor_setting.anchor_setting_icon == 'cellphone' %}selected{% endif %}>{{ l('Мобильный телефон') }}</option>
									</select>
								</div>
								<div class="form-group col-md-12" style="margin-bottom:0;">
									<div class="form-group">
										<label>{{ l('Логотип') }}</label>
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-upload chooseImage" style="cursor:pointer;" id="logo-ref" data-inp="anchor_setting_icon_ref"></i>
											</div>
											<input type="text" name="anchor_setting[anchor_setting_icon_ref]" value="{{ anchor_setting.anchor_setting_icon_ref }}" class="form-control">
										</div>
										<button id="clear" type="button" class="btn btn-link"><i class="fa fa-trash"></i> Очистить</button>
									</div>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Размер иконки') }}</label>
									<div style="margin-bottom:0;" class="form-group col-md-10">
										<input name="anchor_setting[anchor_setting_size]"  id="size" type="text" data-slider-min="40" data-slider-max="100" data-slider-step="1" data-slider-value="{{ anchor_setting.anchor_setting_size }}"/>
									</div>
								</div>
								<div class="col-md-12" style="padding-bottom:10px">
									<label>{{ l('Цвет иконки ') }}</label>
									<div class="input-group input-colorpicker" id="icon-color">
										<input type="text" name="anchor_setting[anchor_setting_icon_color]" value="{{ anchor_setting.anchor_setting_icon_color }}" class="form-control" id="color" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
								<div class="form-group col-sm-12 col-md-12">
									<label for="background">{{ l('Цвет фона') }}</label>
									<div class="input-group input-colorpicker">
										<input type="text" name="anchor_setting[anchor_setting_background]" value="{{ anchor_setting.anchor_setting_background }}" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Задать фон') }}</label>
									<input type="checkbox" class="switch"  name="anchor_setting[anchor_setting_back_enable]" {% if anchor_setting.anchor_setting_back_enable == 'on' %}checked{% endif %}>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Закругление углов') }}</label>
									<div class="row">
										<div style="margin-bottom:0;" class="form-group col-md-10">
											<input name="anchor_setting[anchor_setting_border_radius]"  id="radius" type="text" data-slider-min="0" data-slider-max="50" data-slider-step="1" data-slider-value="{{ anchor_setting.anchor_setting_border_radius }}"/>
										</div>
									</div>
								</div>
								<div class="form-group col-sm-12 col-md-12">
									<label for="background">{{ l('Тень')  }}</label>
									<div class="input-group">
										<input type="text" id="shadow" name="anchor_setting[anchor_setting_shadow]" value="{{ anchor_setting_shadow }}" data-slider-min="0" data-slider-max="15" data-slider-step="1" data-slider-value="{{ anchor_setting.anchor_setting_shadow }}" />
									</div>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Включить/Выключить анимацию') }}</label>
									<input type="checkbox" class="switch"  name="anchor_setting[anchor_setting_anim_switch]" {% if anchor_setting.anchor_setting_anim_switch == 'on' %}checked{% endif %} >
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Скорость анимации') }}</label>
									<div class="row">
										<div style="margin-bottom:0;" class="form-group col-md-10">
											<input name="anchor_setting[anchor_setting_anim]"  id="animation" type="text" data-slider-min="1" data-slider-max="3" data-slider-step="0.1" data-slider-value="{{ anchor_setting.anchor_setting_anim }}"/>
										</div>
									</div>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Прозрачность') }}</label>
									<div class="row">
										<div style="margin-bottom:0;" class="form-group col-md-10">
											<input name="anchor_setting[anchor_setting_opacity]"  id="opacity" type="text" data-slider-min="0" data-slider-max="1" data-slider-step="0.1" data-slider-value="{{ anchor_setting.anchor_setting_opacity }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="hidden" id="showLink">{{ showLink }}</div>
								<div class="modal fade" role="dialog" aria-labelledby="gridSystemModalLabel" id="gridSystemModal">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="gridSystemModalLabel">{{ l('Выберите иконку') }}</h4>
											</div>
											<div class="modal-body">
												<div class="container-fluid">
													<div id="upload" class="btn btn-primary" data-url="{{ uploadLink }}"><i class="fa fa-upload"></i>{{ l('Загрузить изображение') }} </div>
													<i class="fa fa-spinner fa-spin" id="spinner" style="display:none;"></i>
													<div id="imgList"></div>
													<div id="imgLogs"></div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">{{ l('Закрыть') }}</button>
												<button type="button" class="btn btn-primary" id="check" style="display:none;"><i class="fa fa-check"></i>{{ l('Выбрать') }} </button>
												<button type="button" class="btn btn-danger" id="delete" style="display:none;" data-url="{{ deleteLink }}"><i class="fa fa-trash"></i>{{ l('Удалить') }} </button>
											</div>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="row tab-pane" id="view">
									<div class="form-group col-md-12">
										<label>{{ l('Расположение иконки:') }}</label>
										<select id="position" class="form-control" name="anchor_setting[anchor_setting_position]">
											<option value="left_top" {% if anchor_setting.anchor_setting_position == 'left_top' %}selected{% endif %}>{{ l('Левый верхний угол') }}</option>
											<option value="left_bottom" {% if anchor_setting.anchor_setting_position == 'left_bottom' %}selected{% endif %}>{{ l('Левый нижний угол') }}</option>
											<option value="right_top" {% if anchor_setting.anchor_setting_position == 'right_top' %}selected{% endif %}>{{ l('Правый верхний угол') }}</option>
											<option value="right_bottom" {% if anchor_setting.anchor_setting_position == 'right_bottom' %}selected{% endif %}>{{ l('Правый нижний угол') }}</option>
										</select>
									</div>
									<div class="form-group col-md-12">
										<label>{{ l('Отступы по горизонтали') }}</label>
										<div class="row">
											<div style="margin-bottom:0;" class="form-group col-md-10">
												<input name="anchor_setting[anchor_setting_margin_horiz]"  class="margin" type="text" data-slider-min="0" data-slider-max="10" data-slider-step="1" data-slider-value="{{ anchor_setting.anchor_setting_margin_horiz }}"/>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label>{{ l('Отступы по вертикали') }}</label>
										<div class="row">
											<div style="margin-bottom:0;" class="form-group col-md-10">
												<input name="anchor_setting[anchor_setting_margin_vert]"  class="margin" type="text" data-slider-min="0" data-slider-max="10" data-slider-step="1" data-slider-value="{{ anchor_setting.anchor_setting_margin_vert }}"/>
											</div>
										</div>
									</div>
								</div>
							</div>
							{% if access == true %} {{ btn }} {% endif %}
						</div>
					</div>
					<div data-sticky_column class="col-md-8" id="show" style="bottom:70%;position:absolute;right:20%;width:300px;padding:10px;"></div>
					</form>
					<div id="draftNotice" class="modal fade" id="myModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-body">{{ l('В прошлый раз вы не закончили работу с предложением, восстановить данные?') }}</div>
									<div class="modal-footer">
										<button class="cancel btn btn-default" data-dismiss="modal">{{ l('Отменить') }}</button>
										<button class="open btn btn-primary">{{ l('Восстановить') }}</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
{% endblock%}
{% block footer %}
{{ render_copyright() | raw }}
{% endblock%}