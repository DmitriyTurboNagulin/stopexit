<script src="{{ highcharts }}"></script>
<script src="{{ exporting }}"></script>
<div id="container" style="min-width:400px;height:440px;margin:0 auto;text-align:center;"><i class="fa fa-4x fa-cog fa-spin" style="margin-top: 175px;"></i></div>
<script type="text/javascript">
function chart() {
	$('#container').highcharts({
		chart: {
			zoomType: 'x'
		},
		title: {
			text: '{{ l('График активности') }}'
		},
		subtitle: {
			text: '{{ l('Для изменения периода нажмите и перетащите график') }}'
		},
		xAxis: {
			type: 'datetime'
		},
		yAxis: {
			title: {
				text: '{{ l('Количество номеров телефонов') }}'
			},
		},
		legend: {
			align: 'center',
			verticalAlign: 'top',
			y: 40,
		},
		tooltip: {
		    formatter: function() {
		        return '<b>' + this.series.name + '</b><br>' + '{{ l('Количество') }}' + ': <b>' + this.y + '</b>';
		    }
		},
		plotOptions: {
			area: {
				fillColor: {
					stops: [
						[0, Highcharts.getOptions().colors[0]],
						[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					]
				},
				marker: {
					radius: 4
				},
				lineWidth: 2,
				states: {
					hover: {
						lineWidth: 2
					}
				},
				threshold: null
			}
		},
		series: [
			{% for num, item in phone %}
				{
					type: 'area',
					name: '{{ l('Форма №') }}{{ num }}',
					data: {{ item }}
				},
			{% endfor %}
		]
	});
}

setTimeout(chart, 1000);
</script>
