{% extends 'main.twig.php' %}

{% block body %}
<div id="wrapper">
    {{include('admin/topbar.twig.php')}}
    {{include('admin/sidebar.twig.php')}}
    <div id="page-wrapper">
<div class="row">
<h3>{{ title }}</h3>
    <table class="table table-bordered table-hover">
        <tr>
            <th>{{ l('Пользователь') }}</th>
            <th>{{ l('Действие') }}</th>
            <th>{{ l('Новое значение') }}</th>
            <th>{{ l('Старое значение') }}</th>
            <th>{{ l('Дата') }}</th>
        </tr>
        {% for log in logs %}
            <tr>
                <th>{{ log.user }}</th>
                <td>{{ event[log.event] }}</td>
                <td class="break-word">{{ log.new }}</td>
                <td class="break-word">{{ log.old }}</td>
                <td>{{ log.created_at }}</td>
            </tr>
        {% endfor %}
    </table>        
</div>
</div>
</div>
{% endblock %}
{% block footer %}
{{ render_copyright() | raw }}
{% endblock%}

