<style type="text/css">
#getcalls_error{{ call_id }} {
    background:{{ call_setting_background_step_30 }};
    border:1px solid {{ call_setting_background_step_30 }};
    border-radius:10px;
    padding:10px !important;
    text-align:center;
    color:#555;
    position:absolute;
    z-index:999;
    margin-top: -80px;
    {{ error_setting }}
}

#getcalls_error{{ call_id }}::before, #getcalls_error{{ call_id }}::after{
    content:"";
    position:absolute;
    left:40px; bottom:-20px;
    border:10px solid transparent;
    border-top:10px solid {{ call_setting_background_step_30 }};
}
</style>
<div class="getcalls" id="getcalls" style="opacity:0;transition:opacity {{ speed }}s;font-family:arial;{{ position }}z-index:2147483647;display:block;color:{{ call_setting_color }};background:{{ call_setting_border_color2 }};{{ border }}width:{{ call_setting_width }}px;height:{{ call_setting_height }}px;padding:0;">
    <div id="getcalls_checkout" style="position:relative;background:{{ call_setting_background }} {% if call_setting_background_image != '' %}url({{ call_setting_background_image }}){% endif %};background-size: 100% 100%;display:block;margin:0;width:100%;height:100%;padding:0% 0 0% 0;">
        {% if call_setting_manager_photo != '' %}
        <div id="image-container" style="max-width:50%;height:100%;display:inline-block; padding:0px 1% 0 1%; text-align:center; margin:2% 0 0 1%;">
        <img src="{{ call_setting_manager_photo }}" style="display:block;max-width:100%;min-width:100%; max-height:90%; margin:0 auto; {{ manager_photo_position }}">
        <span style="color:{{call_setting_color}};{{ manager_name_position }};font-size:{{ call_setting_manger_name_size }}px;text-align:center;width:100%;">{{ call_setting_manger_name }}</span>
        </div>
        {% endif %}
        <div id="container" style="{{ call_setting_container_position }}overflow: hidden;  margin:2% 0 0 0; padding:0px 1% 0 1%; ">
        {% if call_setting_logo != '' %}
        <img src="{{ call_setting_logo }}" style="max-width:98%;margin: 0 0 10px 0; {{ call_setting_logo_position }}">
        {% endif %}
        {% if call_setting_company != '' %}
        <div style="display:block;font-size:{{ call_setting_company_size }}px;line-height:{{ call_setting_company_size }}px; {{ call_setting_company_position }} text-align:center;">{{ call_setting_company }}</div>
        {% endif %}
        <div onclick="{{ remove }}; closeCount({{ call_id }})" id="getmorecalls_close{{ call_id }}" style="display:block;cursor:pointer;z-index:100;position:absolute;right:0;top:0;margin:-7px -7px 0 0;color:{{ call_setting_background }};background:{{ call_setting_color }};height:20px;width:20px;text-align:center;border-radius:18px;font-size:20px;line-height:20px;">&times;</div>
        <div style="display:block;z-index:100;font-size:{{ call_setting_text_size }}px;line-height:{{ call_setting_text_size }}px;text-align:center;width:55%; {{ call_setting_text_position }}">{{ call_setting_text | raw }}</div>
        <div>
        {% if call_setting_num_error != '' %}
        <div id="getcalls_error{{ call_id }}" class="getcalls_error" style="display:none;"><font color="black" size="2" style="margin:10px">{{ call_setting_num_error }}</font></div>
        {% endif %}
        <input type="phone" id="getMoreCall{{ call_id }}phone" placeholder="{{ call_setting_placeholder }}" style="border-radius:{{ call_setting_radius }}px;padding:3px 5px 3px 5px; border:2px solid {{ call_setting_background_step_5 }};outline: none;background-color:{{ call_setting_background_step_20 }};display:block;  {{ call_setting_input_position }}" />
        </div>
        <div id="getcalls_button{{ call_id }}" onclick="postPhone({{ call_id }});" style="text-align:center;line-height:{{ call_setting_yes_size }}px;font-size:{{ call_setting_yes_size }}px;padding:4px;cursor:pointer;border-radius:{{ call_setting_radius }}px;background:{{ call_setting_background_btn }};color:{{ call_setting_color_btn }};display:block;{{ call_setting_button_position }}">{{ call_setting_yes | raw }}</div>
        </div>
    </div>
</div>
