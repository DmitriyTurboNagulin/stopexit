{% extends 'main.twig.php' %}
{% block body %}
	<div id="wrapper">
		{{include('admin/topbar.twig.php')}}
		{{include('admin/sidebar.twig.php')}}
		<div class="row" id="page-wrapper">
			<h3 class="page-header">{{ header }}</h3>
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#install" aria-controls="install" role="tab" data-toggle="tab">{{ l('Установка') }}</a></li>
				<li role="presentation"><a href="#anchor" aria-controls="anchor" role="tab" data-toggle="tab">{{ l('Привязка к иконке') }}</a></li>
				<li role="presentation"><a href="#trueval" aria-controls="trueval" role="tab" data-toggle="tab">{{ l('Показ при условии') }}</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="install">
					<p>{{ l('Добавьте следующий код в шаблон сайта в <head>') }}</p>
					<pre>&lt;script type="text/javascript" src="{{ url }}">&lt;/script&gt;</pre>
				</div>
				<div role="tabpanel" class="tab-pane" id="anchor">
					<p>{{ l('Есть возможность выводить Stopexit или GetMoreCalls при нажатии на иконку-предложение.') }}</p>
					{{ l('<p>Для этого нужно в настройках объявления в разделе <strong>"Видимость и показ"</strong> в пункте') | raw }}
					{{ l('<strong>"Событие для показа"</strong> выбрать <strong>"Не показывать автоматически"</strong>, а затем ниже в пункте <strong>"Привязать объявление к иконке"</strong>') | raw }}
					{{ l('выбрать иконку, при нажатии на которую будет выводиться объявление.<br/>') | raw }}
					{{ l('Несколько объявлений не могут быть одновременно привязаны к одной иконке, так что как только вы выбираете привязку иконки к объявлению,') }}
					{{ l('она становится недоступна для всех других.') | raw }}</p>
				</div>
				<div role="tabpanel" class="tab-pane" id="trueval">
					<p>{{ l('Есть возможность показывать объявление только при выполнении определенного условия. Например, нажатие на кнопку. Для этого необходимо в настройках объявления в разделе') }} 
					{{ l('<strong>"Событие для показа"</strong> выбрать <strong>"Выполнение условия"</strong>.<br/>') | raw }}
					{{ l('Пример испльзования для Stopexit:') }}
					<pre>&ltinput type="button" onclick="stopexit[ID]ShowOnPage = true"&gt</pre>
					{{ l('Где ID = идентификатор объявления, его можно посмотреть там же, где и статистику, в поле № <br/>') | raw }}
					{{ l('Для GetMoreCalls все будет аналогично, за исключением названия переменной: <strong>getCalls[ID]ShowOnPage</strong><br/>') | raw }}
					{{ l('Объявление покажется, когда переменной будет передано значение <strong>true</strong> ') | raw }}</p>
				</div>
			</div>
		</div>
	</div>
{% endblock %}
{% block footer %}
{{ render_copyright() | raw }}
{% endblock%}
