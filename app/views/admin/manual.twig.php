{% extends 'admin/index.twig.php' %}
{% block content %}
	<div class="row page-header-box">
		<div class="col-xs-10">
			<h3>{{ title }}</h3>
		</div>
	</div>
	<div class="row">
		{{ manual }}
	</div>
{% endblock %}
