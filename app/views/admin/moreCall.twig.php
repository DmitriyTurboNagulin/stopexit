{% extends 'main.twig.php' %}
{% block body %}
<div id="wrapper">
	{{ include('admin/topbar.twig.php') }}
	{{ include('admin/sidebar.twig.php') }}
	<div id="page-wrapper">
		<div class="row">
			<h3>{{ header }}</h3>
			<button class="btn btn-default btn-xs" style="margin-bottom:10px;" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">{{ l('История редактирования') }}</button>
			<div class="collapse" id="collapseExample">
				<div class="well">
					<div class="row">
						<div class="col-md-4">
						<div class="hidden" id="showCallLink" >{{ showCallLink }}</div>
							{% for trash in trashed %}
								<a class="showDraft" data-data="{{ trash.data }}" href="#{{ trash.id }}">{{ trash.email }}: {{ trash.deleted }}</a><br>
							{% endfor %}
						</div>
						<div class="col-md-8">
							<div id="showText">
								<button id="checkDraft" class="btn btn-xs btn-default">{{ l('Применить') }}</button>
								<p>{{ l('Будут измнеены все настройки формы: дизайн, текст, видимость и временя показа') }}</p>
							</div>
							<div id="show2"></div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="hidden" id="showCallLink">{{ showCallLink }}</div>
			<form action="{{ linkSave }}" id="linkSave" method="post" data-url="{{ templateSaveLink }}">
				<input type="hidden" name="call_id" value="{{ call.call_id }}">
				{% set btn %}
					<p class="text-muted draft">{{ l('Сохранено в черновиках, время:') }} <span></span></p>
					<div class="form-group">
						<input type="submit" class="btn btn-xs btn-primary cancelDraft" value="{{ l('Сохранить') }}">
						<a href="{{ cancelSave }}" class="btn btn-xs btn-default cancelDraft">{{ l('Отменить') }}</a>
						<input type="button" class="btn btn-xs btn-info saveToDraft" value="{{ l('В черновик') }}">
					</div>
				{% endset %}
				{% if access == true %} {{ btn }} {% endif %}
				<div class="row" data-sticky_parent>
					<div class="col-md-4" data-sticky_column>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#text" aria-controls="text" role="tab" data-toggle="tab">{{ l('Текст') }}</a></li>
							<li role="presentation"><a href="#style" aria-controls="style" role="tab" data-toggle="tab">{{ l('Дизайн') }}</a></li>
							<li role="presentation"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">{{ l('Видимость и показ') }}</a></li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="text">
								<div class="form-group">
									<label>{{ l('Название предложения') }} <small>({{ l('отображается в админке') }})</small></label>
									<input type="text" class="form-control" name="call_name" value="{{ call.call_name }}" placeholder="{{ l('Название предложения') }}">
								</div>
								<div class="form-group">
									<label>{{ l('Логотип') }}</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-upload chooseImage" style="cursor:pointer;" data-inp="call_setting_logo"></i>
										</div>
										<input type="text" name="call_setting[call_setting_logo]" value="{{ call_setting.call_setting_logo }}" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label>{{ l('Название компании') }}</label>
									<div class="input-group">
										<input type="text" class="form-control" name="call_setting[call_setting_company]" value="{{ call_setting.call_setting_company }}">
										<div class="input-group-addon">
											<input type="hidden" class="TouchFontSize" name="call_setting[call_setting_company_size]" value="{{ call_setting.call_setting_company_size }}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>{{ l('Текст предложения') }}</label>
									<div class="input-group">
										<input type="text" class="form-control" name="call_setting[call_setting_text]" value="{{ call_setting.call_setting_text }}" placeholder="{{ l('Мы вам перезвоним') }}">
										<div class="input-group-addon">
											<input type="hidden" class="TouchFontSize" name="call_setting[call_setting_text_size]" value="{{ call_setting.call_setting_text_size }}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>{{ l('Имя менеджера') }}</label>
									<div class="input-group">
										<input type="text" class="form-control" name="call_setting[call_setting_manger_name]" value="{{ call_setting.call_setting_manger_name }}" placeholder="{{ l('Jane Doe') }}">
										<div class="input-group-addon">
											<input type="hidden" class="TouchFontSize" name="call_setting[call_setting_manger_name_size]" value="{{ call_setting.call_setting_manger_name_size }}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>{{ l('Текст в форме для ввода номера') }}</label>
									<input type="text" class="form-control" name="call_setting[call_setting_placeholder]" value="{{ call_setting.call_setting_placeholder }}" placeholder="{{ l('Введите ваш номер телефона') }}">
								</div>
								<div class="form-group">
									<label>{{ l('Текст в форме при ошибке ввода') }}</label>
									<input type="text" class="form-control" name="call_setting[call_setting_num_error]" value="{{ call_setting.call_setting_num_error }}" placeholder="{{ l('Пожалуйста, введите номер телефона') }}">
									<span class="help-block">{{ l('От 20 до 30 символов') }}</span>
								</div>
								<div class="form-group">
									<label>{{ l('Текст кнопки') }}</label>
									<div class="input-group">
										<input type="text" class="form-control" name="call_setting[call_setting_yes]" value="{{ call_setting.call_setting_yes }}" placeholder="{{ l('Отправить!') }}">
										<div class="input-group-addon">
											<input type="hidden" class="TouchFontSize" name="call_setting[call_setting_yes_size]" value="{{ call_setting.call_setting_yes_size }}">
										</div>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="row tab-pane" id="style">
								<div style="margin-bottom:0;" class="form-group col-md-12 ">
									<label>{{ l('Cохраненные схемы:') }}</label>
									<div class="form-group col-md-10 row">
										<div class="hidden" id="showTemplateLink">{{ showTemplateLink }}</div>
										<select class="form-control" id="template-select">
										<option value="0" selected>{{ l('Нет') }}</option>
										{% for value in templates  %}
											<option value="{{ value.call_template_id }}">{{ value.call_template_name }}</option>
										{% endfor %}
										</select>
									</div>
									<div class="form-group col-md-2 col-md-offset-1 ">
										<button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#templateModal">
										<i class="fa fa-trash"></i>
										</button>
									</div>
								</div>
								<div class="hidden" id="deleteTemplateLink">{{ deleteTemplateLink }}</div>
								<div class="modal fade" role="dialog" aria-labelledby="templateModalLabel" id="templateModal">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<p>{{ l('Удалить выбранные шаблоны') }}</p>
											</div>
										<div class="modal-body">
											{% for value in templates %}
												<div class="checkbox">
													<label><input type="checkbox" class="template-check" value="{{ value.call_template_id }}">{{ value.call_template_name }}</label>
												</div>
											{% endfor %}
											<button type="button" class="btn btn-danger" id="deleteTemplate">{{ l('Удалить') }}</button>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">{{ l('Закрыть') }}</button>
										</div>
									</div>
								</div>
							</div>
								<div class="form-group col-md-12">
									<label>{{ l('Фоновое изображение') }}</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-upload chooseImage" style="cursor:pointer;" data-inp="call_setting_background_image"></i>
										</div>
										<input type="text" name="call_setting[call_setting_background_image]" value="{{ call_setting.call_setting_background_image }}" class="form-control" id="background">
									</div>
								</div>
								<div class="form-group col-sm-12 col-md-12">
									<label>{{ l('Положение элементов в блоке') }}</label>
									<select id="man-pos" class="form-control" name="call_setting[call_setting_manager_position]">
										<option value="none" {% if call_setting.call_setting_manager_position == 'none' %}selected{% endif %}>{{ l('По центру') }}</option>
										<option value="left" {% if call_setting.call_setting_manager_position == 'left' %}selected{% endif %}>{{ l('Слева') }}</option>
										<option value="right" {% if call_setting.call_setting_manager_position == 'right' %}selected{% endif %}>{{ l('Справа') }}</option>
									</select>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Фото сотрудника') }}</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-upload chooseImage" style="cursor:pointer;" data-inp="call_setting_manager_photo"></i>
										</div>
										<input type="text" name="call_setting[call_setting_manager_photo]" value="{{ call_setting.call_setting_manager_photo }}" class="form-control" id="man-photo">
									</div>
								</div>
								<div class="form-group col-sm-6 col-md-6">
									<label for="background">{{ l('Цвет фона') }}</label>
									<div class="input-group input-colorpicker">
										<input type="text" name="call_setting[call_setting_background]" value="{{ call_setting.call_setting_background }}" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
								<div class="form-group col-sm-6 col-md-6">
									<label for="background">{{ l('Цвет текста') }}</label>
									<div class="input-group input-colorpicker">
										<input type="text" name="call_setting[call_setting_color]" value="{{ call_setting.call_setting_color }}" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
								<div class="form-group col-sm-6 col-md-6">
									<label for="background">{{ l("Цвет текста кнопки 'Да'") }}</label>
									<div class="input-group input-colorpicker">
										<input type="text" name="call_setting[call_setting_color_btn]" value="{{ call_setting.call_setting_color_btn }}" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
								<div class="form-group col-sm-6 col-md-6">
									<label for="background">{{ l("Цвет фона кнопки 'Да'") }}</label>
									<div class="input-group input-colorpicker">
										<input type="text" name="call_setting[call_setting_background_btn]" value="{{ call_setting.call_setting_background_btn }}" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Рамки кнопки "Да"') }}</label>
									<div style="margin-bottom:0;" class="form-group col-md-10">
										<input name="call_setting[call_setting_radius]"  id="radius" type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="{{ call_setting.call_setting_radius }}"/>
									</div>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Тип рамки') }}</label>
									<select class="form-control" name="call_setting[call_setting_border_type]">
										<option value="none" {% if call_setting.call_setting_border_type == 'none' %}selected{% endif %}>{{ l('Без рамки') }}</option>
										<option value="dashed" {% if call_setting.call_setting_border_type == 'dashed' %}selected{% endif %}>{{ l('В полоску') }}</option>
										<option value="solid" {% if call_setting.call_setting_border_type == 'solid' %}selected{% endif %}>{{ l('Полоса') }}</option>
										<option value="double" {% if call_setting.call_setting_border_type == 'double' %}selected{% endif %}>{{ l('Двойная') }}</option>
									</select>
								</div>
								<div class="form-group col-sm-6 col-md-6">
									<label for="background">{{ l('Цвет рамки %s', 1) }}</label>
									<div class="input-group input-colorpicker">
										<input type="text" name="call_setting[call_setting_border_color]" value="{{ call_setting.call_setting_border_color }}" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
								<div class="form-group col-sm-6 col-md-6">
									<label for="background">{{ l('Цвет рамки %s', 2) }}</label>
									<div class="input-group input-colorpicker">
										<input type="text" name="call_setting[call_setting_border_color2]" value="{{ call_setting.call_setting_border_color2 }}" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Сохранить шаблон схемы') }}</label>
									<div class="row">
										<div style="margin-bottom:0;" class="form-group col-md-10">
											<input class="form-control" type="text" name="call_template_name" id="template-name"/>
										</div>
										<div style="margin-bottom:0;" class="form-group col-md-2 row">
											<a href="#" class="btn btn-primary" id="save-template"><i class="fa fa-floppy-o"></i></a>
										</div>
									</div>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Ширина объявления') }}, <small>px</small></label>
									<input class="form-control TouchSpin" type="text" name="call_setting[call_setting_width]" value="{{ call_setting.call_setting_width }}" />
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Высота объявления') }}, <small>px</small></label>
									<input class="form-control TouchSpin" type="text" name="call_setting[call_setting_height]" value="{{ call_setting.call_setting_height }}" />
								</div>
							</div>
							<div role="tabpanel" class="row tab-pane" id="view">
								<div class="form-group col-md-12">
									<label>{{ l('Условие показа:') }}</label>
									<select class="form-control" id="call_setting_view" name="call_setting[call_setting_view]">
										<option value="new" {% if call_setting.call_setting_view == 'new' %}selected{% endif %}>{{ l('Только новым') }}</option>
										<option value="x_days" {% if call_setting.call_setting_view == 'x_days' %}selected{% endif %}>{{ l('Новым посетителям за X дней') }}</option>
										<option value="all" {% if call_setting.call_setting_view == 'all' %}selected{% endif %}>{{ l('Всем') }}</option>
									</select>
									<label style="margin:15px 0 5px 0">{{ l('Новым посетителям за Х дней') }}</label>
									<input class="form-control" {% if call_setting.call_setting_view != 'x_days' %}readonly{% endif %} type="text" name="call_setting[call_setting_view_x_days]" id="call_setting_view_x_days" min="1" max="365" value="{{ call_setting.call_setting_view_x_days }}" />
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Событие для показа:') }}</label>
									<select class="form-control" id="call_setting_event" name="call_setting[call_setting_event]">
										<option value="escape" {% if call_setting.call_setting_event == 'escape' %}selected{% endif %}>{{ l('Уход с активной вкладки') }}</option>
										<option value="enter" {% if call_setting.call_setting_event == 'enter' %}selected{% endif %}>{{ l('Вход на страницу') }}</option>
										<option value="x_sec_page" {% if call_setting.call_setting_event == 'x_sec_page' %}selected{% endif %}>{{ l('Нахождение на странице более X секунд') }}</option>
										<option value="val_is_true" {% if call_setting.call_setting_event == 'val_is_true' %}selected{% endif %}>{{ l('Выполнение условия') }}</option>
										<option value="off" {% if call_setting.call_setting_event == 'off' %}selected{% endif %}>{{ l('Не показывать автоматически') }}</option>
									</select>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Нахождение на странице более X секунд') }}</label>
									<input class="form-control" {% if call_setting.call_setting_event != 'x_sec_page' %}readonly{% endif %} type="text" name="call_setting[call_setting_x_sec]" id="call_setting_x_sec" min="1" max="120" value="{{ call_setting.call_setting_x_sec }}" />
								</div>
								<div class="form-group col-md-12">
									<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#dateModal">
										<i class="fa fa-clock-o"></i>
										{{ l('Выберите время показа') }}
									</button>
								</div>
								<div class="modal fade" role="dialog" aria-labelledby="dateModalLabel" id="dateModal">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="gridSystemModalLabel">{{ l('Выберите время') }}</h4>
											</div>
										<div class="modal-body">
											<div class="input-group">
												<p>{{ l('Текущее время на сервере:') }}<strong> {{ time }} </strong></p>
												<table class="table table-condensed table-bordered" id="table-hours">
													<tr>
														<th><a href="#" class="select-all" data-done="false">{{ l('Все') }}</a></th>
														{% for i in 0..23 %}
															<td class="fixed-size-td"><a href="#" class="date-select" data-day="hour{{ i }}" data-done="false">{{ i }}</a></td>
														{% endfor %}
													</tr>
													{% for key,value in days  %}
													<tr>
														<th><a href="#" data-day="{{ key }}" class="date-select" data-done="false">{{ value }}</a></th>
														{% for j in 0..23 %}
														<td class="fixed-size-td {{ key }} hour{{ j }}">
															<input type="checkbox" {% if call_setting.call_setting_days[key][j] == 'on' %} checked {% endif %} name="call_setting_days[{{ key }}][{{ j }}]">
														</td>
														{% endfor %}
													</tr>
													{% endfor %}
												</table>
											</div>
											<div class="input-daterange input-group" id="datepicker">
												<input type="text" class="input-sm form-control" name="call_setting[call_setting_start]" {% if call_setting.call_setting_start is not null %} value="{{ call_setting.call_setting_start }}" {% endif %}/>
												<span class="input-group-addon">{{ l('до') }}</span>
												<input type="text" class="input-sm form-control" name="call_setting[call_setting_stop]" {% if call_setting.call_setting_stop is not null %} value="{{ call_setting.call_setting_stop }}" {% endif %}/>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">{{ l('Закрыть') }}</button>
										</div>
									</div>
								</div>
							</div>
								<div class="form-group col-md-12">
									<label>{{ l('Расположение объявления:') }}</label>
									<select class="form-control" name="call_setting[call_setting_position]">
										<option value="center" {% if call_setting.call_setting_position == 'center' %}selected{% endif %}>{{ l('Центр') }}</option>
										<option value="left_top" {% if call_setting.call_setting_position == 'left_top' %}selected{% endif %}>{{ l('Левый верхний угол') }}</option>
										<option value="left_bottom" {% if call_setting.call_setting_position == 'left_bottom' %}selected{% endif %}>{{ l('Левый нижний угол') }}</option>
										<option value="right_top" {% if call_setting.call_setting_position == 'right_top' %}selected{% endif %}>{{ l('Правый верхний угол') }}</option>
										<option value="right_bottom" {% if call_setting.call_setting_position == 'right_bottom' %}selected{% endif %}>{{ l('Правый нижний угол') }}</option>
									</select>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Привязать объявление к иконке') }}</label>
									<select class="form-control" name="call_anchor">
									<option value="none">{{ l('Без привязки') }}</option>
										{% for anchor in anchors %}
											<option value="{{ anchor.anchor_id }}" {% if anchor.anchor_id == call_setting.anchor_id %}selected{% endif %}>{{ anchor.anchor_name }}</option>
										{% endfor %}
									</select>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Эффект появления:') }}</label>
									<select class="form-control" name="call_setting[call_setting_effect]">
										<option value="instantly" {% if call_setting.call_setting_effect == 'instantly' %}selected{% endif %}>{{ l('Мгновенно') }}</option>
										<option value="slow" {% if call_setting.call_setting_effect == 'slow' %}selected{% endif %}>{{ l('Плавно') }}</option>
									</select>
								</div>
								<div class="form-group col-md-12">
									<label>{{ l('Список доменов на которых работает скрипт:') }}[<a href="#" data-toggle="modal" data-target="#domain-hints">{{ l('Правила ввода') }}</a>]</label>
									<textarea name="call_setting[call_setting_domain]" class="form-control">{{ call_setting.call_setting_domain }}</textarea>
								</div>
								<div id="domain-hints" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog"><div class="modal-content"><div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div><div class="modal-body">
								<h2 style="margin:0 0 10px 0;">{{ l('Правила ввода доменов') }}</h2>
								<p>{{ l('Необходимо вводить только хостовую часть домена.<br/>К примеру, для домена http://domain.com хостовой частью будет domain.com') }}</p>
								</div></div></div></div>
								<div class="form-group col-md-12">
									<label>{{ l('Настройки показа предложения по страницам:') }}</label>
									<label><input type="radio" class="radio-show_all" style="margin-bottom: 5px;" name="call_setting[call_setting_templates]" value="everywhere" {% if call_setting.call_setting_templates == 'everywhere' %}checked{% endif %}> {{ l('На всех страницах') }}</label><br>
									<label><input type="radio" class="radio-show_all" style="margin-bottom: 5px;" name="call_setting[call_setting_templates]" value="main" {% if call_setting.call_setting_templates == 'main' %}checked{% endif %}> {{ l('Только на главной') }}</label><br>
									<label><input type="radio" class="radio-show_all" style="margin-bottom: 5px;" name="call_setting[call_setting_templates]" value="optional" {% if call_setting.call_setting_templates == 'optional' %}checked{% endif %}> {{ l('Задать шаблоны URL') }}</label>

									<div class="form-group col-sm-12 col-md-12 urls" style="{% if call_setting.call_setting_templates != 'optional' %}display:none;{% endif %}">
										<label for="white_urls">{{ l('Шаблоны URL для показа') }}[<a href="#" data-toggle="modal" data-target="#templates-hints">{{ l('Правила ввода') }}</a>]<br><small>{{ l('(белый список - url, на которых разрешен показ)') }}</small></label>
										<textarea name="call_setting[call_setting_templates_white]" class="form-control" id="white_urls">{{ call_setting.call_setting_templates_white }}</textarea>
										<small>{{ l('URL разделяются переносом строки. Используются регулярные выражения. Спецсимволы, кроме / должны быть экранированы обратным слешем \.') }}</small>
									</div>

									<div class="form-group col-sm-12 col-md-12 urls" style="{% if call_setting.call_setting_templates != 'optional' %}display:none;{% endif %}">
										<label for="black_urls">{{ l('Шаблоны URL для отключения показа') }}<br><small>{{ l('(черный список - url, на которых показ запрещен)') }}</small></label>
										<textarea name="call_setting[call_setting_templates_black]" class="form-control" id="black_urls">{{ call_setting.call_setting_templates_black }}</textarea>
										<small>{{ l('URL разделяются переносом строки. Используются регулярные выражения. Спецсимволы, кроме / должны быть экранированы обратным слешем \.') }}</small>
									</div>
									<br clear="all">
								</div>
								<div id="templates-hints" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog"><div class="modal-content"><div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div><div class="modal-body">
								<h2 style="margin:0 0 10px 0;">{{ l('Правила ввода шаблонов') }}</h2>
								<p>{{ l('Необходимо вводить только URN страницы.') }}<br/>{{ l('К примеру, для страницы http://domain.com/blog/info нужной частью будет /blog/info') }}</p>
								<p>{{ l('Если вы хотите добавить в черный список главную страницу сайта, нужно написать просто косую черту') }} (<strong>/</strong>)</p>
								</div></div></div></div>
							</div>
							{% if access == true %} {{ btn }} {% endif %}
						</div>
					</div>
					<div data-sticky_column class="col-md-8" id="show" style="width:600px;">{{ show_call | raw }}</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div id="draftNotice" class="modal fade" id="myModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">{{ l('В прошлый раз вы не закончили работу с предложением, восстановить данные?') }}</div>
			<div class="modal-footer">
				<button class="cancel btn btn-default" data-dismiss="modal">{{ l('Отменить') }}</button>
				<button class="open btn btn-primary">{{ l('Восстановить') }}</button>
			</div>
		</div>
	</div>
</div>
<div class="hidden" id="showLink">{{ showLink }}</div>
<div class="modal fade" role="dialog" aria-labelledby="gridSystemModalLabel" id="gridSystemModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="gridSystemModalLabel">{{ l('Выберите изображение') }}</h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div id="upload" class="btn btn-primary" data-url="{{ uploadLink }}"><i class="fa fa-upload"></i>{{ l('Загрузить изображение') }} </div>
					<i class="fa fa-spinner fa-spin" id="spinner" style="display:none;"></i>
					<div id="imgList"></div>
					<div id="imgLogs"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ l('Закрыть') }}</button>
				<button type="button" class="btn btn-primary" id="check" style="display:none;"><i class="fa fa-check"></i>{{ l('Выбрать') }} </button>
				<button type="button" class="btn btn-danger" id="delete" style="display:none;" data-url="{{ deleteLink }}"><i class="fa fa-trash"></i>{{ l('Удалить') }} </button>
			</div>
		</div>
	</div>
</div>




{% endblock %}
{% block footer %}
{{ render_copyright() | raw }}
{% endblock%}
