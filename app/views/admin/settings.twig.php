{% extends 'main.twig.php' %}
{% block body %}
<div id="wrapper">
	{{include('admin/topbar.twig.php')}}
	{{include('admin/sidebar.twig.php')}}
	<div class="row" id="page-wrapper">
		<h3 class="page-header">{{ header }}</h3>
		{% if access == true %}
		<ul class="nav nav-tabs" role="tablist" data-url="{{ linkSettings }}">
			<li role="presentation"><a href="?tab=calls#calls" data-tab="calls" aria-controls="calls" role="tab" data-toggle="tab"><i class="fa fa-phone-square"></i> GetMoreCalls</a></li>
			<li role="presentation" class="active"><a href="?tab=email#email" data-tab="email" aria-controls="email" role="tab" data-toggle="tab"><i class="fa fa-at"></i> Email</a></li>
			<li role="presentation"><a href="?tab=sms#sms" data-tab="sms" aria-controls="sms" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> SMS</a></li>
		</ul>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane" id="calls">
				<div class="row">
					<div class="form-group col-md-5">
						<form action="{{ url }}" method="post">
							<div class="form-group">
								<p>{{ l('Введите маску, которая будет использоваться при вводе номера в GetMoreCalls') }}</p>
								<label>{{ l('Маска поля ввода номера') }}</label><br>
								<input type="text" class="form-control" value="{{ setting.call_mask }}" name="setting[call_mask]">
								<span class="help-block">{{ l('Важно: все числа должны быть девятками. Маска может быть любой.') }}</span>
								<span class="help-block">{{ l('Например, для украинских номеров можно поставить маску вида (999)999-99-99, тогда пользователь введет, к примеру, номер (095)123-45-67.') }}
								{{ l('Или можно задать маску +99(999)999-99-99, тогда телефон будет, соответственно, +38(095)123-45-67') }}</span>
							</div>
							<input type="submit" class="btn btn-primary" value="{{ l('Сохранить') }}">
						</form>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane active" id="email">
				<div class="row">
					<div class="form-group col-md-5">
						<form action="{{ url }}" method="post">
							<div class="form-group">
								<label>{{ l('E-mail получателя') }}</label><br>
								<input type="email" class="form-control" value="{{ setting.reciever_email }}" name="setting[reciever_email]">
							</div>
							<div class="form-group">
								<label>{{ l('Имя отправителя') }}</label><br>
								<input type="text" class="form-control" value="{{ setting.sender_name }}" name="setting[sender_name]">
							</div>
							<div class="form-group">
								<label>{{ l('E-mail отправителя') }}</label><br>
								<input type="email" class="form-control" value="{{ setting.sender_email }}" name="setting[sender_email]">
							</div>
							<div class="form-group"><label>{{ l('Рассылка через SMTP') }}</label><br>
								<input type="hidden" value="off" name="setting[smtp_status]">
								<input type="checkbox" class="switch" name="setting[smtp_status]" {% if setting.smtp_status == 'on' %} checked{% endif %}>
							</div>
							{% if setting.smtp_status == 'on' %}
								<div id="smtp_set">
									<div class="form-group">
										<label>{{ l('SMTP сервер') }}</label><br>
										<input type="text" class="form-control" value="{{ setting.smtp_server }}" name="setting[smtp_server]">
									</div>
									<div class="form-group">
										<label>{{ l('Порт') }}</label><br>
										<input type="text" class="form-control" value="{{ setting.smtp_port }}" name="setting[smtp_port]">
									</div>
									<div class="form-group">
										<label>{{ l('Логин') }}</label><br>
										<input type="text" class="form-control" value="{{ setting.smtp_login }}" name="setting[smtp_login]">
									</div>
									<div class="form-group">
										<label>{{ l('Пароль') }}</label><br>
										<input type="text" class="form-control" value="{{ setting.smtp_password }}" name="setting[smtp_password]">
									</div>
								</div>
							{% endif %}
							<input type="submit" class="btn btn-primary" value="{{ l('Сохранить') }}">
						</form>
					</div>
					<div class="form-group col-md-7">
						<p>{{ l('Email рассылка используется в GetMoreCalls') }}</p>
						<p>{{ l('SMTP сервер - сервер исходящей почты. Сообщение сначала кладется в почтовый ящик SMTP-сервера, а уже потом сервер при возможности передает послание адресату.') }}</p>
						<h3>{{ l('Рекомендуемые SMTP сервера') }}</h3>
						<ol>
							<li><a href="https://sendpulse.com/ru/" target="_blank">sendpulse.com</a></li>
							<li><a href="http://www.epochta.com.ua/" target="_blank">epochta.com.ua</a></li>
							<li><a href="http://www.serversmtp.com/ru" target="_blank">serversmtp.com</a></li>
						</ol>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="sms">
				<div class="row">
					<div class="form-group col-md-5">
						<form action="{{ url }}" method="post">
							<div class="form-group">
								<label>{{ l('SMS рассылка') }}</label><br>
								<input type="hidden" value="off" name="setting[sms_status]">
								<input type="checkbox" class="switch" name="setting[sms_status]" {% if setting.sms_status == 'on' %} checked {% endif %}>
							</div>
							{% if setting.sms_status == 'on' %}
								<div id="sms_set">
									<div class="form-group">
										<label>{{ l('SMS рассылка') }}</label><br>
										<select class="form-control" name="setting[sms_service]" id="sms_service">
											<option value="turbosms" {% if setting.sms_service == 'turbosms' %} selected{% endif %}>turbosms.ua</option>
											<option value="smsc" {% if setting.sms_service == 'smsc' %} selected{% endif %}>smsc.ua</option>
											<option value="websms" {% if setting.sms_service == 'websms' %} selected{% endif %}>websms.ru</option>
											<option value="smsfeedback" {% if setting.sms_service == 'smsfeedback' %} selected{% endif %}>smsfeedback.ru</option>
											<option value="mobizon" {% if setting.sms_service == 'mobizon' %} selected{% endif %}>mobizon.kz</option>
										</select>
									</div>
									<div class="form-group">
										<label>{{ l('Номер получателя') }}</label><br>
										<input type="text" {% if login %} value="{{ setting.sms_reciever }}" {% else %} readonly="readonly"{% endif %} class="form-control" name="setting[sms_reciever]" id="sms_reciever">
									</div>
									<div class="form-group">
										<label>{{ l('Альфа-имя') }}</label><br>
										<input type="text" class="form-control" value="{{ setting.sms_alphaname }}" name="setting[sms_alphaname]">
										<small>{{ l('Предварительно добавить в сервисе SMS рассылок') }}</small>
									</div>
									<div class="form-group">
										<label>{{ l('Логин') }}</label><br>
										<input type="text" {% if login %} value="{{ setting.sms_login }}" {% else %} readonly="readonly"{% endif %} class="form-control" name="setting[sms_login]" id="sms_login">
									</div>
									<div class="form-group">
										<label>{{ l('Пароль') }}</label><br>
										<input type="text" {% if pass %} value="{{ setting['sms_pass'] }}" {% else %} readonly="readonly"{% endif %} class="form-control" name="setting[sms_pass]" id="sms_pass">
									</div>
									<div class="form-group">
										<label>API key</label><br>
										<input type="text" {% if api %} value="{{ setting['sms_api'] }}" {% else %} readonly="readonly"{% endif %} class="form-control" name="setting[sms_api]" id="sms_api">
									</div>
								</div>
							{% endif %}
							<input type="submit" class="btn btn-primary" value="{{ l('Сохранить') }}">
						</form>
					</div>
					<div class="form-group col-md-7">
						<p>{{ l('SMS рассылка используется в GetMoreCalls') }}</p>
						{% if setting.sms_test == 1 %}
							<h1>{{ l('Баланс:') }} {{ balance | raw }}</h1>
							{% if filemtime %}
								<p class="text-primary">{{ l('Данные актуальны на') }} <b>{{ filemtime }}</b>, <a href="{{ update }}">{{ l('Обновить сейчас') }}</a></p>
							{% endif %}
						{% endif %}
					</div>
				</div>
			</div>
		</div>
		{% endif %}
	</div>
</div>
{% endblock %}
{% block footer %}
{{ render_copyright() | raw }}
{% endblock%}
