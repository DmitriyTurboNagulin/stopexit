<style type="text/css">

@-webkit-keyframes pulse {
	0% {-webkit-transform: scale(0.5);opacity: 0.3;}
	50% {-webkit-transform: scale(0.7);opacity: 0.2;}
	100% {-webkit-transform: scale(0.5);opacity: 0;}
}
@-moz-keyframes pulse {
	0% {-moz-transform: scale(0.5);opacity: 0.3;}
	50% {-moz-transform: scale(0.7);opacity: 0.2;}
	100% {-moz-transform: scale(0.5);opacity: 0;}
}
@-o-keyframes pulse {
	0% {-o-transform: scale(0.5);opacity: 0.3;}
	50% {-o-transform: scale(0.7);opacity: 0.2;}
	100% {-o-transform: scale(0.5);opacity: 0;}
}
@-ms-keyframes pulse {
	0% {-ms-transform: scale(0.5);opacity: 0.3;}
	50% {-ms-transform: scale(0.7);opacity: 0.2;}
	100% {-ms-transform: scale(0.5);opacity: 0;}
}
@keyframes pulse {
	0% {transform: scale(0.5);opacity: 0.3;}
	50% {transform: scale(0.7);opacity: 0.2;}
	100% {transform: scale(0.5);opacity: 0;}
}


@-webkit-keyframes pulse2 {
	0% {-webkit-transform: scale(2);opacity: 0;}
	100% {-webkit-transform: scale(0.6);opacity: 0.8;}
}
@-moz-keyframes pulse2 {
	0% {-moz-transform: scale(2);opacity: 0;}
	100% {-moz-transform: scale(0.6);opacity: 0.8;}
}
@-o-keyframes pulse2 {
	0% {-o-transform: scale(2);opacity: 0;}
	100% {-o-transform: scale(0.6);opacity: 0.8;}
}
@-ms-keyframes pulse2 {
	0% {-ms-transform: scale(2);opacity: 0;}
	100% {-ms-transform: scale(0.6);opacity: 0.8;}
}
@keyframes pulse2 {
	0% {transform: scale(2);opacity: 0;}
	100% {transform: scale(0.6);opacity: 0.8;}
}

#anchor_button{{ anchor_setting_id }}:hover #anchor_icon{{ anchor_setting_id }} {
	-webkittext-shadow: 0 0 12px rgba(255,255,255, 1)!important;
	-moz-text-shadow: 0 0 12px rgba(255,255,255, 1)!important;
	-ms-text-shadow: 0 0 12px rgba(255,255,255, 1)!important;
	-o-text-shadow: 0 0 12px rgba(255,255,255, 1)!important;
	text-shadow: 0 0 12px rgba(255,255,255, 1)!important;
}

#anchor_button{{ anchor_setting_id }}:hover #anchor_body{{ anchor_setting_id }}{
	-webkit-box-shadow: 0 0 0 0 {{ anchor_setting_background_step_10 }}!important;
	-moz-box-shadow: 0 0 0 0 {{ anchor_setting_background_step_10 }}!important;
	-ms-box-shadow: 0 0 0 0 {{ anchor_setting_background_step_10 }}!important;
	-o-box-shadow: 0 0 0 0 {{ anchor_setting_background_step_10 }}!important;
	box-shadow: 0 0 0 0 {{ anchor_setting_background_step_10 }}!important;

}

#anchor_button{{ anchor_setting_id }}:hover{
	opacity: 1!important;
}

</style>
	<div id="anchor_button{{ anchor_setting_id }}" onclick="{{ anchor_target }}"
	style="display: block;
		position:absolute;
		{{ anchor_setting_position }}
		height: {{ anchor_setting_size + 135 }}px;
		width: {{ anchor_setting_size + 135 }}px;
		-webkit-backface-visibility: hidden;
		-webkit-transform: translateZ(0);
		opacity: {{ anchor_setting_opacity }};
		-webkit-transition: all 0.3s ease-in-out;
		-moz-transition: all 0.3s ease-in-out;
		-ms-transition: all 0.3s ease-in-out;
		-o-transition: all  0.3s ease-in-out;
		transition: all  0.3s ease-in-out;
		z-index: 999990!important;
		transform-origin: 50% 50% 0;">
		<div id="anchor_icon{{ anchor_setting_id }}" style="
			cursor: pointer;
		text-align: center;
		width: {{ anchor_setting_size }}px;
		height: {{ anchor_setting_size }}px;
		top: 50%;
		left: 50%;
		margin: -{{ anchor_setting_size/2 }}px 0 0 -{{ anchor_setting_size/2 }}px;
		position: absolute;
		background-position: center center;
		background-repeat: no-repeat;
		z-index: 999993;
		transform-origin: 50% 50% 0;">{{ anchor_setting_icon | raw }}</div>
			{% if anchor_setting_anim_switch == 'on' %}
				<div style="
		height: {{ anchor_setting_size + 45 }}px;
		width: {{ anchor_setting_size + 45 }}px;
		top: 50%;
		left: 50%;
		margin: -{{ (anchor_setting_size + 45)/2 }}px 0 0 -{{ (anchor_setting_size + 45)/2 }}px;
		position: absolute;
		z-index: 999991;
		border: 1px solid {{ anchor_setting_background_step_5 }};
		border-radius: {{ anchor_setting_border_radius }}%;
		-webkit-animation: pulse2 {{ anchor_setting_anim }}s infinite ease-in-out;
		-moz-animation: pulse2 {{ anchor_setting_anim }}s infinite ease-in-out;
		-ms-animation: pulse2 {{ anchor_setting_anim }}s infinite ease-in-out;
		-o-animation: pulse2 {{ anchor_setting_anim }}s infinite ease-in-out;
		animation: pulse2 {{ anchor_setting_anim }}s infinite ease-in-out;
		transform-origin: 50% 50% 0;"></div>
				<div style="transform-origin: 50% 50% 0;"></div>
				<div style="
		height: {{ anchor_setting_size + 95 }}px;
		width: {{ anchor_setting_size + 95 }}px;
		background: {{ anchor_setting_background }};
		border-radius: {{ anchor_setting_border_radius }}%;
		position: absolute;
		top: 50%;
		left: 50%;
		margin: -{{ (anchor_setting_size + 95)/2 }}px 0 0 -{{ (anchor_setting_size + 95)/2 }}px;
		z-index: 999991;
		-webkit-animation: pulse 2.5s infinite ease-in-out;
		-moz-animation: pulse 2.5s infinite ease-in-out;
		-ms-animation: pulse 2.5s infinite ease-in-out;
		-o-animation: pulse 2.5s infinite ease-in-out;
		animation: pulse 2.5s infinite ease-in-out;
		transform-origin: 50% 50% 0;"></div>
			{% endif %}
			{% if anchor_setting_back_enable == 'on' %}
				<div id="anchor_body{{ anchor_setting_id }}" style="
		height: {{ anchor_setting_body_size }}px;
		width: {{ anchor_setting_body_size }}px;
		position: absolute;
		top: 50%;
		left: 50%;
		margin: -{{ (anchor_setting_body_size)/2 }}px 0 0 -{{ (anchor_setting_body_size)/2 }}px;
		border-radius: {{ anchor_setting_border_radius }}%;
		z-index: 999992;
		box-shadow: 0 0 0 15px {{ anchor_setting_background_step_10 }};
		-webkit-box-shadow: 0 0 0 15px {{ anchor_setting_background_step_10 }};
		-moz-box-shadow: 0 0 0 15px {{ anchor_setting_background_step_10 }};
		-ms-box-shadow: 0 0 0 15px {{ anchor_setting_background_step_10 }};
		-o-box-shadow: 0 0 0 15px {{ anchor_setting_background_step_10 }};
		box-shadow: 0 0 0 {{ anchor_setting_shadow }}px {{ anchor_setting_background_step_10 }};
		-webkit-transition: box-shadow 0.8s ease-in-out;
		-moz-transition: box-shadow 0.8s ease-in-out;
		-ms-transition: box-shadow 0.8s ease-in-out;
		-o-transition: box-shadow  0.8s ease-in-out;
		transition: box-shadow  0.8s ease-in-out;
		background: {{ anchor_setting_background }};
		transform-origin: 50% 50% 0;"></div>
			{% endif %}
	</div>
