<nav class="navbar navbar-inverse navbar-static-top header" role="navigation" style="margin-bottom:0">
	<a class="navbar-brand" href="{{siteUrl('/admin/')}}">
		{{ app_name }}
	</a>
	<div class="pull-right selectLang">
		{% for s in all_lang %}
			<button class="btn btn-{% if lang == s %}success{% else %}default{% endif %} btn-xs" type="submit">{{ s }}</button>
		{% endfor %}
	</div>
</nav>
