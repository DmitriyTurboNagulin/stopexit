{% extends 'main.twig.php' %}

{% block body %}
	<div id="wrapper">
		{{include('admin/topbar.twig.php')}}
		{{include('admin/sidebar.twig.php')}}
		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="page-header">{{ header }}</h3>
                </div>
            </div>
		</div>
	</div>
{% endblock %}

{% block footer %}
{{ render_copyright() | raw }}
{% endblock%}
