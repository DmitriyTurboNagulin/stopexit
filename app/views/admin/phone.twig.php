{% extends 'main.twig.php' %}
{% block body %}
<div id="wrapper">
	{{include('admin/topbar.twig.php')}}
	{{include('admin/sidebar.twig.php')}}
	<div id="page-wrapper">
		<div class="row">
			<div class="panel panel-default" style="margin-top:15px;">
				<div id="phone" class="panel-heading">
					{{ header }}
					{% if call|length > 0 %}
					<a href="{{ showPhoneChart }}" data-remote="false" data-toggle="modal" data-target="#showChart" class="btn btn-xs btn-success">{{ l('Показать график') }}</a>
					{% endif %}
				</div>
				{% if call > 1 %}
				<table class="table table-hover">
				<thead>
					<tr>
						<th>
							<ul class="nav nav-pills" role="tablist">
								<li role="presentation">
									<a href="#phone">№</a>
								</li>
							</ul>
						</th>
						<th>
							<ul class="nav nav-pills" role="tablist">
								<li role="presentation">
									<a href="#phone">{{ l('Номер телефона') }}</a>
								</li>
							</ul>
						</th>
						<th>
							<ul class="nav nav-pills" role="tablist">
								<li role="presentation">
									<a href="{{ urlFilter }}">{{ l('Дата') }} <i class="fa fa-sort"></i></a></th>
								</li>
							</ul>
						<th>
							<ul class="nav nav-pills" role="tablist">
								<li role="presentation">
									<a href="#" id="drop6" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ l('Id предложения') }} <i class="fa fa-filter"></i></a>
									<ul id="menu3" class="dropdown-menu" aria-labelledby="drop6">
										<li><a href="{{ urlOffer }}all/">{{ l('Все') }}</a></li>
										{% for id in call %}
											<li><a href="{{ urlOffer }}{{ id }}/">{{ id }}</a></li>
										{% endfor %}
									</ul>
								</li>
							</ul>
						</th>
					</tr>
				</thead>
				<tbody>
					{% for phone in phones %}
					<tr>
						<td scope="row">{{ phone.phone_id }}</td>
						<td scope="row">{{ phone.phone_number }}</td>
						<td scope="row">{{ phone.created_at }}</td>
						<td scope="row">{{ phone.call_id }}</td>
					</tr>
					{% endfor %}
				</tbody>
				</table>
				{% else %}
				<p style="text-align:center;font-weight:bold;padding:20px 10px 10px 10px;">{{ l('База пуста') }}</p>
				{% endif %}
			</div>
			{% if count > 1 %}
			<div style="padding:10px;">
				{% for i in 1..count %}
					<a class="btn btn-{% if i == page %}success{% else %}default{% endif %}" href="{{ urlOffer }}{{ offer }}/{{ i }}/" role="button">{{ i }}</a>
				{% endfor %}
			</div>
			{% endif %}
		</div>
	</div>
</div>
<div class="modal fade" id="showChart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		</div>
	</div>
</div>
{% endblock %}
{% block footer %}
{{ render_copyright() | raw }}
{% endblock%}
