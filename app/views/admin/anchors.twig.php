{% extends 'main.twig.php' %}

{% block body %}
<div id="show"></div>
<div id="wrapper">
    {{include('admin/topbar.twig.php')}}
    {{include('admin/sidebar.twig.php')}}
    <div id="page-wrapper">
        <div class="row">
            <div class="panel panel-default" style="margin-top:15px;">
            <div class="panel-heading">{{ header }} {% if access == true %}<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal">{{ l('Создать иконку') }}</button>{% endif %}</div>
                <table class="table table-hover">
                <thead>
                    <tr>
                        <th>{{ l('№') }}</th>
                        <th>{{ l('Название') }}</th>
                        <th style="width:100px;">{{ l('Действие') }}</th>
                    </tr>
                </thead>
                <tbody>
                    {% for anchor in anchors %}
                    <tr {% if anchor.deleted_at %}class="trash"{% endif %} data-url="{{ trashCancel }}{{ anchor.anchor_id }}">
                        <th scope="row">{{ anchor.anchor_id }}</th>
                        <td>{{ anchor.anchor_name }}</td>
                        <td>
                            <a href="{{ anchShow }}" title="{{ l('Показать пример') }}" class="linkShow btn btn-default btn-xs" data-id="{{ anchor.anchor_id }}"><i class="glyphicon glyphicon-eye-open"></i></a>
                            <a href="{{ anchEdit }}{{ anchor.anchor_id }}" title="{{ l('Редактировать') }}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-edit"></i></a>
                            {% if access == true %}<a href="{{ anchDelete }}{{ anchor.anchor_id }}" title="{{ l('Удалить') }}" class="delete-offer btn btn-default btn-xs"><i class="glyphicon glyphicon-trash"></i></a>{% endif %}
                        </td>
                    </tr>
                    {% endfor %}
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form action="{{ anchCreate }}" method="post" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">{{ l('Создать иконку-предложение') }}</h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>{{ l('Название иконки') }} <small>({{ l('отображается в админке') }})</small></label>
                    <input type="name" class="form-control" name="anchor_name" value="{{ anchor.anchor_name }}" placeholder="{{ l('Название иконки') }}">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ l('Закрыть') }}</button>
                <input type="submit" class="btn btn-primary" value="{{ l('Добавить') }}">
            </div>
        </form>
    </div>
</div>
<div id="confirm" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>{{ l('Эта иконка была удаленна, восстановить?') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ l('Отмена') }}</button>
                <a href="" id="btn-confirm" class="btn btn-primary">{{ l('Восстановить') }}</a>
            </div>
        </div>
    </div>
</div>
<div id="delete" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>{{ l('Удалить эту иконку?') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ l('Отмена') }}</button>
                <a href="" id="btn-delete" class="btn btn-primary">{{ l('Удалить') }}</a>
            </div>
        </div>
    </div>
</div>
{% endblock %}
{% block footer %}
{{ render_copyright() | raw }}
{% endblock%}
