{% extends 'main.twig.php' %}
{% block body %}
<style>
	body {
		background:#2c3742;
	}
</style>
{% if flash.message %}
	<div class="alert alert-danger alert-dismissable">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		{{flash.message}}
	</div>
{% endif %}
<div style="padding-top:10%;text-align:center;width:900px;margin:0 auto;">
	<h1 style="color:#fff;font-size:37px;">{{ title }}</h1><br>
	{% if enable_app == FALSE %}
		<p class="bg-danger text-danger" style="padding:10px;border-radius:5px;">{{ l('Cрок действия лицензии истек') }}</p>
	{% else %}
	<form class="col-xs-6" role="form" method="post" action="{{siteUrl('login')}}">
		<input type="hidden" name="openid_auth" value="1">
		<input type="hidden" name="redirect" value="{{ flash.redirect ? : redirect }}">
		<button type="submit" style="float:right;" class="btn btn-default btn-lg">{{ l('Для сотрудников Netpeak') }}</button>
	</form>
	<form class="col-xs-6" role="form" method="post" action="{{siteUrl('login')}}">
		<input type="hidden" name="oauth_auth" value="1">
		<input type="hidden" name="redirect" value="{{ flash.redirect ? : redirect }}">
		<button type="submit" style="float:left;" class="btn btn-default btn-lg">{{ l('Для клиентов Netpeak') }}</button>
	</form>
	{% endif %}
	<div style="text-align:center;width:900px;margin:0 auto;color:#fff;clear:both;">
		{{ render_copyright() | raw }}
	</div>
</div>
{% endblock %}
