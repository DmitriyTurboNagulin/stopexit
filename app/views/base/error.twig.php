{% extends 'main.twig.php' %}

{% block body %}
<div class="container">
	<div class="row">
		<div class="login-panel panel panel-default" style="margin-top:40px">
			<div class="panel-heading">
				<h3 class="panel-title">Ошибка</h3>
			</div>
			<div class="panel-body">
				<div class="alert alert-danger alert-dismissable">{{error_msg}}</div>
			</div>
		</div>
	</div>
</div>
{% endblock %}
