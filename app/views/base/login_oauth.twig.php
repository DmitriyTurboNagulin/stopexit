{% extends 'main.twig.php' %}
{% block body %}
<div>
	<div class="alert alert-danger alert-dismissable" style="display:none;">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<span id="error_msg"></span>
	</div>
	<form id="oauth_form" method="post" action="{{urlFor('login_oauth')}}">
		<input type="hidden" name="oauth_auth" value="1"/>
	</form>
</div>
{% endblock %}
{% block jscode %}
	jQuery(document).ready(function($) {
		if (location.href.indexOf("#") != -1) {
			hash = decodeURIComponent(location.href.split('#')[1]);
			var hash_params = hash.split('&');
			var form_data = {};
			for(var i = 0; i < hash_params.length; i++) {
				var tmp_param = hash_params[i].split('=');
				form_data[tmp_param[0]] = tmp_param[1];
			}

			$.each(form_data, function(name,value) {
				var input = $("<input>").attr("type", "hidden").attr("name", name).val(value);
				$('#oauth_form').append($(input));
			});
		}
		$('#oauth_form').submit();
	});
{% endblock %}
