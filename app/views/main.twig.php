<!DOCTYPE html>
<html>
	<head>
		<title>{{title}} | {% if app_name is defined %}{{ app_name }}{% endif %}</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
{% for metaname, metavalue in meta %}
		<meta name="{{metaname}}" value="{{metavalue}}" />
{% endfor %}
		<script type="text/javascript">
			var global = {{global|json_encode|raw}}
		</script>
{% for cssfile in css.plugins %}
		<link rel="stylesheet" href="{{assetUrl}}plugins/{{cssfile}}" />
{% endfor %}
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
{% for cssfile in css.external %}
		<link rel="stylesheet" href="{{cssfile}}" />
{% endfor %}
{% for cssfile in css.internal %}
		<link rel="stylesheet" href="{{assetUrl}}css/{{cssfile}}" />
{% endfor %}
	</head>
	<body>
{% block body %}{% endblock %}
{% if enable_footer %}
	<div id="footer" class="navbar-bottom row-fluid">
		<div class="navbar-inner">
			<div class="container">
				{% block footer %}{% endblock %}
			</div>
		</div>
	</div>
{% endif %}
{% for jsfile in js.external %}
		<script src="{{jsfile}}"></script>
{% endfor %}
{% for jsfile in js.plugins %}
		<script src="{{assetUrl}}plugins/{{jsfile}}"></script>
{% endfor %}
{% for jsfile in js.internal %}
		<script src="{{assetUrl}}js/{{jsfile}}"></script>
{% endfor %}
		<script>
{{ block('jscode') }}
		</script>
	</body>
</html>
