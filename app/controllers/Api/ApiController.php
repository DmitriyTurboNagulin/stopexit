<?php
namespace Api;

use \App;
use \Input;
use \Cache;

use \Settings_model;

class ApiController extends ApiBaseController {

	public function getIndex() {
		App::render(200, array('data' => 'OK'));
	}

	public function getPing() {
		App::render(200, array('data' => 'OK'));
	}

	public function getSetting() {
		$p_data = $this->get_api_data();
		$p_data = array_filter($p_data, 'strlen');
		if (empty($p_data)) {
			App::render(404, array('error' => TRUE, 'error_message' => 'empty data'));
		}
		$data = Settings_model::get_settings_by_keys($p_data);
		if (empty($data)) {
			App::render(404, array('error' => TRUE, 'error_message' => 'not found settings'));
		}
		App::render(200, array('data' => $data));
	}

	public function postEdit_setting() {
		$p_data = $this->get_api_data();
		if (empty($p_data)) {
			App::render(404, array('error' => TRUE, 'error_message' => 'empty data'));
		}

		$settings_keys_to_clear_cache = array(
			'company_copyright' => array(
				'company_copyright',
			),
		);

		$p_data_keys = array_keys($p_data);
		$old_data = Settings_model::get_settings_by_keys($p_data_keys);
		$clear_cache_keys = array();
		foreach ($p_data as $key => $value) {
			if (isset($old_data[$key])) {
				Settings_model::setting_edit($key, $value);
				// Если поменялось значение, то необходимо сбросить кэш
				if (isset($settings_keys_to_clear_cache[$key])) {
					foreach ($settings_keys_to_clear_cache[$key] as $value2) {
						$clear_cache_keys[] = $value2;
					}
				}
			} else {
				Settings_model::setting_add($key, $value);
			}
		}

		if (!empty($clear_cache_keys)) {
			foreach ($clear_cache_keys as $cache_key) {
				Cache::forget($cache_key);
			}
		}
		App::render(200, array('data' => 'OK'));
	}

	public function postChange_expire_date() {
		$p_data = $this->get_api_data();
		$key = 'expire_date';
		if (!isset($p_data[$key])) {
			App::render(404, array('error' => TRUE, 'error_message' => 'expire date key not found'));
		}

		$tmp_expire_date_unix = strtotime($p_data[$key]);

		if ($tmp_expire_date_unix === FALSE) {
			App::render(404, array('error' => TRUE, 'error_message' => 'expire date not valid'));
		}

		$setting_expire_date_old = Settings_model::get_setting_by_key('expire_date');
		if ($setting_expire_date_old === FALSE) {
			$setting_expire_date_old = '0000-00-00';
		}
		$setting_expire_date_new = date('Y-m-d', $tmp_expire_date_unix);
		Settings_model::setting_edit('expire_date', $setting_expire_date_new);
		$ret = array(
			'expire_date' => $setting_expire_date_new,
			'expire_date_old' => $setting_expire_date_old,
		);
		App::render(200, array('data' => $ret));
	}

}
