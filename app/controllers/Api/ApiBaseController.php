<?php

namespace Api;

use \App;
use \View;
use \Input;
use \Crypt;

class ApiBaseController {

	public function __construct() {
		View::reset();
	}

	protected function get_api_data() {
		$p_data = Input::post('data');
		$p_data = json_decode(Crypt::decode($p_data), TRUE);
		return $p_data;
	}

}
