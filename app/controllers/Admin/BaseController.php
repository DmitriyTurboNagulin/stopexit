<?php

namespace Admin;

use \App;
use \Menu;
use \Module;

class BaseController extends \BaseController {

	public function __construct() {
		parent::__construct();
		$this->data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';
		$this->data['enable_footer'] = TRUE;
		$this->data['all_lang'] = \I18n_model::getAllowLang();
		$this->data['lang'] = \I18n_model::getLang();

		$adminMenu = Menu::create('admin_sidebar');
		$index = $adminMenu->createItem('index', array(
			'label' => l('Инструкция по работе'),
			'icon'  => 'question-circle',
			'url'   => 'admin'
		));
		$adminMenu->addItem('index', $index);

		$phone = $adminMenu->createItem('phone', array(
			'label' => l('Список телефонов'),
			'icon'  => 'phone',
			'url'   => 'admin/phone'
		));
		$adminMenu->addItem('phone', $phone);

		$offers = $adminMenu->createItem('offers', array(
			'label' => l('Stopexit'),
			'icon'  => 'minus-circle',
			'url'   => 'admin/offers'
		));
		$adminMenu->addItem('offers', $offers);

		$GetMoreCalls = $adminMenu->createItem('GetMoreCalls', array(
			'label' => l('GetMoreCalls'),
			'icon'  => 'phone',
			'url'   => 'admin/moreCalls'
		));
		$adminMenu->addItem('getMoreCalls', $GetMoreCalls);

		$anchors = $adminMenu->createItem('anchors', array(
			'label' => l('Иконка-предложение'),
			'icon'  => 'anchor',
			'url'   => 'admin/anchors'
		));
		$adminMenu->addItem('anchors', $anchors);

		$settings = $adminMenu->createItem('settings', array(
			'label' => l('Настройки'),
			'icon'  => 'gear',
			'url'   => 'admin/settings'
		));
		$adminMenu->addItem('settings', $settings);

		$install = $adminMenu->createItem('install', array(
			'label' => l('Как установить?'),
			'icon'  => 'code',
			'url'   => 'admin/install'
		));
		$adminMenu->addItem('install', $install);

		$logs = $adminMenu->createItem('logs', array(
			'label' => l('Просмотр логов'),
			'icon'  => 'list-alt',
			'url'   => 'admin/logs'
		));
		$adminMenu->addItem('logs', $logs);

		$settings = $adminMenu->createItem('logout', array(
			'label' => l('Выйти'),
			'icon'  => 'sign-out',
			'url'   => 'logout'
		));
		$adminMenu->addItem('logout', $settings);

		$this->loadCss('admin/sb-admin.css');
		$this->loadJs('admin/sb-admin.js');
		$this->loadJs("metismenu/src/metisMenu.js", array('location' => 'plugins'));
	}

}
