<?php
namespace Admin;

if (!defined('APP_PATH')) exit('No direct script access allowed');

use \Base_model;
use \Menu;
use \View;
use \Input;
use \Crypt;
use \Offer_model;
use \Settings_model;
use \OfferSetting_model;
use \Call_model;
use \CallSetting_model;
use \CallTemplate_model;
use \OfferTemplate_model;
use \OfferStat_model;
use \Anchor_model;
use \AnchorSetting_model;
use \Image_model;
use \Logs_model;
use \Response;
use \Phone_model;
use \SMSService_model;
use Illuminate\Database\Capsule\Manager as DB;

class AdminController extends BaseController {

	protected $email;

	public function __construct() {
		parent::__construct();
		$this->email = Base_model::get_user();
	}

	public function getIndex() {
		$this->data['header'] = $this->data['title'] = l('Инструкция по работе');
		Menu::get('admin_sidebar')->setActiveMenu('index');
		$setting = Settings_model::get_settings_by_keys('manual');
		$this->data['manual'] = $setting['manual'];
		View::display('admin/manual.twig.php', $this->data);
	}

	public function getMoreCalls() {
		$this->data['title'] = $this->data['header'] = l('GetMoreCalls');
		Menu::get('admin_sidebar')->setActiveMenu('GetMoreCalls');
		$this->data['access'] = Base_model::user_access();
		$this->data['linkCreate'] = $this->siteUrl('admin/createCall/');
		$this->data['linkShow'] = $this->siteUrl('admin/previewCall');
		$this->data['linkEdit'] = $this->siteUrl('admin/moreCall/');
		$this->data['linkDelete'] = $this->siteUrl('admin/moreCallDelete/');
		$this->data['trashCancel'] = $this->siteUrl('admin/trashCallCancel/');
		$this->data['calls'] = Call_model::withTrashed()->get()->toArray();
		foreach ($this->data['calls'] as &$value) {
			if ($value['call_show_num'] == 0) {
				$value['ctr'] = 0;
			} else {
				$value['ctr'] = round($value['call_yes_click'] / $value['call_show_num'],2);
			}
		}
		View::display('admin/call.twig.php', $this->data);
	}

	public function postPreviewCall() {
		$id = Input::post('id');
		$setting = CallSetting_model::where('call_id', $id)->first()->toArray();
		$setting = CallSetting_model::style($setting);
		echo  str_replace('opacity:0;', 'opacity:1;', View::fetch('admin/show_call.twig.php', $setting));
		die;
	}

	public function postPreviewOffer() {
		$id = Input::post('id');
		$setting = OfferSetting_model::where('offer_id', $id)->first()->toArray();
		$setting = OfferSetting_model::style($setting);
		echo  str_replace('opacity:0;', 'opacity:1;', View::fetch('admin/show.twig.php', $setting));
		die;
	}

	public function postPreviewAnchor() {
		$id = Input::post('id');
		$setting = AnchorSetting_model::where('anchor_id', $id)->first()->toArray();
		$setting = AnchorSetting_model::style($setting);
		echo  View::fetch('admin/show_anchor.twig.php', $setting);
		die;
	}

	public function postCreateCall() {
		$call_name = Input::post('call_name');
		if (!empty($call_name)) {
			Call_model::insert(array(
				'call_name' => $call_name,
				'call_show_num' => 0,
				'call_yes_click' => 0,
				));
			$id = DB::getPdo()->lastInsertId();
			$call_setting = array(
				'call_setting_email' => Crypt::encode($this->email),
				'call_id' => $id,
				'call_setting_position' => 'center',
				'call_setting_company' => Crypt::encode(l('Ваша компания')),
				'call_setting_company_size' => '25',
				'call_setting_text' => Crypt::encode(l('Хотите связаться с нами?')),
				'call_setting_text_size' => '22',
				'call_setting_manger_name' => 'Jane Doe',
				'call_setting_manager_position' => 'none',
				'call_setting_yes_size' => '16',
				'call_setting_placeholder' => l('Введите ваш номер телефона'),
				'call_setting_yes' => l('Хочу!'),
				'call_setting_width' => '450',
				'call_setting_height' => '250',
				'call_setting_photo_position' => 'center',
				'call_setting_background' => '#c9c9c9',
				'call_setting_color' => '#f2e4e4',
				'call_setting_background_btn' => '#a80e0e',
				'call_setting_color_btn' => '#bababa',
				'call_setting_radius' => '20',
				'call_setting_border_type' => 'solid',
				'call_setting_border_color' => '#080808',
				'call_setting_effect' => 'instantly',
				'call_setting_logo' => '',
				'call_setting_num_error' => '',
				'call_setting_background_image' => '',
				'call_setting_background_position' => '',
				'call_setting_border_color2' => '',
				'call_setting_view' => '',
				'call_setting_event' => '',
				'call_setting_x_sec' => '',
				'call_setting_position' => '',
				'call_setting_effect' => '',
				'call_setting_templates' => '',
				'call_setting_templates_white' => '',
				'call_setting_templates_black' => '',
				'call_setting_domain' => '',
				'call_setting_days' => '',
				'call_setting_view' => '',
				'call_setting_view_x_days' => '',
				'call_setting_position' => 'center',
			);
			CallSetting_model::insert($call_setting);
			$data = array(
				'user' => $this->email,
				'new' => $call_name,
				'event' => 'create_call',
			);
			$Logs_model = new Logs_model();
			$Logs_model->insert($data);
			Response::redirect($this->siteUrl('admin/moreCall/' . $id));
		} else {
			Response::redirect($this->siteUrl('admin/moreCalls/'));
		}

	}



	public function getOffers() {
		$this->data['title'] = $this->data['header'] = l('Предложения');
		Menu::get('admin_sidebar')->setActiveMenu('offers');
		$this->data['access'] = Base_model::user_access();
		$this->data['linkCreate'] = $this->siteUrl('admin/createOffer/');
		$this->data['linkShow'] = $this->siteUrl('admin/previewOffer');
		$this->data['linkEdit'] = $this->siteUrl('admin/offer/');
		$this->data['linkDelete'] = $this->siteUrl('admin/offerDelete/');
		$this->data['trashCancel'] = $this->siteUrl('admin/trashCancel/');
		$this->data['offers'] = Offer_model::withTrashed()->get()->toArray();
		foreach ($this->data['offers'] as &$value) {
			$value['yesno'] = $value['offer_yes_click'] + $value['offer_no_click'];
			if ($value['offer_show_num'] == 0) {
				$value['ctr'] = 0;
				$value['yesctr'] = 0;
				$value['noctr'] = 0;
			} else {
				$value['ctr'] = round($value['yesno'] / $value['offer_show_num'], 2);
				$value['yesctr'] = round($value['offer_yes_click'] / $value['offer_show_num'], 2);
				$value['noctr'] = round($value['offer_no_click'] / $value['offer_show_num'], 2);
			}
		}
		View::display('admin/offers.twig.php', $this->data);
	}

	public function getAnchors() {
		$this->data['title'] = $this->data['header'] = l('Иконка-предложение');
		Menu::get('admin_sidebar')->setActiveMenu('anchors');
		$this->data['access'] = Base_model::user_access();
		$this->data['anchCreate'] = $this->siteUrl('admin/createAnchor/');
		$this->data['anchShow'] = $this->siteUrl('admin/previewAnchor');
		$this->data['anchEdit'] = $this->siteUrl('admin/anchor/');
		$this->data['anchDelete'] = $this->siteUrl('admin/anchorDelete/');
		$this->data['trashCancel'] = $this->siteUrl('admin/anchTrashCancel/');
		$this->data['anchors'] = Anchor_model::withTrashed()->get()->toArray();
		View::display('admin/anchors.twig.php', $this->data);
	}

	public function getAnchTrashCancel($get) {
		$id = (int) reset($get);
		$few = Anchor_model::withTrashed()->where('anchor_id', $id)->update(array('deleted_at' => NULL));
		$anchor = Anchor_model::where('anchor_id', $id)->first()->toArray();
			$data = array(
				'user' => $this->email,
				'new' => $anchor['anchor_name'],
				'event' => 'anchor_restore',
			);
			$Logs_model = new Logs_model();
			$Logs_model->insert($data);
		Response::redirect($this->siteUrl('admin/anchors/'));
	}

	public function getAnchorDelete($get) {
		$id = (int) reset($get);
		if ($id == 0) {
			Response::redirect($this->siteUrl('admin/anchors/'));
		}
		$anchor = Anchor_model::where('anchor_id', $id)->first()->toArray();
		Anchor_model::where('anchor_id', $id)->delete();
		$data = array(
			'user' => $this->email,
			'old' => $anchor['anchor_name'],
			'event' => 'delete_anchor',
		);
		$Logs_model = new Logs_model();
		$Logs_model->insert($data);
		Response::redirect($this->siteUrl('admin/anchors/'));
	}

	public function postCreateAnchor() {
		$anchor_name = Input::post('anchor_name');
		if (!empty($anchor_name)) {
			Anchor_model::insert(array('anchor_name' => $anchor_name));
			$id = DB::getPdo()->lastInsertId();
			$anch_setting = array(
				'anchor_id' => $id,
				'anchor_setting_email' => Crypt::encode($this->email),
				'anchor_setting_background' => '#e0dcf7',
				'anchor_setting_icon' => 'whatsapp-round',
				'anchor_setting_icon_color' => '#8000ff',
				'anchor_setting_shadow' => '0',
				'anchor_setting_size' => '60',
				'anchor_setting_anim_switch' => 'on',
				'anchor_setting_border_radius' => '50',
				'anchor_setting_anim' => '2.5',
				'anchor_setting_position' => 'right_bottom',
				'anchor_setting_margin_vert' => '0',
				'anchor_setting_margin_horiz' => '0',
				'anchor_setting_back_enable' => 'on',
				'anchor_setting_opacity' => '0.8',
				'anchor_setting_icon_ref' => '',
			);
			AnchorSetting_model::insert($anch_setting);
			$data = array(
				'user' => $this->email,
				'new' => $anchor_name,
				'event' => 'create_anchor',
			);
			$Logs_model = new Logs_model();
			$Logs_model->insert($data);
			Response::redirect($this->siteUrl('admin/anchor/' . $id));
		} else {
			Response::redirect($this->siteUrl('admin/anchors/'));
		}

	}

	public function getAnchor($get) {
		$id = (int) reset($get);
		$this->loadJs('admin/bootstrap-datepicker.bg.min.js', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-datepicker.ru.min.js', array('position' => 'first'));
		$this->loadCss('admin/bootstrap-datepicker.css', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-datepicker.js', array('position' => 'first'));
		$this->loadJs('admin/jquery.deserialize.js', array('position' => 'first'));
		$this->loadJs('admin/jquery.sticky-kit.min.js', array('position' => 'first'));
		$this->loadJs('admin/ajaxupload.3.6.js', array('position' => 'first'));
		$this->loadJs('admin/jquery.bootstrap-touchspin.js', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-colorpicker.min.js', array('position' => 'first'));
		$this->loadJs('admin/anchor.js', array('position' => 'last'));
		$this->loadJs('admin/bootstrap-slider.js', array('position' => 'first'));
		$this->loadCss('admin/bootstrap-slider.css', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-switch.js', array('position' => 'first'));
		$this->loadCss('admin/bootstrap-switch.css', array('position' => 'first'));
		$this->data['showAnchor'] = $this->siteUrl('admin/showAnchor/');
		$this->data['anchSave'] = $this->siteUrl('admin/anchorSave/');
		$this->data['cancelSave'] = $this->siteUrl('admin/anchors/');
		$this->data['showLink'] = $this->siteUrl('admin/show/');
		$this->data['uploadLink'] = $this->siteUrl('admin/upload/');
		$this->data['showAnchHistory'] = $this->siteUrl('admin/showAnchHistory/');
		$this->data['deleteLink'] = $this->siteUrl('admin/delete/');
		$this->data['connectOffer'] = $this->siteUrl('admin/connectOffer/');
		$this->data['title'] = $this->data['header'] = l('Иконка-сообщение');
		Menu::get('admin_sidebar')->setActiveMenu('anchors');
		$this->data['access'] = Base_model::user_access();
		$this->data['anchor'] = Anchor_model::where('anchor_id', $id)->first()->toArray();
		$anchor_setting = AnchorSetting_model::where('anchor_id', $id)->get()->toArray();
		$this->data['anchor_setting'] = reset($anchor_setting);
		$this->data['trashed'] = AnchorSetting_model::where('anchor_id', $id)->take(20)->orderBy('deleted_at','desc')->onlyTrashed()->get()->toArray();
		array_walk($this->data['trashed'], function(&$item) {
			$item = array(
				'id' => $item['anchor_setting_id'],
				'email' => $item['anchor_setting_email'],
				'deleted' => $item['deleted_at'],
				'data' => http_build_query(array('anchor_setting' => $item)),
			);
		});
		View::display('admin/anchor.twig.php', $this->data);
	}

	public function postConnectOffer() {
		$toconnect = (array) Input::post('data');
		$anchor_id = (int) Input::post('id');
		if (!empty($toconnect)) {
			Offer_model::whereIn('offer_id', $toconnect)->update(array('anchor_id' => $anchor_id));
		}
	}

	public function postAnchorSave() {
		$post = Input::post();
		$anchor_setting = Input::post('anchor_setting');
		$anchor_id = $post['anchor_id'];
		$anchor = array(
			'anchor_name' => $post['anchor_name'],
		);
		Anchor_model::withTrashed()->where('anchor_id', $anchor_id)->update($anchor);
		$res = AnchorSetting_model::where('anchor_id', $post['anchor_id'])->delete();
		$anchor_setting['anchor_id'] = $post['anchor_id'];
		$anchor_setting['anchor_setting_email'] = $this->email;
		AnchorSetting_model::insert($anchor_setting);
			$data = array(
				'user' => $this->email,
				'new' => $anchor['anchor_name'],
				'event' => 'edit_anchor',
			);
		$Logs_model = new Logs_model();
		$Logs_model->insert($data);
		Response::redirect($this->siteUrl('admin/anchor/' . $post['anchor_id'] . '/'));
	}

	public function postShowAnchor() {
		$post = Input::post('data');
		parse_str($post, $post);
		$setting = $post['anchor_setting'];
		$setting = AnchorSetting_model::style($setting);
		$setting['anchor_setting_position'] = 'position:fixed;bottom:15px;right:10px;';
		$setting['anchor_setting_id'] = 'post';
		echo  str_replace(array_keys($this->array), $this->array, View::fetch('admin/show_anchor.twig.php', $setting));
		die;
	}

	public function postShowAnchHistory() {
		$id = (int) Input::post('id');
		$anchor = AnchorSetting_model::withTrashed()->where('anchor_setting_id', $id)->first()->toArray();
		$anchor = AnchorSetting_model::style($anchor);
		echo str_replace(array_keys($this->array), $this->array, View::fetch('admin/show_anchor.twig.php', $anchor));
		die;
	}

	public function postCreateOffer() {
		$offer_name = Input::post('offer_name');
		if (!empty($offer_name)) {
			Offer_model::insert(array(
				'offer_name' => $offer_name,
				'offer_show_num' => 0,
				'offer_yes_click' => 0,
				'offer_no_click' => 0,
				));
			$id = DB::getPdo()->lastInsertId();
			$offer_setting = array(
				'offer_setting_email' => Crypt::encode($this->email),
				'offer_id' => $id,
				'offer_setting_position' => 'center',
				'offer_setting_width' => '450',
				'offer_setting_height' => '250',
				'offer_setting_company_size' => '24',
				'offer_setting_text' => Crypt::encode(l('Хотите получить особенную скидку?')),
				'offer_setting_text_size' => '20',
				'offer_setting_free' => '-50%',
				'offer_setting_free_size' => '60',
				'offer_setting_yes' => l('Хочу!'),
				'offer_setting_yes_size' => '26',
				'offer_setting_yes_link' => '#',
				'offer_setting_background' => '#eaeaea',
				'offer_setting_background_btn' => '#c24848',
				'offer_setting_background_size' => 'full',
				'offer_setting_color' => '#000000',
				'offer_setting_color_btn' => '#ffffff',
				'offer_setting_border_type' => 'double',
				'offer_setting_border_color' => '#c24848',
				'offer_setting_border_color2' => '#f5f5f5',
				'offer_setting_logo' => '',
				'offer_setting_company' => Crypt::encode('Your Company'),
				'offer_setting_background_image' => '',
				'offer_setting_no' => '',
				'offer_setting_event' => '',
				'offer_setting_x_sec' => '',
				'offer_setting_effect' => '',
				'offer_setting_templates' => '',
				'offer_setting_templates_black' => '',
				'offer_setting_templates_white' => '',
				'offer_setting_domain' => '',
				'offer_setting_days' => '',
				'offer_setting_view' => '',
				'offer_setting_view_x_days' => '',
			);
			OfferSetting_model::insert($offer_setting);
			$data = array(
				'user' => $this->email,
				'new' => $offer_name,
				'event' => 'offer_create',
			);
			$Logs_model = new Logs_model();
			$Logs_model->insert($data);
			Response::redirect($this->siteUrl('admin/offer/' . $id));
		} else {
			Response::redirect($this->siteUrl('admin/offers/'));
		}
	}

	public function getTrashCancel($get) {
		$id = (int) reset($get);
		if ($id > 0) {
			$few = Offer_model::withTrashed()->where('offer_id', $id)->update(array('deleted_at' => NULL));
			$offer = Offer_model::where('offer_id', $id)->first()->toArray();
			$data = array(
				'user' => $this->email,
				'new' => $offer['offer_name'],
				'event' => 'offer_restore',
			);
			$Logs_model = new Logs_model();
			$Logs_model->insert($data);
		}
		Response::redirect($this->siteUrl('admin/offers/'));
	}

	public function getTrashCallCancel($get) {
		$id = (int) reset($get);
		if ($id > 0) {
			$few = Call_model::withTrashed()->where('call_id', $id)->update(array('deleted_at' => NULL));
			$call = Call_model::where('call_id', $id)->first()->toArray();
			$data = array(
				'user' => $this->email,
				'new' => $call['call_name'],
				'event' => 'call_restore',
			);
			$Logs_model = new Logs_model();
			$Logs_model->insert($data);
		}
		Response::redirect($this->siteUrl('admin/moreCalls/'));
	}

	private $array = array(
		'opacity:0;' => 'opacity:1;',
		'z-index:2147483647;' => 'z-index:1;',
	);

	public function postShowOffer() {
		$post = Input::post('data');
		parse_str($post, $post);
		$setting = $post['offer_setting'];
		$setting = OfferSetting_model::style($setting);
		$setting['position'] = 'position:absolute;';
		echo '<div>' . str_replace(array_keys($this->array), $this->array, View::fetch('admin/show.twig.php', $setting)) . '</div>';
		die;
	}

	public function postTemplateSave() {
		$post = Input::post();
		parse_str($post['data'], $post);
		$offer_setting = $post['offer_setting'];
		$offer_setting['offer_template_name'] = $post['offer_template_name'];
		$list = OfferTemplate_model::getList();
		$offer_template = array_intersect_key($offer_setting, array_flip($list));
		$check = OfferTemplate_model::where('offer_template_name', $offer_template['offer_template_name'])->get()->toArray();
		if (!empty($check)) {
			$data = array(
				'user' => $this->email,
				'new' => $offer_setting['offer_template_name'],
				'event' => 'edit_offer_template',
			);
			$Logs_model = new Logs_model();
			$Logs_model->insert($data);
			OfferTemplate_model::where('offer_template_name', $offer_template['offer_template_name'])->update($offer_template);
		} else {
			$data = array(
				'user' => $this->email,
				'new' => $offer_setting['offer_template_name'],
				'event' => 'create_offer_template',
			);
			$Logs_model = new Logs_model();
			$Logs_model->insert($data);
			OfferTemplate_model::insert($offer_template);
		}
		Response::redirect($this->siteUrl('admin/offer/' . $post['offer_id'] . '/'));
	}

	public function postShowTemplate() {
		$id = (int) Input::post('id');
		$template = OfferTemplate_model::where('offer_template_id', $id)->first()->toArray();
		echo http_build_query(array('offer_setting' => $template));
		die;
	}

	public function postDeleteTemplate() {
		$todelete = (array) Input::post('ids');
		if (!empty($todelete)) {
			$templates = OfferTemplate_model::whereIn('offer_template_id', $todelete)->get()->toArray();
			echo (int) OfferTemplate_model::whereIn('offer_template_id', $todelete)->delete();
			foreach ($templates as $template) {
				$data = array(
				'user' => $this->email,
				'old' => $template['offer_template_name'],
				'event' => 'delete_offer_template',
			);
			$Logs_model = new Logs_model();
			$Logs_model->insert($data);
			}
		}
	}

	public function postOfferSave() {
		$post = Input::post();
		$offer_setting = Input::post('offer_setting');
		$offer_setting['anchor_id'] = Input::post('offer_anchor');
		if ($offer_setting['anchor_id'] == 'none') {
			$offer_setting['anchor_id'] = NULL;
		}
		$offer_id = $post['offer_id'];
		$offer = array(
			'offer_name' => $post['offer_name'],
		);
		Offer_model::withTrashed()->where('offer_id', $offer_id)->update($offer);
		$res = OfferSetting_model::where('offer_id', $post['offer_id'])->delete();
		$offer_setting['offer_id'] = $post['offer_id'];
		$offer_setting['offer_setting_email'] = $this->email;
		$tmp = strtotime($offer_setting['offer_setting_start']);
		// 1451602800 это timestamp для первого января 2016 года
		if ($tmp > 1451602800) {
			$offer_setting['offer_setting_start'] = date('Y-m-d H:i:s', $tmp);
		} else {
			$offer_setting['offer_setting_start'] = NULL;
		}
		$tmp = strtotime($offer_setting['offer_setting_stop']);
		if ($tmp > 1451602800) {
			$offer_setting['offer_setting_stop'] = date('Y-m-d H:i:s', $tmp);
		} else {
			$offer_setting['offer_setting_stop'] = NULL;
		}
		if ($offer_setting['offer_setting_start'] > $offer_setting['offer_setting_stop']) {
			$offer_setting['offer_setting_start'] = NULL;
		}
		if (!empty($post['offer_setting_days'])) {
			$offer_setting['offer_setting_days'] = json_encode($post['offer_setting_days']);
		}
		$offer_setting['offer_setting_email'] = Crypt::encode($offer_setting['offer_setting_email']);
		$offer_setting['offer_setting_text'] = Crypt::encode($offer_setting['offer_setting_text']);
		$offer_setting['offer_setting_company'] = Crypt::encode($offer_setting['offer_setting_company']);
		// array_walk($offer_setting, function(&$item){
		// 	$item['offer_setting_email'] = Crypt::encode($item['offer_setting_email']);
		// 	$item['offer_setting_text'] = Crypt::encode($item['offer_setting_text']);
		// 	$item['offer_setting_company'] = Crypt::encode($item['offer_setting_company']);

		// });
		OfferSetting_model::insert($offer_setting);
		$data = array(
			'user' => $this->email,
			'new' => $offer['offer_name'],
			'event' => 'offer_create',
		);
		$Logs_model = new Logs_model();
		$Logs_model->insert($data);
		Response::redirect($this->siteUrl('admin/offer/' . $post['offer_id'] . '/'));
	}

	public function getOfferDelete($get) {
		$id = (int) reset($get);
		if ($id > 0) {
			$offer = Offer_model::where('offer_id', $id)->first()->toArray();
			Offer_model::where('offer_id', $id)->delete();
		}
		$data = array(
			'user' => $this->email,
			'old' => $offer['offer_name'],
			'event' => 'offer_delete',
		);
		$Logs_model = new Logs_model();
		$Logs_model->insert($data);
		Response::redirect($this->siteUrl('admin/offers/'));
	}

	public function getMoreCall($get) {
		$id = (int) reset($get);
		if ($id == 0) {
			Response::redirect($this->siteUrl('admin/moreCalls/'));
		}
		Menu::get('admin_sidebar')->setActiveMenu('GetMoreCalls');
		$this->loadJs('admin/bootstrap-datepicker.bg.min.js', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-datepicker.ru.min.js', array('position' => 'first'));
		$this->loadCss('admin/bootstrap-datepicker.css', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-datepicker.js', array('position' => 'first'));
		$this->loadJs('admin/jquery.deserialize.js', array('position' => 'first'));
		$this->loadJs('admin/jquery.sticky-kit.min.js', array('position' => 'first'));
		$this->loadJs('admin/ajaxupload.3.6.js', array('position' => 'first'));
		$this->loadJs('admin/jquery.bootstrap-touchspin.js', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-colorpicker.min.js', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-slider.js', array('position' => 'first'));
		$this->loadCss('admin/bootstrap-slider.css', array('position' => 'first'));
		$this->loadJs('admin/call.js', array('position' => 'last'));
		$this->data['showCallLink'] = $this->siteUrl('admin/showCalls/');
		$this->data['deleteCallLink'] = $this->siteUrl('admin/deleteCall/');
		$this->data['showLink'] = $this->siteUrl('admin/show/');
		$this->data['callSaveLink'] = $this->siteUrl('admin/callSave/');
		$this->data['uploadLink'] = $this->siteUrl('admin/upload/');
		$this->data['deleteLink'] = $this->siteUrl('admin/delete/');
		$this->data['showTemplateLink'] = $this->siteUrl('admin/callShowTemplate/');
		$this->data['deleteTemplateLink'] = $this->siteUrl('admin/CallDeleteTemplate/');
		$this->data['templateSaveLink'] = $this->siteUrl('admin/callTemplateSave/');
		$this->data['days'] = OfferSetting_model::getDays();
		$this->data['time'] = date('H:i');
		$this->data['access'] = Base_model::user_access();
		$anchors = Anchor_model::get()->toArray();
		$settings = OfferSetting_model::get()->toArray();
		$taken = array();
		foreach ($settings as $setting) {
			$taken[] = $setting['anchor_id'];
		}
		$taken = array_filter($taken);
		if (!empty($taken)) {
			$this->data['anchors'] = Anchor_model::whereNotIn('anchor_id', $taken) ->get()->toArray();
		} else {
			$this->data['anchors'] = Anchor_model::get()->toArray();
		}
		$this->data['templates'] = CallTemplate_model::select('call_template_id', 'call_template_name')->get()->toArray();
		$this->data['call'] = Call_model::where('call_id', $id)->first()->toArray();
		$this->data['title'] = $this->data['header'] = $this->data['call']['call_name'];
		$call_setting = CallSetting_model::where('call_id', $id)->get()->toArray();
		$this->data['call_setting'] = reset($call_setting);
		$this->data['call_setting'] = CallSetting_model::style($this->data['call_setting']);
		$this->data['trashed'] = CallSetting_model::where('call_id', $id)->take(20)->orderBy('deleted_at','desc')->onlyTrashed()->get()->toArray();
		if (!empty($this->data['trashed'])) {
			array_walk($this->data['trashed'], function(&$item) {
				$item = array(
					'id' => $item['call_setting_id'],
					'email' => $item['call_setting_email'],
					'deleted' => $item['deleted_at'],
					'data' => http_build_query(array('call_setting' => $item)),
				);
			});
		}
		$this->data['cancelSave'] = $this->siteUrl('admin/moreCalls/');
		$this->data['linkSave'] = $this->siteUrl('admin/callSave/');
		$this->data['call_setting']['position'] = 'position:absolute;';
		$this->data['show_call'] = View::fetch('admin/show_call.twig.php', $this->data['call_setting']);
		$this->data['show_call'] = str_replace(array_keys($this->array), $this->array, $this->data['show_call']);
		View::display('admin/moreCall.twig.php', $this->data);
	}

	public function postCallSave() {
		$post = Input::post();
		$call_setting = Input::post('call_setting');
		$call_setting['anchor_id'] = Input::post('call_anchor');
		if ($call_setting['anchor_id'] == 'none') {
			$call_setting['anchor_id'] = NULL;
		}
		$call_id = $post['call_id'];
		$call = array(
			'call_name' => $post['call_name'],
		);
		Call_model::withTrashed()->where('call_id', $call_id)->update($call);
		$res = CallSetting_model::where('call_id', $post['call_id'])->delete();
		$call_setting['call_id'] = $post['call_id'];
		$call_setting['call_setting_email'] = $this->email;
		$tmp = strtotime($call_setting['call_setting_start']);
		// 1451602800 это timestamp для первого января 2016 года
		if ($tmp > 1451602800) {
			$call_setting['call_setting_start'] = date('Y-m-d H:i:s', $tmp);
		} else {
			$call_setting['call_setting_start'] = NULL;
		}
		$tmp = strtotime($call_setting['call_setting_stop']);
		if ($tmp > 1451602800) {
			$call_setting['call_setting_stop'] = date('Y-m-d H:i:s', $tmp);
		} else {
			$call_setting['call_setting_stop'] = NULL;
		}
		if($call_setting['call_setting_start'] > $call_setting['call_setting_stop']) {
			$call_setting['call_setting_start'] = NULL;
		}
		if(!empty($post['call_setting_days'])) {
			$call_setting['call_setting_days'] = json_encode($post['call_setting_days']);
		}
		$call_setting['call_setting_email'] = Crypt::encode($call_setting['call_setting_email']);
		$call_setting['call_setting_text'] = Crypt::encode($call_setting['call_setting_text']);
		$call_setting['call_setting_company'] = Crypt::encode($call_setting['call_setting_company']);
		CallSetting_model::insert($call_setting);
		$data = array(
			'user' => $this->email,
			'new' => $call['call_name'],
			'event' => 'create_call',
		);
		$Logs_model = new Logs_model();
		$Logs_model->insert($data);
		Response::redirect($this->siteUrl('admin/moreCall/' . $post['call_id'] . '/'));
	}

	public function postShowCalls() {
		$post = Input::post('data');
		parse_str($post, $post);
		$setting = $post['call_setting'];
		$setting = CallSetting_model::style($setting);
		$setting['position'] = 'position:absolute;';
		echo  str_replace(array_keys($this->array), $this->array, View::fetch('admin/show_call.twig.php', $setting));
		die;
	}

	public function getMoreCallDelete($get) {
		$id = (int) reset($get);
		if ($id > 0) {
			$call = Call_model::where('call_id', $id)->first()->toArray();
			Call_model::where('call_id', $id)->delete();
		}
		$data = array(
			'user' => $this->email,
			'old' => $call['call_name'],
			'event' => 'delete_call',
		);
		$Logs_model = new Logs_model();
		$Logs_model->insert($data);
		Response::redirect($this->siteUrl('admin/moreCalls/'));
	}

	public function postCallTemplateSave() {
		$post = Input::post();
		parse_str($post['data'], $post);
		$call_setting = $post['call_setting'];
		$call_setting['call_template_name'] = $post['call_template_name'];
		$list = CallTemplate_model::getList();
		$call_template = array_intersect_key($call_setting, array_flip($list));
		$check = CallTemplate_model::where('call_template_name', $call_template['call_template_name'])->get()->toArray();
		if (!empty($check)) {
		$data = array(
			'user' => $this->email,
			'new' => $call_setting['call_template_name'],
			'event' => 'edit_call_template',
		);
		$Logs_model = new Logs_model();
		$Logs_model->insert($data);
			CallTemplate_model::where('call_template_name', $call_template['call_template_name'])->update($call_template);
		} else {
		$data = array(
			'user' => $this->email,
			'new' => $call_setting['call_template_name'],
			'event' => 'create_call_template',
		);
		$Logs_model = new Logs_model();
		$Logs_model->insert($data);
			CallTemplate_model::insert($call_template);
		}

		Response::redirect($this->siteUrl('admin/moreCall/' . $post['call_id'] . '/'));
	}

	public function postCallShowTemplate() {
		$id = (int) Input::post('id');
		$template = CallTemplate_model::where('call_template_id', $id)->first()->toArray();
		echo http_build_query(array('call_setting' => $template));
		die;
	}

	public function postCallDeleteTemplate() {
		$todelete = (array) Input::post('ids');
		if (!empty($todelete)) {
			$templates = CallTemplate_model::whereIn('call_template_id', $todelete)->get()->toArray();
			echo (int) CallTemplate_model::whereIn('call_template_id', $todelete)->delete();
			foreach ($templates as $template) {
				$data = array(
				'user' => $this->email,
				'old' => $template['call_template_name'],
				'event' => 'delete_call_template',
			);
			$Logs_model = new Logs_model();
			$Logs_model->insert($data);
			}
		}
	}

	public function getOffer($get) {
		$id = (int) reset($get);
		if ($id == 0) {
			Response::redirect($this->siteUrl('admin/offers/'));
		}
		$this->loadJs('admin/bootstrap-datepicker.bg.min.js', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-datepicker.ru.min.js', array('position' => 'first'));
		$this->loadCss('admin/bootstrap-datepicker.css', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-datepicker.js', array('position' => 'first'));
		$this->loadJs('admin/jquery.deserialize.js', array('position' => 'first'));
		$this->loadJs('admin/jquery.sticky-kit.min.js', array('position' => 'first'));
		$this->loadJs('admin/ajaxupload.3.6.js', array('position' => 'first'));
		$this->loadJs('admin/jquery.bootstrap-touchspin.js', array('position' => 'first'));
		$this->loadJs('admin/bootstrap-colorpicker.min.js', array('position' => 'first'));
		$this->loadJs('admin/offer.js', array('position' => 'last'));
		$this->data['showOfferLink'] = $this->siteUrl('admin/showOffer/');
		$this->data['showTemplateLink'] = $this->siteUrl('admin/showTemplate/');
		$this->data['deleteTemplateLink'] = $this->siteUrl('admin/DeleteTemplate/');
		$this->data['showLink'] = $this->siteUrl('admin/show/');
		$this->data['templateSaveLink'] = $this->siteUrl('admin/templateSave/');
		$this->data['uploadLink'] = $this->siteUrl('admin/upload/');
		$this->data['deleteLink'] = $this->siteUrl('admin/delete/');
		$this->data['title'] = $this->data['header'] = l('Предложения');
		$this->data['linkShowCalls'] = $this->siteUrl('admin/showCalls/');
		$this->data['days'] = OfferSetting_model::getDays();
		$this->data['time'] = date('H:i');
		$this->data['access'] = Base_model::user_access();
		$anchors = Anchor_model::get()->toArray();
		$settings = CallSetting_model::get()->toArray();
		$taken = array();
		foreach ($settings as $setting) {
			$taken[] = $setting['anchor_id'];
		}
		$taken = array_filter($taken);
		if (!empty($taken)) {
			$this->data['anchors'] = Anchor_model::whereNotIn('anchor_id', $taken) ->get()->toArray();
		} else {
			$this->data['anchors'] = Anchor_model::get()->toArray();
		}
		Menu::get('admin_sidebar')->setActiveMenu('offers');
		$this->data['templates'] = OfferTemplate_model::select('offer_template_id', 'offer_template_name')->get()->toArray();
		$this->data['offer'] = Offer_model::where('offer_id', $id)->first()->toArray();
		$offer_setting = OfferSetting_model::where('offer_id', $id)->get()->toArray();
		$this->data['offer_setting'] = reset($offer_setting);
		$this->data['offer_setting'] = OfferSetting_model::style($this->data['offer_setting']);
		$this->data['trashed'] = OfferSetting_model::where('offer_id', $id)->take(20)->orderBy('deleted_at','desc')->onlyTrashed()->get()->toArray();
		if (!empty($this->data['trashed'])) {
			array_walk($this->data['trashed'], function(&$item) {
				$item = array(
					'id' => $item['offer_setting_id'],
					'email' => $item['offer_setting_email'],
					'deleted' => $item['deleted_at'],
					'data' => http_build_query(array('offer_setting' => $item)),
				);
			});
		}
		$this->data['cancelSave'] = $this->siteUrl('admin/offers/');
		$this->data['linkSave'] = $this->siteUrl('admin/offerSave/');
		$this->data['offer_setting']['position'] = 'position:absolute;';
		$this->data['show'] = View::fetch('admin/show.twig.php', $this->data['offer_setting']);
		$this->data['show'] = str_replace(array_keys($this->array), $this->array, $this->data['show']);
		View::display('admin/offer.twig.php', $this->data);
	}

	private function convertDate($date) {
		$result = date('Y,', $date) . (date('m', $date) - 1) . date(',d', $date);
		return $result;
	}

	public function getShowPhoneChart() {
		$this->data['phone'] = array();
		$tmp = Phone_model::select('call_id', 'created_at')->orderBy('created_at', 'asc')->get()->toArray();
		$today = time();
		$mindate = strtotime($tmp[0]['created_at']);
		$today = time();
		$dates = array();
		while($mindate < $today) {
			$key = $this->convertDate($mindate);
			$dates[$key] = 0;
			$mindate = strtotime('+1 day', $mindate);
		}
		$key = $this->convertDate($mindate);
		$dates[$key] = 0;
		foreach ($tmp as $value) {
			$call_id = $value['call_id'];
			if (empty($this->data['phone'][$call_id])) {
				$this->data['phone'][$call_id] = $dates;
			}
		}
		foreach ($tmp as $value) {
			$call_id = $value['call_id'];
			$date = strtotime($value['created_at']);
			$date = $this->convertDate($date);
				$this->data['phone'][$call_id][$date]++;
		}
		$tmp = $this->data['phone'];
		$this->data['phone'] = array();
		foreach ($tmp as $num => $item) {
			$this->data['phone'][$num] = '([';
			foreach ($item as $key => $value) {
				$this->data['phone'][$num] .= '[Date.UTC(' . $key . '), ' . intval($value) . '],';
			}
			$this->data['phone'][$num] = rtrim($this->data['phone'][$num], ',') . '])';
		}
		$this->data['highcharts'] = $this->siteUrl('assets/js/admin/highcharts.js');
		$this->data['exporting'] = $this->siteUrl('assets/js/admin/exporting.js');
		View::display('admin/showPhoneChart.twig.php', $this->data);
	}

	public function getPhone($get = array()) {
		$limit = 15;
		$offset = 0;
		$this->data['page'] = 1;
		if (!empty($get[0]) and $get[0] == 'asc') {
			$filter = 'asc';
			$this->data['urlFilter'] = $this->siteUrl('admin/phone/desc/');
		} else {
			$filter = 'desc';
			$this->data['urlFilter'] = $this->siteUrl('admin/phone/asc/');
		}
		if (!empty($get[1]) and $get[1] != 'all') {
			$offer = $get[1];
			$this->data['urlFilter'] .= $offer . '/';
		}
		if (!empty($get[2])) {
			$this->data['page'] = $get[2];
			$offset = $limit * $get[2];
			$offset = $offset - $limit;
		}
		$this->data['call'] = array();
		$this->data['showPhoneChart'] = $this->siteUrl('admin/showPhoneChart/');
		$this->data['urlOffer'] = $this->siteUrl('admin/phone/' . $filter . '/');
		$this->data['title'] = $this->data['header'] = l('Список телефонов');
		Menu::get('admin_sidebar')->setActiveMenu('phone');
		$this->data['url'] = $this->siteUrl('admin/phone/');
		$phones = new Phone_model();
		if (!empty($offer)) {
			$phones = $phones->where('call_id', $offer);
			$this->data['offer'] = $offer;
		} else {
			$this->data['offer'] = 'all';
		}
		$count = $phones->count();
		$this->data['phones'] = $phones->orderBy('created_at', $filter)->take($limit)->skip($offset)->get()->toArray();
		$this->data['count'] = ceil($count / $limit);
		if (!empty($this->data['phones'])) {
			$this->data['call'] = Phone_model::select('call_id')->groupBy('call_id')->get()->toArray();
			if (!empty($this->data['call'])) {
				array_walk($this->data['call'], function(&$item) {
					$item = $item['call_id'];
				});
			}
		}
		if (empty($this->data['count'])) {
			$this->data['count'] = 1;
		}
		View::display('admin/phone.twig.php', $this->data);
	}

	public function getInstall() {
		Menu::get('admin_sidebar')->setActiveMenu('install');
		$this->data['title'] = $this->data['header'] = l('Как установить?');
		$this->data['url'] = $this->siteUrl('js/');
		View::display('admin/install.twig.php', $this->data);
	}

	public function getSettings() {
		$this->data['title'] = $this->data['header'] = l('Настройки GetMoreCalls');
		Menu::get('admin_sidebar')->setActiveMenu('settings');
		$this->loadJs('admin/bootstrap-switch.js', array('position' => 'first'));
		$this->loadCss('admin/bootstrap-switch.css');
		$this->loadJs('admin/setting.js');
		$this->data['access'] = Base_model::user_access();
		$this->data['url'] = $this->siteUrl('admin/saveSettings/');
		$this->data['update'] = $this->siteUrl('admin/settings/?cache');
		$settings_model = new Settings_model();
		$getField = $settings_model->getField();
		$setting = Settings_model::get_settings_by_keys(array_keys($getField));
		if ($setting['sms_service'] == 'mobizon') {
			$this->data['api'] = TRUE;
			$this->data['login'] = FALSE;
			$this->data['pass'] = FALSE;
		} else {
			$this->data['api'] = FALSE;
			$this->data['login'] = TRUE;
			$this->data['pass'] = TRUE;
		}
		$this->data['setting'] = $setting;
		$smsService = new SMSService_model();
		$balance = $smsService->balance(1);
		if ($balance === FALSE) {
			$data['sms_test'] = 0;
		} else {
			$data['sms_test'] = 1;
		}
		$filemtime = NULL;
		$balance = 'Нет данных';
		if ($setting['sms_test']) {
			$smsService = new SMSService_model();
			$balance = (string) $smsService->balance();
			$filemtime = NULL;
			if (!empty($balance)) {
				$cacheBalance = STORAGE_PATH . 'cache/cacheBalance.json';
				$filemtime = filemtime($cacheBalance);
				$filemtime = date('d-m-Y H:i:s', $filemtime);
			}
		}
		$this->data['balance'] = $balance;
		$this->data['filemtime'] = $filemtime;
		View::display('admin/settings.twig.php', $this->data);
	}

	public function postSaveSettings() {
		$settings_model = new Settings_model();
		$Logs_model = new Logs_model();
		$user = $this->email;
		$getField = $settings_model->getField();
		$setting = $settings_model->get_settings_by_keys(array_keys($getField));
		$post = Input::post('setting');
		foreach (array_keys($getField) as $key) {
			if (!empty($post[$key]) && $setting[$key] != $post[$key]) {
				$post[$key] = trim($post[$key], ', ');
				$settings_model->setting_edit($key, $post[$key]);
				$data = array(
					'user' => $user,
					'event' => 'setting_update',
					'new' => $key . ' | ' . $post[$key],
					'old' => $key . ' | ' . $setting[$key],
				);
				$Logs_model->insert($data);
			}
		}
		\Response::redirect($this->siteUrl('admin/settings/'));
	}

	public function getLogs() {
		$this->data['title'] = $this->data['header'] = l('Просмотр логов');
		Menu::get('admin_sidebar')->setActiveMenu('logs');
		$Settings_model = new Settings_model();
		$Logs_model = new Logs_model();
		$this->data['event'] = $Logs_model->getEvent();
		$getField = $Settings_model->getField();
		$logs = $Logs_model->take(1000)->orderBy('created_at', 'desc')->get()->toArray();
		foreach ($logs as $value) {
			if ($value['event'] == 'setting_update') {
				foreach (array('old', 'new') as $key) {
					$tmp = explode(' | ', $value[$key]);
					$tmp[0] = $getField[$tmp[0]];
					$value[$key] = implode(': ', $tmp);
				}
			}
			$this->data['logs'][$value['id']] = $value;
		}
		View::display('admin/logs.twig.php', $this->data);
	}

	private $dir = '/image/';
	public function postDelete() {
		$Image_model = new Image_model();
		$src = Input::post('src');
		if (empty($src)) {
			$output = array(
				'type' => 'danger',
				'close' => TRUE,
				'text' => l('Неправильный путь к файлу'),
			);
		} else {
			$output = $Image_model->delete($src, $this->dir);
		}
		echo json_encode($output);
	}

	public function postShow() {
		$Image_model = new Image_model();
		echo $Image_model->scandir($this->dir);
	}

	public function postUpload() {
		$Image_model = new Image_model();
		$output = $Image_model->upload($this->dir);
		echo json_encode($output);
	}

}
