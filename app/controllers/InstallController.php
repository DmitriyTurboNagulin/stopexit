<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Capsule\Manager as DB;
use \Main;

class InstallController extends BaseController {

	public function index() {
		$this->data['app_name'] = App::getName();
		$this->loadJs('app/install.js');
		$this->data['checkUrl'] = $this->siteUrl('db/check?rand=' . time());
		$this->data['configureUrl'] = $this->siteUrl('db/configure?rand=' . time());
		App::render('install/configure_db.twig.php', $this->data);
	}

	public function checkConnection() {
		$driver = Input::post('dbdriver');
		$success = FALSE;
		$message = '';
		$config  = $this->getPostConfiguration();
		try {
			if (Request::isAjax() === FALSE) {
				throw new Exception(l('Неизвестная ошибка'));
			}
			if ($driver == 'sqlite') {
				$config_database_file_location = Main::get_sqlite_database($config['database']);
				$config_database_file_name = basename($config_database_file_location);
				if (!file_exists($config_database_file_location)) {
					if (!touch($config_database_file_location)) {
						throw new Exception(l('Не удалось создать файл БД "%s"', $config_database_file_name));
					}
				} elseif (!is_writable($config_database_file_location)) {
					throw new Exception(l('У файла %s отсутствуют права на запись.', $config_database_file_name));
				}
			}
			$this->makeConnection($config);
			switch ($driver) {
				case 'mysql':
					$tables = Capsule::select('show tables');
					break;
				case 'sqlite':
					$tables = Capsule::select("SELECT * FROM sqlite_master WHERE `type` = 'table'");
					break;
				case 'pgsql':
					$tables = Capsule::select("SELECT * FROM pg_catalog.pg_tables");
					break;
				default:
					throw new Exception(l('Выбран неизвестный драйвер БД.'));
			}

			$success = TRUE;
			$message = l('Успешное подключение!');
		} catch(Exception $e) {
			$message = $e->getMessage();
		}

		Response::headers()->set('Content-Type', 'application/json');
		Response::setBody(json_encode(
			array(
				'success' => $success,
				'message' => $message,
			)
		));
	}

	public function writeConfiguration() {
		$database_name_file = Main::get_database_name_file();
		$config_file = APP_CONFIG_PATH . $database_name_file;

		$config	= $this->getPostConfiguration();

		$config_driver = trim($config['driver']);
		$config_database = trim($config['database']);

		$config_database_str = '\'' . $config_database . '\'';
		$config_str = <<<CONFIG
<?php
return array(
	'default' => '{$config_driver}',

	'connections'   => array(
		'{$config_driver}' => array(
			'driver' => '{$config['driver']}',
			'host' => '{$config['host']}',
			'database' => {$config_database_str},
			'username' => '{$config['username']}',
			'password' => '{$config['password']}',
			'charset' => '{$config['charset']}',
			'collation' => '{$config['collation']}',
			'prefix' => '{$config['prefix']}'
		)
	)
);

CONFIG;
		if (file_exists($config_file) && !is_writable($config_file)) {
			$config_file_base = basename($config_file);
			throw new Exception(l('Файл %s не доступен для записи. Установите права на запись.', $config_file_base));
		}
		if (!is_writable(APP_CONFIG_PATH)) {
			$config_file_dirname = basename(APP_CONFIG_PATH);
			throw new Exception(l('Директория %s закрыта для записи. Установите права на запись.', $config_file_dirname));
		}
		file_put_contents($config_file, $config_str);
		$this->makeConnection($config);
		$this->migrate();
		Response::redirect($this->siteUrl('admin/?rand=' . time()));
	}

	private function getPostConfiguration() {
		$driver = trim(Input::post('dbdriver'));
		$database = trim(Input::post('dbname'));
		$prefix = trim(Input::post('prefix'));
		if (!empty($prefix)) {
			$prefix = rtrim($prefix, '_') . '_';
		}

		return array(
			'driver' => trim($driver),
			'host' => trim(Input::post('dbhost')),
			'database' => $database,
			'username' => trim(Input::post('dbuser')),
			'password' => trim(Input::post('dbpass')),
			'charset' => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix' => $prefix,
		);
	}

	private function makeConnection($config) {
		$final_config = Main::pre_make_connection_database($config);
		try {
			$capsule = new Capsule();
			$capsule->addConnection($final_config);
			$capsule->setAsGlobal();
			$capsule->bootEloquent();
		} catch(Exception $e) {
			throw $e;
		}
		return TRUE;
	}

	public function update() {
		$this->migrate();
		Response::redirect($this->siteUrl('admin/?rand=' . time()));
	}

	/**
	 * Миграции
	 *
	 * @return void
	 */
	private function migrate() {
		$glob = Helper_model::getMigrationFile();
		if (empty($glob)) {
			exit(l('Отсутствуют миграции'));
		}
		$migrate = Helper_model::migrationNumberBD();
		foreach ($glob as $path) {
			$class = basename($path);
			$class = rtrim($class, '.php');
			$num = ltrim($class, 'm');
			if ($num > $migrate) {
				$class = '\\' . $class;
				$class = new $class();
				$class->up();
				$settings_model = new \Settings_model();
				$settings_model->setting_edit('migrations', $num);
			}
		}
	}

}
