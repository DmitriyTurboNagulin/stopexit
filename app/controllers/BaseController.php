<?php
use \I18n_model;

class BaseController {

	protected $app;
	protected $data;
	protected $allow_location;

	public function __construct() {
		I18n_model::init();
		$this->data = array();

		$allow_location = array('internal', 'external', 'plugins');
		$this->allow_location = $allow_location;

		$this->data['app_name'] = App::getName();

		/** meta tag and information */
		$this->data['meta'] = array(
			'robots' => 'noindex, nofollow',
		);

		/** queued css files */
		$this->data['css'] = array_fill_keys($allow_location, array());

		/** queued js files */
		$this->data['js'] = array_fill_keys($allow_location, array());

		/** prepared message info */
		$this->data['message'] = array(
			'error'	=> array(),
			'info'	=> array(),
			'debug'	=> array(),
		);

		/** global javascript var */
		$this->data['global'] = array();
		/** base dir for asset file */
		$framework_location = Main::framework_location();
		$this->publish('framework_base', $framework_location);

		$this->loadBaseCss();
		$this->loadBaseJs();
	}

	/**
	 * enqueue css asset to be loaded
	 * @param  [string] $css	 [css file to be loaded relative to base_asset_dir]
	 * @param  [array]  $options [location=internal|external, position=first|last|after:file|before:file]
	 */
	protected function loadCss($css, $options = array()) {
		$location = (isset($options['location']) ? $options['location'] : 'internal');
		$position = (isset($options['position']) ? $options['position'] : 'last');

		if (!in_array($css, $this->data['css'][$location])) {
			if ($position == 'first' || $position == 'last') {
				$key = $position;
				$file = '';
			} else {
				list($key, $file) = explode(':', $position);
			}

			switch($key) {
				case 'first':
					array_unshift($this->data['css'][$location], $css);
				break;
				case 'last':
					$this->data['css'][$location][] = $css;
				break;
				case 'before':
				case 'after':
					$varkey = array_keys($this->data['css'][$location], $file);
					if ($varkey) {
						$nextkey = ($key == 'after') ? $varkey[0] + 1 : $varkey[0];
						array_splice($this->data['css'][$location], $nextkey, 0, $css);
					} else {
						$this->data['css'][$location][] = $css;
					}
				break;
			}
		}
	}

	/**
	 * enqueue js asset to be loaded
	 * @param  [string] $js	  [js file to be loaded relative to base_asset_dir]
	 * @param  [array]  $options [location=internal|external, position=first|last|after:file|before:file]
	 */
	protected function loadJs($js, $options = array()) {
		$location = (isset($options['location']) ? $options['location'] : 'internal');
		$position = (isset($options['position']) ? $options['position'] : 'last');

		if (!in_array($js, $this->data['js'][$location])) {
			if ($position == 'first' || $position == 'last') {
				$key = $position;
				$file = '';
			} else {
				list($key, $file) = explode(':', $position);
			}

			switch($key) {
				case 'first':
					array_unshift($this->data['js'][$location], $js);
				break;

				case 'last':
					$this->data['js'][$location][] = $js;
				break;

				case 'before':
				case 'after':
					$varkey = array_keys($this->data['js'][$location], $file);
					if ($varkey) {
						$nextkey = ($key == 'after') ? $varkey[0] + 1 : $varkey[0];
						array_splice($this->data['js'][$location], $nextkey, 0, $js);
					} else {
						$this->data['js'][$location][] = $js;
					}
				break;
			}
		}
	}

	/**
	 * clear enqueued css asset
	 */
	protected function resetCss() {
		$this->data['css'] = array_fill_keys($allow_location, array());
	}

	/**
	 * clear enqueued js asset
	 */
	protected function resetJs() {
		$this->data['js'] = array_fill_keys($allow_location, array());
	}

	/**
	 * remove individual css file from queue list
	 * @param  [string] $css [css file to be removed]
	 */
	protected function removeCss($css) {
		if (empty($css)) {
			return;
		}
		foreach ($this->allow_location as $value) {
			$key = array_keys($this->data['css'][$value], $css);
			if (!empty($key)) {
				array_splice($this->data['css'][$value], $key[0], 1);
			}
		}
	}

	/**
	 * remove individual js file from queue list
	 * @param  [string] $js [js file to be removed]
	 */
	protected function removeJs($js) {
		if (empty($js)) {
			return;
		}
		foreach ($this->allow_location as $value) {
			$key = array_keys($this->data['js'][$value], $css);
			if (!empty($key)) {
				array_splice($this->data['js'][$value], $key[0], 1);
			}
		}
	}

	/**
	 * addMessage to be viewd in the view file
	 */
	protected function message($message, $type = 'info') {
		$this->data['message'][$type] = $message;
	}

	/**
	 * register global variable to be accessed via javascript
	 */
	protected function publish($key, $val) {
		$this->data['global'][$key] = $val;
	}

	/**
	 * remove published variable from registry
	 */
	protected function unpublish($key) {
		unset($this->data['global'][$key]);
	}

	/**
	 * add custom meta tags to the page
	 */
	protected function meta($name, $content) {
		$this->data['meta'][$name] = $content;
	}

	/**
	 * load base css for the template
	 */
	protected function loadBaseCss() {
		$this->loadCss('bootstrap/dist/css/bootstrap.min.css', array('location' => 'plugins'));
		$this->loadCss('font-awesome.min.css');
		$this->loadCss('custom.css');
	}

	/**
	 * load base js for the template
	 */
	protected function loadBaseJs() {
		$this->loadJs('jquery/jquery.min.js', array('location' => 'plugins'));
		$this->loadJs('bootstrap/dist/js/bootstrap.min.js', array('location' => 'plugins'));
	}

	/**
	 * generate siteUrl
	 */
	protected function siteUrl($path) {
		return Request::getRootUri() . '/' . $path;
	}

	protected function json($data) {
		App::response()->header('Content-Type', 'application/json');
		App::response()->body(json_encode($data));
		App::stop();
	}

}

function l($key) {
	$return = \I18n_model::l($key);
	$args = func_get_args();
	$args = array_slice($args, 1);
	if (is_array($args) && count($args)) {
		foreach ($args as $arg) {
			$return = \I18n_model::str_replace_first('%s', $arg, $return);
		}
	}
	return $return;
}
