<?php
use \Settings_model;
use \SMSService_model;

class HomeController extends BaseController {

	private $array = array(
		"\n" => '',
		"\r" => '',
		"\t" => '',
	);

	public function index() {
		Response::redirect($this->siteUrl('admin'));
	}

	/**
	 * Главный js который вызывает форму
	 */
	public function js() {
		$settings_model = new Settings_model;
		$mask = $settings_model->get_setting_by_key('call_mask');
		Response::headers()->set('Content-Type', 'application/javascript');
		$data = array(
			'maskSrc' => '/stopexit/assets/js/vanilla-masker.min.js',
			'mask' => $mask,
		);
		echo str_replace(array_keys($this->array), array_values($this->array), View::fetch('js.twig.php', $data));
	}

	/**
	 * Формирует js который выводит форму
	 */
	public function show() {
		Response::headers()->set('Content-Type', 'application/javascript');
		$href = Input::post('href');
		$url = parse_url($href);
		$echo ='';
		$array = $this->array;
		$array["'"] = '&#39;';
		$clear1 = 'stopexit_RemoveElementsByClass("stopexitBackground");stopexit_RemoveElementsByClass("anchor-offer");';
		$clear2 = 'stopexit_RemoveElementsByClass("getCallsBackground");stopexit_RemoveElementsByClass("anchor-call");';
		$offer_ids = Offer_model::select('offer_id')->get()->toArray();
		if (!empty($offer_ids)) {
			$offer_id = array();
            foreach ($offer_ids as $value) {
                $offer_id[] = $value['offer_id'];
            }
			$offer_setting = OfferSetting_model::whereIn('offer_id', $offer_id)->get()->toArray();
		}
		$call_ids = Call_model::select('call_id')->get()->toArray();
		if (!empty($call_ids)) {
			$call_id = array();
            foreach ($call_ids as $value) {
                $call_id[] = $value['call_id'];
            }
			$call_setting = CallSetting_model::whereIn('call_id', $call_id)->get()->toArray();
		}
		if (!empty($call_setting)) {
			foreach ($call_setting as $call) {
				$id = $call['call_setting_id'];
				if (!empty($call['call_setting_domain'])) {
					$domain = explode("\n", $call['call_setting_domain']);
					$domain = array_filter($domain);
					$domain = array_map('trim', $domain);
					if (array_search($url['host'], $domain) === FALSE) {
						continue;
					}
				}
				if (!empty($call['call_setting_start'])) {
					$now = date('Y-m-d H-i-s');
					if ($call['call_setting_start'] > $now) {
						continue;
					} else {
						if ($call['call_setting_stop'] < $now) {
							continue;
						}
					}
				}
				if (!empty($call['call_setting_templates'])) {
						if($call['call_setting_templates'] == 'main') {
							if ($url['path'] != '/') {
							continue;
							}
						} elseif ($call['call_setting_templates'] == 'optional') {
							if (!empty($call['call_setting_templates_white'])) {
								$white_urls = explode("\n", $call['call_setting_templates_white']);
								$white_urls = array_filter($white_urls);
								$url_path = str_replace('/', '\/', trim($url['path']));
								$matches = preg_grep('/' . $url_path . '/i', $white_urls);
								if(empty($matches)) {
									continue;
								}
							}
							if (!empty($call['call_setting_templates_black'])) {
								$black_urls = explode("\n", $call['call_setting_templates_black']);
								$black_urls = array_filter($black_urls);
								$mainpage = array_search('/', $black_urls);
								$flag = TRUE;
								if ($url['path'] == '/') {
									if ($mainpage === FALSE) {
										$flag = FALSE;
									} else {
										$flag = TRUE;
									}
								}
								$url_path = str_replace('/', '\/', trim($url['path']));
								$matches = preg_grep('/' . $url_path . '/i', $black_urls);
								if(!empty($matches) && $flag === TRUE) {
									continue;
								}
							}
						}
					}
				$call = CallSetting_model::style($call);
				if	(!empty($call['call_setting_days'])) {
					$day = date('D');
					if	(!empty($call['call_setting_days'][$day])) {
						$time = date('H');
						if (empty($call['call_setting_days'][$day][$time])) {
							continue;
						}
					} else {
						continue;
					}
				}
				$call_id = $call['call_id'];
				if (!empty($call['call_setting_view'])) {
					$cookies = \Request::cookies();
					$now = time();
					if ($call['call_setting_view'] == 'new') {
						if (!empty($cookies['getcalls_' . $call_id])) {
							continue;
						}
					} elseif ($call['call_setting_view'] == 'x_days') {
						$days = $call['call_setting_view_x_days'];
						$check = $now - $cookies['getcalls_' . $call_id];
						$check = strtotime($check);
						if ($check < $days) {
							continue;
						}
					}
				}
				$view_call_anchor = '';
				$view_call = str_replace('"', '\"', View::fetch('admin/show_call.twig.php', $call));
				if (!empty($call['anchor_id'])) {
					$anchor_id = $call['anchor_id'];
					$anchor_setting = AnchorSetting_model::where('anchor_id', $anchor_id)->get()->toArray();
					$anchor_setting = AnchorSetting_model::style($anchor_setting[0]);
					$data['anchor_setting_id'] = $anchor_setting['anchor_setting_id'];
					$data['anchor_setting_position'] = $anchor_setting['anchor_setting_position'];
					$data['anchor_setting_size'] = $anchor_setting['anchor_setting_size'];
					$anchor_setting['anchor_target'] = 'sendStatGA("Netpeak GetMoreCalls", "Open form",  "id-' . $call_id . '", "false");';
					$view_call_anchor = str_replace('"', '\"', View::fetch('admin/show_anchor.twig.php', $anchor_setting));
				}
				$echo .= 'document.getElementsByTagName("body")[0].innerHTML += "';
				$echo .= '<div style=\"background:url(' . $call['call_setting_background_image'] .  ');position:absolute; width:1px;height:1px; top:-90000px;left:-90000px;\"></div>';
				$echo .= '<div style=\"background:url(' . $call['call_setting_manager_photo'] .  ');position:absolute; width:1px;height:1px; top:-90000px;left:-90000px;\"></div>';
				$echo .= '<div style=\"background:url(' . $call['call_setting_logo'] .  ');position:absolute; width:1px;height:1px; top:-90000px;left:-90000px;\"></div>";';
				$data['event_id'] = $call_id;
				$data['id'] = $id;
				$data['clear'] = $clear2;
				$data['displayClass'] = 'getCallsBackground';
				$data['class'] = 'getcalls';
				$data['action'] = 'Open form';
				$data['interaction'] = 'auto';
				$data['label'] = 'id-' . $call_id;
				$data['category'] = 'Netpeak GetMoreCalls';
				if ($call['call_setting_event'] == 'val_is_true') {
					$data['functionName'] = 'getCalls';
					$data['view'] = $view_call;
					$echo .= View::fetch('forms/trueval.twig.php', $data);
				} elseif ($call['call_setting_event'] == 'x_sec_page') {
					$data['timeout'] = $call['call_setting_x_sec'];
					$data['view'] = $view_call;
					$echo .= View::fetch('forms/xsec.twig.php', $data);
				} elseif ($call['call_setting_event'] == 'escape') {
					$data['view'] = $view_call;
					$echo .= View::fetch('forms/escape.twig.php', $data);
				} elseif ($call['call_setting_event'] == 'off' && !empty($view_call_anchor)) {
					$data['view'] = $view_call;
					$data['interaction'] = 'anchor_id-' . $call['anchor_id'];
					$data['view_anchor'] = $view_call_anchor;
					$data['anchorClass'] = 'anchor-call';
					$echo .= View::fetch('forms/anchor.twig.php', $data);
				} elseif ($call['call_setting_event'] == 'enter') {
					$data['view'] = $view_call;
					$echo .= View::fetch('forms/enter.twig.php', $data);
				}
			}
		}
		$array = $this->array;
		$array["'"] = '&#39;';
		if (!empty($offer_setting)) {
			foreach ($offer_setting as $setting) {
				$id = $setting['offer_setting_id'];
				if (!empty($setting['offer_setting_domain'])) {
					$domain = explode("\n", $setting['offer_setting_domain']);
					$domain = array_filter($domain);
					$domain = array_map('trim', $domain);
					if (array_search($url['host'], $domain) === FALSE) {
						continue;
					}
				}
				if (!empty($setting['offer_setting_start'])) {
					$now = date('Y-m-d H-i-s');
					if ($setting['offer_setting_start'] > $now) {
						continue;
					} else {
						if ($setting['offer_setting_stop'] < $now) {
							continue;
						}
					}
				}
				if (!empty($setting['offer_setting_templates'])) {
						if ($setting['offer_setting_templates'] == 'main') {
							if ($url['path'] != '/') {
							continue;
							}
						} elseif ($setting['offer_setting_templates'] == 'optional') {
							if (!empty($setting['offer_setting_templates_white'])) {
								$white_urls = explode("\n", $setting['offer_setting_templates_white']);
								$white_urls = array_filter($white_urls);
								$url_path = str_replace('/', '\/', trim($url['path']));
								$matches = preg_grep('/' . $url_path . '/i', $white_urls);
								if (empty($matches)) {
									continue;
								}
							}
							if (!empty($setting['offer_setting_templates_black'])) {
								$black_urls = explode("\n", $setting['offer_setting_templates_black']);
								$black_urls = array_filter($black_urls);
								$flag = TRUE;
								if ($url['path'] == '/') {
									$mainpage = array_search('/', $black_urls);
									if ($mainpage === FALSE) {
										$flag = FALSE;
									} else {
										$flag = TRUE;
									}
								}
								$url_path = str_replace('/', '\/', trim($url['path']));
								$matches = preg_grep('/' . $url_path . '/i', $black_urls);
								if (!empty($matches) && $flag === TRUE) {
									continue;
								}
							}
						}
					}
				$setting = OfferSetting_model::style($setting);
				if	(!empty($setting['offer_setting_days'])) {
					$day = date('D');
					if	(!empty($setting['offer_setting_days'][$day])) {
						$time = date('H');
						if (empty($setting['offer_setting_days'][$day][$time])) {
							continue;
						}
					} else {
						continue;
					}
				}
				$offer_id = $setting['offer_id'];
				if (!empty($setting['offer_setting_view'])) {
					$cookies = \Request::cookies();
					$now = time();
					if ($setting['offer_setting_view'] == 'new') {
						if (!empty($cookies['stopexit_' . $offer_id])) {
							continue;
						}
					} elseif ($setting['offer_setting_view'] == 'x_days') {
						$days = $setting['offer_setting_view_x_days'];
						$check = $now - $cookies['stopexit_' . $offer_id];
						$check = strtotime($check);
						if ($check < $days) {
							continue;
						}
					}
				}
				$view_anchor = '';
				$view = str_replace('"', '\"', View::fetch('admin/show.twig.php', $setting));
				if (!empty($setting['anchor_id'])) {
					$anchor_id = $setting['anchor_id'];
					$anchor_setting = AnchorSetting_model::where('anchor_id', $anchor_id)->get()->toArray();
					$anchor_setting = AnchorSetting_model::style($anchor_setting[0]);
					$data['anchor_setting_id'] = $anchor_setting['anchor_setting_id'];
					$data['anchor_setting_position'] = $anchor_setting['anchor_setting_position'];
					$data['anchor_setting_size'] = $anchor_setting['anchor_setting_size'];
					$anchor_setting['anchor_target'] = 'sendStatGA("Netpeak Stopexit", "Open form",  "id-' . $offer_id . '", "false");';
					$view_anchor = str_replace('"', '\"', View::fetch('admin/show_anchor.twig.php', $anchor_setting));
				}
				$echo .= 'document.getElementsByTagName("body")[0].innerHTML += "';
				$echo .= '<div style=\"background:url(' . $setting['offer_setting_background_image'] .  ');position:absolute; width:1px;height:1px; top:-90000px;left:-90000px;\"></div>';
				$echo .= '<div style=\"background:url(' . $setting['offer_setting_logo'] . ');position:absolute; width:1px;height:1px; top:-90000px;left:-90000px;\"></div>";';
				$data['event_id'] = $offer_id;
				$data['id'] = $id;
				$data['clear'] = $clear1;
				$data['displayClass'] = 'stopexitBackground';
				$data['class'] = 'stopexit';
				$data['action'] = 'Open form';
				$data['interaction'] = 'auto';
				$data['label'] = 'id-' . $offer_id;
				$data['category'] = 'Netpeak Stopexit';
				if ($setting['offer_setting_event'] == 'val_is_true') {
					$data['functionName'] = 'stopexit';
					$data['view'] = $view;
					$echo .= View::fetch('forms/trueval.twig.php', $data);
				} elseif ($setting['offer_setting_event'] == 'x_sec_page') {
					$data['timeout'] = $setting['offer_setting_x_sec'];
					$data['view'] = $view;
					$echo .= View::fetch('forms/xsec.twig.php', $data);
				} elseif ($setting['offer_setting_event'] == 'escape') {
					$data['view'] = $view;
					$echo .= View::fetch('forms/escape.twig.php', $data);
				} elseif ($setting['offer_setting_event'] == 'off' && !empty($view_anchor)) {
					$data['view'] = $view;
					$data['action'] = 'Anchor click';
					$data['interaction'] = 'anchor_id-' . $setting['anchor_id'];
					$data['view_anchor'] = $view_anchor;
					$data['anchorClass'] = 'anchor-offer';
					$echo .= View::fetch('forms/anchor.twig.php', $data);
				} elseif ($setting['offer_setting_event'] == 'enter') {
					$data['view'] = $view;
					$data['event'] = 'enter';
					$echo .= View::fetch('forms/enter.twig.php', $data);
				}
			}
		}
		echo str_replace(array_keys($array), array_values($array), $echo);
	}

	/**
	 * Обработка данных из формы
	 */
	public function proceed() {
		$post = Input::post();
		if (empty($post['id']) || empty($post['type'])) {
			return;
		}
		$id = $post['id'];
		$setclass = $post['type'];
		if ($setclass == 'stopexit') {
			Offer_model::where('offer_id', $id)->increment('offer_show_num');
		} else {
			Call_model::where('call_id', $id)->increment('call_show_num');
		}
		$curdate = time();
		$app = \Slim\Slim::getInstance();
		$app->setCookie($setclass . '_' . $id, $curdate, '2050-01-01', '/');
	}

	public function offerCTR() {
		$post = Input::post();
		$id = $post['id'];
		$increment = Offer_model::where('offer_id', $id);
		if(!empty($post['yesclick'])) {
			$increment->increment('offer_yes_click');
		} elseif (!empty($post['noclick'])) {
			$increment->increment('offer_no_click');
		}
	}

	/**
	 * Отправка телефона из формы GetMoreCalls
	 */
	public function phone() {
		$domain = $_SERVER['SERVER_NAME'];
		$post = Input::post();
		if (!empty($post['id']) and !empty($post['phone']) and strlen($post['phone']) > 6) {
			$r = (int) \Phone_model::insert(array(
				'call_id' => $post['id'],
				'phone_number' => \Crypt::encode($post['phone']),
			));
			Call_model::where('call_id', $post['id'])->increment('call_yes_click');
			$settings_model = new Settings_model;
			$getField = $settings_model->getField();
			$settings = $settings_model->get_settings_by_keys(array_keys($getField));
			if ($settings['sender_email'] != '' && $settings['sender_name'] != ''  && $settings['reciever_email'] != '') {
				$mail_model = new Mail_model;
				$reciever = $settings['reciever_email'];
				$subject = l('С сайта %s поступила заявка на обратный звонок. Номер: %s',$domain, $post['phone']);
				$message = l('С сайта %s поступила заявка на обратный звонок. Номер: %s. Перезвоните! ',$domain, $post['phone']);
				$mail_model->sendmail($reciever, $subject, $message);
			}
			if ($settings['sms_status'] == 'on') {
				$sms = new SMSService_model;
				$msg = l('С сайта %s поступила заявка на обратный звонок. Номер: %s. Перезвоните! ',$domain, $post['phone']);
				$phone = $settings['sms_reciever'];
				$sms = $sms->send($phone, $msg);
			}
			echo($r);die;
		}
	}

	/**
	 * Страница логина
	 */
	public function login() {
		$base_model = new Base_model();
		if ($base_model->user_is_auth()) {
			$url_admin = App::urlFor('admin');
			Response::redirect($url_admin);
		} else {
			$this->data['redirect'] = (Input::get('redirect') ? base64_decode(Input::get('redirect')) : '');
			$this->data['title'] = l('Авторизация в %s', App::getName());
			$this->data['enable_app'] = $base_model->check_app_enable();
			View::display('base/login.twig.php', $this->data);
		}
	}

	/**
	 * POST login
	 */
	public function do_login() {
		$base_model = new Base_model();
		$redirect = Input::post('redirect');
		$url_admin = App::urlFor('admin');
		$redirect = empty($redirect) ? $url_admin : $redirect;

		$is_openid_auth = (int) Input::post('openid_auth');
		$is_oauth_auth = (int) Input::post('oauth_auth');
		try {
			if ($is_openid_auth == 0 && $is_oauth_auth == 0) {
				$error_msg = l('Неизвестный метод авторизации');
				throw new \ErrorException($error_msg);
			}
			if ($base_model->check_app_enable() === FALSE) {
				$error_msg = l('Приложение отключено');
				throw new \ErrorException($error_msg);
			}
			if ($is_openid_auth == 1) {
				$login_openid_url = App::urlFor('login_openid');
				if (empty($login_openid_url)) {
					$error_msg = l('Отсутствует метод для авторизации через OpenID');
					throw new \ErrorException($error_msg);
				}
				$openid_model = Openid_model::getInstance();
				$openid_identity = Settings_model::get_setting_by_key('openid_identity');
				if ($openid_identity !== FALSE) {
					$openid_model->openid_set_config('identity', $openid_identity);
				}
				$openid = $openid_model->openid_get_login(array('return_url' => Request::getUrl() . $login_openid_url));
				Response::redirect($openid->authUrl());
			}
			if ($is_oauth_auth == 1) {
				$url_login_oauth = App::urlFor('login_oauth');
				Response::redirect($url_login_oauth);
			}
		} catch(\Exception $e) {
			App::flash('message', $e->getMessage());
			App::flash('redirect', $redirect);
			$url_login = App::urlFor('login');
			Response::redirect($url_login);
		}
	}

	/**
	 * Авторизация через openID
	 *
	 */
	public function login_openid() {
		$base_model = new Base_model();
		$redirect = Input::post('redirect');
		$url_login = App::urlFor('login');
		if (empty($redirect)) {
			$redirect = $url_login;
		}
		try {
			$openid_model = Openid_model::getInstance();
			if ($openid_model->openid_get_mode() == 'cancel') {
				throw new \ErrorException(l('Аутентификация была прервана'));
			}
			if ($openid_model->openid_validate()) {
				$user_attributes = $openid_model->openid_get_attributes();
				if (empty($user_attributes['contact/email'])) {
					throw new \ErrorException(l('Не переданы необходимы данные для авторизации'));
				}
				$user_auth_params = array(
					'email' => $user_attributes['contact/email'],
					'group' => 'company',
				);
				$ret = $base_model->set_user_auth($user_auth_params);
				if ($ret === FALSE) {
					$error_msg = l('Не удалось авторизовать пользователя');
					throw new Exception($error_msg);
				}
				Response::redirect($redirect);
			} else {
				$error_msg = l('Не удалось выполнить валидацию');
				throw new \ErrorException($error_msg);
			}
		} catch(\Exception $e) {
			App::flash('message', $e->getMessage());
			App::flash('redirect', $redirect);
			Response::redirect($url_login);
		}
	}

	public function login_oauth() {
		$oauth_step = 'get_token';
		$base_model = new Base_model();

		$oauth_redirect_url = Request::getUrl() . Request::getPath();
		$oauth_response_type = 'token';
		$provider_params = array(
			'clientId' => (string) constant('OAUTH_CLIENT_ID'),
			'redirectUri' => $oauth_redirect_url,
		);

		$provider = new OAuth2\Client\Provider\Reports($provider_params);

		$is_oauth_auth = (int) Input::post('oauth_auth');
		$post_access_token = Input::post('access_token');

		$user_is_auth = $base_model->user_is_auth();

		// Если сотрудник уже авторизирован, редиректим на страницу логина
		if ($user_is_auth === TRUE) {
			$url_login = App::urlFor('login');
			Response::redirect($url_login);
		}
		$get_error = Input::get('error');

		if ($get_error == 'access_denied') {
			$error_msg = Input::get('error_description');
			App::flash('message', l('Возникла ошибка при авторизации через oauth-сервис: %s', $error_msg));
			$url_login = App::urlFor('login');
			Response::redirect($url_login);
		}

		if ($is_oauth_auth == 1 && empty($post_access_token)) {
			$provider->authorize(array('response_type' => $oauth_response_type));
			App::stop();
		}
		if (!empty($post_access_token)) {
			try {
				$get_user_details = $provider->getUserDetails($post_access_token);
				if ($get_user_details === FALSE) {
					$error_msg = l('Не удалось получить информацию о пользователе');
					throw new Exception($error_msg);
				}

				$user_auth_params = array(
					'email' => $get_user_details['email'],
					'group' => $get_user_details['role'],
				);
				$ret = $base_model->set_user_auth($user_auth_params);
				if ($ret === FALSE) {
					$error_msg = l('Не удалось авторизовать пользователя');
					throw new Exception($error_msg);
				}
			} catch (Exception $e) {
				// Failed to get access token
				App::flash('message', $e->getMessage());
			}
			$url_login = App::urlFor('login');
			Response::redirect($url_login);
		}
		$this->data['oauth_step'] = $oauth_step;
		View::display('base/login_oauth.twig.php', $this->data);
	}

	/**
	 * Вылогиниваем
	 *
	 */
	public function logout() {
		Base_model::logout();
		$main_url = Request::getRootUri();
		Response::redirect($main_url);
	}

}
