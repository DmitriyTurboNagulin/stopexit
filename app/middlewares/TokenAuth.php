<?php

use \App;
use \Config;

class TokenAuth extends \Slim\Middleware {

	protected $route;
	protected $ignore_route;

	public function __construct($route, $ignore_route = '') {
		$this->route = rtrim($route, '/');
		$ignore_route = (array) $ignore_route;
		$this->ignore_route = $ignore_route;
		$this->call();
	}

	public function call() {
		$current_route = rtrim(App::request()->getPathInfo(), '/');
		if (strpos($current_route, $this->route) !== FALSE && !in_array($current_route, $this->ignore_route)) {
			$this->check_token();
		}
	}

	/**
	 * Deny Access
	 *
	 */
	public function deny_access() {
		throw new Exception('token not valid', 401);
	}

	/**
	 * Check against the DB if the token is valid
	 * 
	 * @param string $token
	 * @return bool
	 */
	public function authenticate($token) {
		if (empty($token)) {
			return FALSE;
		}
		if ($token != INSTALL_API_TOKEN) {
			return FALSE;
		}
		return TRUE;
	}

	public function check_token() {
		$token = App::request()->headers->get('X-Authorization');
		//Check if our token is valid
		if ($this->authenticate($token) === FALSE) {
			$this->deny_access();
		}
	}

}
