<?php
/**
 * jsonAPI - Slim extension to implement fast JSON API's
 *
 * @package Slim
 * @subpackage Middleware
 * @author Jonathan Tavares <the.entomb@gmail.com>
 * @license GNU General Public License, version 3
 * @filesource
 *
 *
*/
use \App;
use \View;
/**
 * JsonApiMiddleware - Middleware that sets a bunch of static routes for easy bootstrapping of json API's
 *
 * @package Slim
 * @subpackage View
 * @author Jonathan Tavares <the.entomb@gmail.com>
 * @license GNU General Public License, version 3
 * @filesource
 */
class JsonApiMiddleware extends \Slim\Middleware {

	function __construct() {
		App::config('debug', FALSE);

		// Generic error handler
		App::error(function (Exception $e) {
			if ($e->getCode()) {
				$errorCode = $e->getCode();
			} else {
				$errorCode = 500;
			}
			View::reset();
			App::render($errorCode, array(
				'error' => TRUE,
				'error_message' => \JsonApiMiddleware::_errorType($errorCode) . ": " . $e->getMessage(),
			));
		});

		// Not found handler (invalid routes, invalid method types)
		App::notFound(function() {
			App::render(404, array('error' => TRUE, 'error_message' => 'Invalid route'));
		});

		// Handle Empty response body
		App::hook('slim.after.router', function () {
			//Fix sugested by: https://github.com/bdpsoft
			//Will allow download request to flow
			if (App::response()->header('Content-Type') === 'application/octet-stream') {
				return;
			}

			if (strlen(App::response()->body()) == 0) {
				App::render(500, array('error' => TRUE, 'error_message' => 'Empty response'));
			}
		});
	}

	function call() {

	}

	static function _errorType($type = 1) {
		switch($type) {
			default:
			case E_ERROR: // 1 //
				return 'ERROR';
			case E_WARNING: // 2 //
				return 'WARNING';
			case E_PARSE: // 4 //
				return 'PARSE';
			case E_NOTICE: // 8 //
				return 'NOTICE';
			case E_CORE_ERROR: // 16 //
				return 'CORE_ERROR';
			case E_CORE_WARNING: // 32 //
				return 'CORE_WARNING';
			case E_CORE_ERROR: // 64 //
				return 'COMPILE_ERROR';
			case E_CORE_WARNING: // 128 //
				return 'COMPILE_WARNING';
			case E_USER_ERROR: // 256 //
				return 'USER_ERROR';
			case E_USER_WARNING: // 512 //
				return 'USER_WARNING';
			case E_USER_NOTICE: // 1024 //
				return 'USER_NOTICE';
			case E_STRICT: // 2048 //
				return 'STRICT';
			case E_RECOVERABLE_ERROR: // 4096 //
				return 'RECOVERABLE_ERROR';
			case E_DEPRECATED: // 8192 //
				return 'DEPRECATED';
			case E_USER_DEPRECATED: // 16384 //
				return 'USER_DEPRECATED';
		}
	}

}
