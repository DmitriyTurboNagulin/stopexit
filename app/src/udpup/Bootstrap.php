<?php

namespace Udpup;

use \Illuminate\Database\Capsule\Manager as Capsule;
use \udpup\Module\Manager as ModuleManager;
use \udpup\Menu\MenuManager;
use \udpup\Copyright\CopyrightManager;
use \SlimFacades\Facade;
use \Illuminate\Events\Dispatcher;

use \Illuminate\Cache\CacheManager;
use \Illuminate\Filesystem\Filesystem;

use \Main;
use \Config;

if (!defined('APP_PATH')) exit('No direct script access allowed');

/**
 * udpup Bootstrapper, initialize all the thing needed on the start
 */
class Bootstrap {

	private $app;

	private $config;

	/**
	 * udpup Bootstrap constructor
	 * @param \Slim\Slim $app
	 */
	public function __construct(\Slim\Slim $app = NULL) {
		$this->app = $app;
	}

	/**
	 * Setup udpup configuration and inject it to Slim instance
	 * @param array $config
	 */
	public function setConfig($config) {
		$this->config = $config;
		foreach ($config as $key => $value) {
			$this->app->config($key, $value);
		}
	}

	/**
	 * Setting up slim instance for slim starter
	 * @param SlimSlim $app [description]
	 */
	public function setApp(\Slim\Slim $app) {
		$this->app = $app;
	}

	/**
	 * Boot up Slim Facade accessor
	 * @param  Array $config
	 */
	public function bootFacade($config) {
		Facade::setFacadeApplication($this->app);
		Facade::registerAliases($config);
	}

	/**
	 * Boot up Eloquent ORM and inject to Slim container
	 */
	public function bootEloquent() {
		$is_debug = Config::get('debug');
		try{

			$config_name = Main::get_database_name_file();
			$config = array();
			if (!file_exists(APP_CONFIG_PATH . $config_name)) {
				throw new \Exception('Application not installed, run install script');
			}
			$config = require_once (APP_CONFIG_PATH . $config_name);
			if (empty($config['default']) || empty($config['connections'][$config['default']])) {
				throw new \Exception('database config is corrupted');
			}
			$db = $config['connections'][$config['default']];
			$db = Main::pre_make_connection_database($db);
			$this->app->container->singleton('db', function() {
				return new Capsule();
			});
			$this->app->db->addConnection($db);
			$dispatcher = new Dispatcher();
			$this->app->db->setEventDispatcher($dispatcher);
			$this->app->db->setAsGlobal();
			$this->app->db->bootEloquent();

			if ($is_debug === TRUE) {
				$dispatcher->listen('illuminate.query', function($sql, $params, $time, $conn) {
					$this->app->log->debug($sql . ':' . implode(' ', $params));
				});
			}
		}catch(\Exception $e) {
			die($e->getMessage());
		}
	}

	/**
	 * Boot up Twig template engine
	 * @param  Array $config
	 */
	public function bootTwig($config) {
		$app = $this->app;
		$view = $app->view;

		$view->parserOptions = $config;
		$view->parserExtensions = array(
			new \Slim\Views\TwigExtension(),
			new \udpup\TwigExtension\MenuRenderer(),
			new \udpup\TwigExtension\CopyrightRenderer(),
			new \udpup\TwigExtension\I18n(),
		);
	}

	/**
	 * Boot up Menu Manager
	 */
	public function bootMenuManager() {
		$this->app->container->singleton('menu', function() {
			return new MenuManager();
		});
	}

	/**
	 * Run the boot sequence
	 * @return void
	 */
	public function boot() {
		$this->bootFacade($this->config['aliases']);
		$this->bootMenuManager();
		$this->bootCopyrightManager();
		$this->bootEloquent();
		$this->bootTwig($this->config['twig']);
		$this->bootCache();
	}

	/**
	 * Run the Slim application
	 */
	public function run() {
		$this->app->run();
	}

	public function bootCache() {
		$this->app->container->singleton('cache', function() {
			$config = Config::get('cache');
			$cache_manager_config = array(
				'files' => new \Illuminate\Filesystem\Filesystem(),
				'config' => $config,
			);
			$cache_manager = new \Illuminate\Cache\CacheManager($cache_manager_config);
			$cache = $cache_manager->driver();
			return $cache;
		});
	}

	/**
	 * Boot up Copyright Manager
	 */
	public function bootCopyrightManager() {
		$this->app->container->singleton('copyright', function() {
			return new CopyrightManager();
		});
	}

}
