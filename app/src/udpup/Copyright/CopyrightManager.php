<?php

namespace udpup\Copyright;
use \Base_model;

class CopyrightManager {
	protected $menuCollection;

	public function __construct() {
	}

	public function render($params = array()) {
		return Base_model::get_copyright();
	}

}
