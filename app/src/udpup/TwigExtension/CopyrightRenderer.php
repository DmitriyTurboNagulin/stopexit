<?php

namespace udpup\TwigExtension;
use \Slim;

class CopyrightRenderer extends \Twig_Extension {

	public function getName() {
		return 'сopyright_renderer';
	}

	public function getFunctions() {
		return array(
			new \Twig_SimpleFunction('render_copyright', array($this, 'render_copyright'))
		);
	}

	public function render_copyright($params = array()) {
		$app = Slim::getInstance();
		return $app->copyright->render($params);
	}

}
