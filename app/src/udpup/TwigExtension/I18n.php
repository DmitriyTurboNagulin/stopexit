<?php

namespace udpup\TwigExtension;
use \Slim;

class I18n extends \Twig_Extension {

    public function getName() {
        return 'I18n';
    }

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('l', 'l')
        );
    }

}
