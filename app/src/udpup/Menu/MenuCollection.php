<?php

namespace udpup\Menu;

use \Illuminate\Support\Collection;

class MenuCollection extends Collection{
	protected $active;
	protected $name;

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setActiveMenu($menu) {
		$this->active = $menu;

		foreach ($this->items as $item) {
			$this->seekAndActivate($item, $menu);
		}
	}

	protected function seekAndActivate(\udpup\Menu\MenuItem $item, $menu) {
		if ($item->getName() == $menu) {
			$item->setActive(TRUE);
		} elseif ($item->hasChildren()) {
			foreach ($item->getChildren() as $child) {
				$this->seekAndActivate($child, $menu);
			}
		} else {
			$item->setActive(FALSE);
		}
	}

	public function getActiveMenu() {
		return $this->active;
	}

	/**
	 * Add new item to menuCollection
	 * @param udpup\Menu\MenuItem $item
	 * @param String $menu
	 */
	public function addItem($name, \udpup\Menu\MenuItem $item) {
		$this->items[$name] = $item;
	}

	public function getItem($name) {
		return isset($this->items[$name]) ? $this->items[$name] : NULL;
	}

	/**
	 * MenuItem factory
	 * @param  String $label
	 * @param  String $url
	 * @return MenuItem
	 */
	public function createItem($name, $option) {
		return new MenuItem($name, $option);
	}

}
