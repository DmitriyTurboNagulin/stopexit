<?php

namespace udpup\Facade;

class ModuleManagerFacade extends \SlimFacades\Facade{

	protected static function getFacadeAccessor() { return 'module'; }

}
