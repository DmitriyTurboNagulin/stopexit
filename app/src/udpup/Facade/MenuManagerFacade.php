<?php

namespace udpup\Facade;

class MenuManagerFacade extends \SlimFacades\Facade{

	protected static function getFacadeAccessor() { return 'menu'; }

}
