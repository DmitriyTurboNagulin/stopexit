<?php

namespace udpup\Facade;

class RouteFacade extends \SlimFacades\Route{

	/**
	 * Map route to all public controller method
	 *
	 * with
	 * Route::get('/prefix', 'ClassController')
	 *
	 * this will map
	 * GET  domain.com/prefix -> ClassController::getIndex
	 * POST domain.com/prefix -> ClassController::postIndex
	 * PUT  domain.com/prefix -> ClassController::putIndex
	 */
	public static function controller() {
		$arguments  = func_get_args();
		$path	   = $arguments[0];
		$controller = end($arguments);

		$class	  = new \ReflectionClass($controller);
		$controllerMethods = $class->getMethods(\ReflectionMethod::IS_PUBLIC);
		$uppercase  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

		foreach ($controllerMethods as $method) {
			if (substr($method->name, 0, 2) != '__') {
				$methodName = $method->name;
				$callable   = $arguments;

				$pos		= strcspn($methodName, $uppercase);
				$httpMethod = substr($methodName, 0, $pos);
				$ctrlMethod = lcfirst(strpbrk($methodName, $uppercase));

				if ($ctrlMethod == 'index') {
					$pathMethod = $path;
				} elseif ($httpMethod == 'get') {
					$pathMethod = "{$path}/{$ctrlMethod}(/(:params+))";
				} else {
					$pathMethod = "{$path}/{$ctrlMethod}(/)";
				}

				//put edited pattern to the top stack
				array_shift($callable);
				array_unshift($callable, $pathMethod);

				//put edited controller to the bottom stack
				array_pop($callable);
				array_push($callable, "{$controller}:{$methodName}");
				call_user_func_array(array(self::$slim, $httpMethod), $callable);
			}
		}
	}

}
