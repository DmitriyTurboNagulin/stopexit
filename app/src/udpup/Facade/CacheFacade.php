<?php

namespace udpup\Facade;

use \Crypt;

class CacheFacade extends \SlimFacades\Facade {

	protected static function getFacadeAccessor() { return 'cache'; }

	public static function crypt_put($key, $value, $minutes) {
		$value_encoded = Crypt::encode(serialize($value));
		self::put($key, $value_encoded, $minutes);
	}

	public static function crypt_get($key) {
		$value = self::get($key);
		if (!is_null($value)) {
			$value = unserialize(Crypt::decode($value));
		}
		return $value;
	}

}
