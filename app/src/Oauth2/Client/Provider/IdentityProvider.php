<?php

namespace OAuth2\Client\Provider;

abstract class IdentityProvider {

	public $clientId = '';

	public $clientSecret = '';

	public $redirectUri = '';

	public $name;

	public $uidKey = 'uid';

	public $scopes = array();

	public $method = 'post';

	public $scopeSeperator = ',';

	public $responseType = 'json';

	protected $cachedUserDetailsResponse;

	public function __construct($options = array())
	{
		foreach ($options as $option => $value) {
			if (isset($this->{$option})) {
				$this->{$option} = $value;
			}
		}
	}

	abstract public function urlAuthorize();

	abstract public function urlAccessToken();

	abstract public function urlUserDetails();

	abstract public function userDetails($response, $token);

	public function getScopes() {
		return $this->scopes;
	}

	public function setScopes(array $scopes) {
		$this->scopes = $scopes;
	}

	public function getAuthorizationUrl($options = array()) {
		$state = md5(uniqid(rand(), TRUE));
		setcookie($this->name . '_authorize_state', $state);

		$params = array(
			'client_id' => $this->clientId,
			'redirect_uri' => $this->redirectUri,
			'state' => $state,
			'scope' => is_array($this->scopes) ? implode($this->scopeSeperator, $this->scopes) : $this->scopes,
			'response_type' => isset($options['response_type']) ? $options['response_type'] : 'code',
			'approval_prompt' => 'force' // - google force-recheck
		);
		$url_part = $this->urlAuthorize();
		return $url_part . '?' . http_build_query($params);
	}

	public function authorize($options = array()) {
		$auth_url = $this->getAuthorizationUrl($options);
		header('Location: ' . $auth_url);
		exit;
	}

	public function getUserDetails($token, $force = FALSE) {
		$response = $this->fetchUserDetails($token);
		return $this->userDetails(json_decode($response), $token);
	}

	public function getUserEmail(AccessToken $token, $force = FALSE) {
		$response = $this->fetchUserDetails($token, $force);
		return $this->userEmail(json_decode($response), $token);
	}

	public function getUserScreenName($token, $force = FALSE) {
		$response = $this->fetchUserDetails($token, $force);
		return $this->userScreenName(json_decode($response), $token);
	}

	protected function fetchUserDetails($token, $force = FALSE) {
		if (!$this->cachedUserDetailsResponse || $force == TRUE) {
			$url = $this->urlUserDetails();
			try {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_TIMEOUT, 3);
				curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
				curl_setopt($ch, CURLOPT_POSTFIELDS, 'access_token=' . $token);
				$curl_exec = curl_exec($ch);
				if ($curl_exec === FALSE) {
					throw new \Exception('Curl Error:' . curl_error($ch));
				}
				$this->cachedUserDetailsResponse = $curl_exec;
			} catch (Exception $e) {
				$error_msg = $e->getMessage();
				throw new \Exception($error_msg);
			}
		}
		return $this->cachedUserDetailsResponse;
	}

}
