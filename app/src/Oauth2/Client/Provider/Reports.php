<?php

namespace OAuth2\Client\Provider;

class Reports extends IdentityProvider {
	public $scopes = array('profile', 'email');

	public $scopeSeperator = ' ';

	public $responseType = 'json';

	public $method = 'post';

	private $all_role = array(
		'company',
		'client',
		'contact',
		'guest',
	);

	private $default_roles = 'guest';

	public function urlAuthorize() {
		return 'http://reports.netpeak.net/oauth';
	}

	public function urlAccessToken() {
		return 'http://reports.netpeak.net/oauth/access_token';
	}

	public function urlUserDetails() {
		return 'http://reports.netpeak.net/oauth/user_info';
	}

	public function userDetails($response, $token) {
		$response = $this->parse_response($response);
		if ($response === FALSE) {
			return FALSE;
		}
		$user = array();
		if (property_exists($response, 'name')) {
			$user['name'] = $response->name;
			$user['first_name'] = $user['name'];
		}
		if (property_exists($response, 'last_name')) {
			$user['last_name'] = $response->last_name;
		}
		if (property_exists($response, 'mail')) {
			$user['email'] = $response->mail;
		}
		if (property_exists($response, 'role')) {
			$user_role = $response->role;
			if (!in_array($user_role, $this->all_role)) {
				$user_role = $this->default_roles;
			}
			$user['role'] = $user_role;
		}
		return $user;
	}

	private function parse_response($response) {
		if (property_exists($response, 'success') && $response->success === TRUE && property_exists($response, 'response')) {
			return $response->response;
		}
		return FALSE;
	}

	public function userEmail($response, $token) {
		$email = NULL;
		$response = $this->parse_response($response);
		if ($response === FALSE) {
			return $email;
		}
		if (property_exists($response, 'mail')) {
			$email = $response->mail;
		}
		return $email;
	}

	public function userScreenName($response, $token) {
		$full_name = NULL;
		$response = $this->parse_response($response);
		if ($response === FALSE) {
			return $full_name;
		}
		$tmp = array();
		if (property_exists($response, 'name')) {
			$tmp[] = $response->name;
		}
		if (property_exists($response, 'last_name')) {
			$tmp[] = $response->last_name;
		}
		$tmp = array_filter($tmp, 'strlen');
		return implode(' ', $tmp);
	}

}
