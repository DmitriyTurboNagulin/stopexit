<?php

date_default_timezone_set('Europe/Kiev');
ini_set('date.timezone', 'Europe/Kiev');

header('Content-Type: text/html; charset=utf-8');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", FALSE);
header("Pragma: no-cache");

// Define directory
define('ROOT_PATH' , __DIR__ . '/../../');
define('PUBLIC_PATH', __DIR__ . '/../../');
define('VENDOR_PATH', ROOT_PATH . 'vendor/');
define('APP_PATH' , ROOT_PATH . 'app/');
define('APP_CONFIG_PATH' , APP_PATH . 'config/');
define('STORAGE_PATH' , ROOT_PATH . 'storage/');
define('ROUTE_PATH', APP_PATH . 'routes/');

$tags = array(
	'language' => '{%LANGUAGE%}',
	'app_name' => '{%APP_NAME%}',
	'app_secret' => '{%CRYPT_SECRET%}',
	'install_expire_date_ymd' => '{%INSTALL_EXPIRE_DATE_YMD%}',
	'install_api_token' => '{%INSTALL_API_TOKEN%}',
	'oauth_client_id' => '{%OAUTH_CLIENT_ID%}',
);

if (empty($_ENV['SLIM_MODE'])) {
	// Если необходимо включить дебаг, то прописываем в index.php: $_ENV['SLIM_MODE'] = 'development';
	// Либо по кошерному в виртуальный хост apache2: SetEnv SLIM_MODE development
	$_ENV['SLIM_MODE'] = (getenv('SLIM_MODE')) ? getenv('SLIM_MODE') : 'production';
}
define('SLIM_MODE', $_ENV['SLIM_MODE']);
if (SLIM_MODE == 'development' && file_exists(ROOT_PATH . 'develop/debug.php')) {
	// При дебаге подключаем конфиг для отладки
	$tags = require_once (ROOT_PATH . 'develop/debug.php');
}

define('LANG' , $tags['language']);
define('APP_NAME' , $tags['app_name']);
define('APP_SECRET' , $tags['app_secret']);
define('INSTALL_EXPIRE_DATE_YMD' , $tags['install_expire_date_ymd']);
define('INSTALL_API_TOKEN' , $tags['install_api_token']);
define('OAUTH_CLIENT_ID' , $tags['oauth_client_id']);
$tags = NULL;

$app_session_name = substr(md5(INSTALL_EXPIRE_DATE_YMD . APP_NAME), 0, 16);

mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
session_cache_limiter(FALSE);
session_start();
session_name($app_session_name);

require_once (VENDOR_PATH . 'autoload.php');

$config = array(
	'path.root' => ROOT_PATH,
	'path.public' => PUBLIC_PATH,
	'path.app' => APP_PATH,
);
if (file_exists(APP_CONFIG_PATH . 'config.php') === FALSE) {
	die('Not found config.php');
}
$external_config = require_once (APP_CONFIG_PATH . 'config.php');
if (!is_array($external_config) || empty($external_config)) {
	die('config app is corrupted');
}

$config = array_merge($config, $external_config);
$external_config = NULL;

/** Merge cookies config to slim config */
if (isset($config['cookies'])) {
	$config['cookies']['secret'] = APP_SECRET;
	$config['cookies']['cipher'] = MCRYPT_RIJNDAEL_256;
	$config['cookies']['cipher_mode'] = MCRYPT_MODE_CBC;
	foreach ($config['cookies'] as $key => $value) {
		$config['slim']['cookies.' . $key] = $value;
	}
}

$config['crypt.size'] = 256;

if (isset($config['log'])) {
	$log_writer = new \Slim\Extras\Log\DateTimeFileWriter($config['log']);
	$config['slim']['log.writer'] = $log_writer;
}
$log_writer = NULL;
if (SLIM_MODE == 'development') {
	// Включаем отображение ошибок
	if (isset($config['slim'])) {
		$config['slim']['debug'] = TRUE;
	}
	// Отключаем кэшированием вьюх твигом
	if (isset($config['twig'])) {
		$config['twig']['debug'] = TRUE;
	}
}

// Initialize Slim and udpup application
$app = new \Slim\Slim($config['slim']);
$app->setName(APP_NAME);

$template_error_data = array(
	'error_msg' => 'Page not Found',
	'css' => array(
		'internal' => array('font-awesome.min.css', 'custom.css'),
		'plugins' => array('bootstrap/dist/css/bootstrap.min.css'),
	),
);

$app->error(function(\Exception $e) use ($template_error_data) {
	$template_error_data['error_msg'] = $e->getMessage();
	View::display('base/error.twig.php', $template_error_data);
});
$app->notFound(function () use ($template_error_data) {
	View::display('base/error.twig.php', $template_error_data);
});
$template_error_data = NULL;

$framework_location = Main::framework_location();
$base_urls = array(
	'framework_base' => $framework_location,
	'assetUrl' => $framework_location . 'assets/',
);
$app->view()->appendData($base_urls);
$base_urls = NULL;

unset($GLOBALS);
$starter = new \udpup\Bootstrap();

if (method_exists($starter, 'setApp') === FALSE) {
	die('Bootstrap file is corrupted');
}

$starter->setApp($app);
$starter->setConfig($config);

$config_name = Main::get_database_name_file();
if (file_exists(APP_CONFIG_PATH . $config_name)) {
	$starter->boot();
	require_once (APP_PATH . 'routes.php');
} else {
	$starter->bootFacade($config['aliases']);
	Route::get('/', 'InstallController:index');
	Route::post('/db/check(/)', 'InstallController:checkConnection');
	Route::post('/db/configure(/)', 'InstallController:writeConfiguration');
}

return $starter;
