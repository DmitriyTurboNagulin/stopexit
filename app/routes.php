<?php if (!defined('APP_PATH')) exit('No direct script access allowed');

foreach (glob(ROUTE_PATH . '*_route.php') as $route_file) {
	require_once($route_file);
}

/** default routing */
Route::get('/', 'HomeController:index');
Route::post('/show(/)', 'HomeController:show');
Route::post('/phone(/)', 'HomeController:phone');
Route::post('/proceed(/)', 'HomeController:proceed');
Route::post('/offerCTR(/)', 'HomeController:offerCTR');
Route::get('/js(/)', 'HomeController:js');
Route::get('/login(/)', 'HomeController:login')->name('login');
Route::get('/login/openid(/)', 'HomeController:login_openid')->name('login_openid');

Route::post('/login/oauth(/)', 'HomeController:login_oauth');
Route::get('/login/oauth(/)', 'HomeController:login_oauth')->name('login_oauth');

Route::get('/logout(/)', 'HomeController:logout')->name('logout');
Route::post('/login(/)', 'HomeController:do_login');
Route::get('/update(/)', 'InstallController:update')->name('update');
