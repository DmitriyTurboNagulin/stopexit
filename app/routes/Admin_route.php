<?php if (!defined('APP_PATH')) exit('No direct script access allowed');

Route::group(
	'/admin',
	function() {
		if (Base_model::user_is_auth() === FALSE) {
			if (Request::isAjax()) {
				Response::headers()->set('Content-Type', 'application/json');
				$json = json_encode(array(
					'success' => FALSE,
					'message' => 'Session expired or unauthorized access.',
					'code' => 401,
				));
				Response::setBody($json);
			} else {
				$redirect = Request::getResourceUri();
				Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
			}
			App::stop();
		}
	},
	function() {
		Route::get('/', 'Admin\AdminController:getIndex')->name('admin');
		Route::controller('(/)', 'Admin\AdminController');
	}
);
