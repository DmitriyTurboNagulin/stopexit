<?php if (!defined('APP_PATH')) exit('No direct script access allowed');

Route::group(
	'/api',
	function() {
		$token_auth_white_list = array('/api');
		App::view(new \JsonApiView());
		App::add(new \JsonApiMiddleware());
		App::add(new \TokenAuth('/api', $token_auth_white_list));
	},
	function(){
		Route::controller('(/)', 'Api\ApiController');
	}
);
