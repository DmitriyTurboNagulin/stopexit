<?php
use Illuminate\Database\Capsule\Manager as DB;
use \Settings_model;
use \Main;

class m2 extends Model {

	public function up() {
		try {
			DB::beginTransaction();
			$settings_model = new Settings_model();
			$settings_model->setting_key = 'reciever_email';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'sender_name';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'sender_email';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'smtp_status';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'smtp_server';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'smtp_port';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'smtp_login';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'smtp_password';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'sms_status';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'sms_service';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'sms_reciever';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'sms_alphaname';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'sms_login';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'sms_pass';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'sms_api';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'sms_test';
			$settings_model->setting_value = '1';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'call_mask';
			$settings_model->setting_value = '(999) 999-99-99';
			$settings_model->save();

			\Helper_model::setFKCheckOff();
			DB::schema()->dropIfExists('offer');
			DB::schema()->dropIfExists('offer_setting');
			DB::schema()->dropIfExists('offer_template');
			DB::schema()->dropIfExists('offer_stat');
			DB::schema()->dropIfExists('call');
			DB::schema()->dropIfExists('call_setting');
			DB::schema()->dropIfExists('call_template');
			DB::schema()->dropIfExists('phone');
			DB::schema()->dropIfExists('anchor');
			DB::schema()->dropIfExists('anchor_settings');
			DB::schema()->dropIfExists('logs');
			\Helper_model::setFKCheckOn();

			if (DB::schema()->hasTable('logs') === FALSE) {
				DB::schema()->create('logs', function($table) {
					$table->increments('id');
					$table->string('user', 100);
					$table->string('event', 50);
					$table->string('new', 255)->nullable();
					$table->string('old', 255)->nullable();
					$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
					$table->engine = 'InnoDB';
				});
			}

			if (DB::schema()->hasTable('anchor') === FALSE) {
				DB::schema()->create('anchor', function($table) {
					$table->increments('anchor_id');
					$table->string('anchor_name', 150);
					$table->softDeletes();
					$table->nullableTimestamps();
					$table->engine = 'InnoDB';
				});
			}

			if (DB::schema()->hasTable('offer') === FALSE) {
				DB::schema()->create('offer', function($table) {
					$table->increments('offer_id')->unsigned();
					$table->string('offer_name', 150);
					$table->integer('offer_show_num');
					$table->integer('offer_yes_click');
					$table->integer('offer_no_click');
					$table->softDeletes();
					$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
					$table->engine = 'InnoDB';
				});
			}

			if (DB::schema()->hasTable('offer_setting') === FALSE) {
				DB::schema()->create('offer_setting', function($table) {
					$table->increments('offer_setting_id');
					$table->string('offer_setting_email', 255);
					$table->string('offer_setting_text', 255)->nullable();
					$table->integer('offer_setting_text_size')->nullable();
					$table->string('offer_setting_logo', 255)->nullable();
					$table->string('offer_setting_company', 255)->nullable();
					$table->integer('offer_setting_company_size')->nullable();
					$table->string('offer_setting_free', 50)->nullable();
					$table->integer('offer_setting_free_size')->nullable();
					$table->string('offer_setting_yes', 50)->nullable();
					$table->string('offer_setting_yes_link', 255)->nullable();
					$table->integer('offer_setting_yes_size')->nullable();
					$table->string('offer_setting_no', 50)->nullable();
					$table->string('offer_setting_background', 7)->nullable();
					$table->string('offer_setting_background_image', 255)->nullable();
					$table->string('offer_setting_background_size', 10)->nullable();
					$table->string('offer_setting_background_btn', 7)->nullable();
					$table->string('offer_setting_color', 7)->nullable();
					$table->string('offer_setting_color_btn', 7)->nullable();
					$table->string('offer_setting_border_type', 20)->nullable();
					$table->string('offer_setting_border_color', 7)->nullable();
					$table->string('offer_setting_border_color2', 7)->nullable();
					$table->integer('offer_setting_width')->unsigned();
					$table->integer('offer_setting_height')->unsigned();
					$table->string('offer_setting_event', 20)->nullable();
					$table->integer('offer_setting_x_sec')->nullable();
					$table->string('offer_setting_position', 20)->nullable();
					$table->string('offer_setting_effect', 20)->nullable();
					$table->text('offer_setting_templates')->nullable();
					$table->text('offer_setting_templates_white')->nullable();
					$table->text('offer_setting_templates_black')->nullable();
					$table->string('offer_setting_domain', 255)->nullable();
					$table->text('offer_setting_days')->nullable();
					$table->dateTime('offer_setting_start')->nullable();
					$table->dateTime('offer_setting_stop')->nullable();
					$table->string('offer_setting_view', 20)->nullable();
					$table->integer('offer_setting_view_x_days')->nullable();
					$table->integer('offer_id')->unsigned();
					$table->integer('anchor_id')->unsigned()->nullable();
					$table->softDeletes();
					$table->nullableTimestamps();
					$table->foreign('anchor_id')->references('anchor_id')->on('anchor')->onDelete('cascade')->onUpdate('cascade');
					$table->foreign('offer_id')->references('offer_id')->on('offer')->onDelete('cascade')->onUpdate('cascade');
					$table->engine = 'InnoDB';
				});
			}

			if (DB::schema()->hasTable('call') === FALSE) {
				DB::schema()->create('call', function($table) {
					$table->increments('call_id');
					$table->string('call_name', 150);
					$table->integer('call_show_num');
					$table->integer('call_yes_click');
					$table->softDeletes();
					$table->nullableTimestamps();
					$table->engine = 'InnoDB';
				});
			}

			if (DB::schema()->hasTable('phone') === FALSE) {
				DB::schema()->create('phone', function($table) {
					$table->increments('phone_id');
					$table->string('phone_number', 100);
					$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
					$table->engine = 'InnoDB';
					$table->integer('call_id')->unsigned()->nullable();
					$table->foreign('call_id')->references('call_id')->on('call')->onDelete('cascade')->onUpdate('cascade');
				});
			}

			if (DB::schema()->hasTable('call_setting') === FALSE) {
				DB::schema()->create('call_setting', function($table) {
					$table->increments('call_setting_id');
					$table->string('call_setting_email', 255);
					$table->string('call_setting_text', 255)->nullable();
					$table->integer('call_setting_text_size')->nullable();
					$table->string('call_setting_logo', 255)->nullable();
					$table->string('call_setting_yes', 50)->nullable();
					$table->string('call_setting_num_error', 50)->nullable();
					$table->integer('call_setting_yes_size')->nullable();
					$table->string('call_setting_color_btn', 7)->nullable();
					$table->string('call_setting_background', 7)->nullable();
					$table->string('call_setting_background_image', 255)->nullable();
					$table->string('call_setting_background_position', 10)->nullable();
					$table->string('call_setting_color', 7)->nullable();
					$table->string('call_setting_manger_name', 30)->nullable();
					$table->string('call_setting_manger_name_size', 3)->nullable();
					$table->string('call_setting_manager_photo', 255)->nullable();
					$table->string('call_setting_photo_position', 50)->nullable();
					$table->string('call_setting_border_type', 20)->nullable();
					$table->string('call_setting_border_color', 7)->nullable();
					$table->string('call_setting_border_color2', 7)->nullable();
					$table->integer('call_setting_width')->unsigned()->nullable();
					$table->integer('call_setting_height')->unsigned()->nullable();
					$table->string('call_setting_placeholder', 50)->nullable();
					$table->string('call_setting_background_btn', 7)->nullable();
					$table->string('call_setting_company', 255)->nullable();
					$table->string('call_setting_company_size', 10)->nullable();
					$table->string('call_setting_manager_position', 50)->nullable();
					$table->string('call_setting_radius', 3)->nullable();
					$table->string('call_setting_event', 20)->nullable();
					$table->integer('call_setting_x_sec')->nullable();
					$table->string('call_setting_position', 20)->nullable();
					$table->string('call_setting_effect', 20)->nullable();
					$table->text('call_setting_templates')->nullable();
					$table->text('call_setting_templates_white')->nullable();
					$table->text('call_setting_templates_black')->nullable();
					$table->string('call_setting_domain', 255)->nullable();
					$table->text('call_setting_days')->nullable();
					$table->dateTime('call_setting_start')->nullable();
					$table->dateTime('call_setting_stop')->nullable();
					$table->string('call_setting_view', 20)->nullable();
					$table->integer('call_setting_view_x_days')->nullable();
					$table->integer('anchor_id')->unsigned()->nullable();
					$table->integer('call_id')->unsigned();
					$table->softDeletes();
					$table->nullableTimestamps();
					$table->foreign('anchor_id')->references('anchor_id')->on('anchor')->onDelete('cascade')->onUpdate('cascade');
					$table->foreign('call_id')->references('call_id')->on('call')->onDelete('cascade')->onUpdate('cascade');
					$table->engine = 'InnoDB';
				});
			}

			if (DB::schema()->hasTable('call_template') === FALSE) {
				DB::schema()->create('call_template', function($table) {
					$table->increments('call_template_id');
					$table->string('call_template_name', 30)->nullable();
					$table->string('call_setting_background', 7)->nullable();
					$table->string('call_setting_background_image', 255)->nullable();
					$table->string('call_setting_background_btn', 7)->nullable();
					$table->string('call_setting_manager_position', 50)->nullable();
					$table->string('call_setting_radius', 3)->nullable();
					$table->string('call_setting_color', 7)->nullable();
					$table->string('call_setting_color_btn', 7)->nullable();
					$table->string('call_setting_border_type', 20)->nullable();
					$table->string('call_setting_border_color', 7)->nullable();
					$table->string('call_setting_border_color2', 7)->nullable();
					$table->string('call_setting_manager_photo', 255)->nullable();
					$table->string('call_setting_photo_position', 30)->nullable();
					$table->nullableTimestamps();
					$table->engine = 'InnoDB';
				});
			}

			if (DB::schema()->hasTable('offer_stat') === FALSE) {
				DB::schema()->create('offer_stat', function($table) {
					$table->increments('offer_stat_id');
					$table->integer('offer_views')->unsigned();
					$table->integer('offer_yes')->unsigned();
					$table->integer('offer_no')->unsigned();
					$table->integer('offer_id')->unsigned();
					$table->nullableTimestamps();
					$table->foreign('offer_id')->references('offer_id')->on('offer')->onDelete('cascade')->onUpdate('cascade');
					$table->engine = 'InnoDB';
				});
			}

			if (DB::schema()->hasTable('offer_template') === FALSE) {
				DB::schema()->create('offer_template', function($table) {
					$table->increments('offer_template_id');
					$table->string('offer_template_name', 30)->nullable();
					$table->string('offer_setting_background', 7)->nullable();
					$table->string('offer_setting_background_image', 255)->nullable();
					$table->string('offer_setting_background_btn', 7)->nullable();
					$table->string('offer_setting_color', 7)->nullable();
					$table->string('offer_setting_color_btn', 7)->nullable();
					$table->string('offer_setting_border_type', 20)->nullable();
					$table->string('offer_setting_border_color', 7)->nullable();
					$table->string('offer_setting_border_color2', 7)->nullable();
					$table->nullableTimestamps();
					$table->engine = 'InnoDB';
				});
			}

			if (DB::schema()->hasTable('anchor_settings') === FALSE) {
				DB::schema()->create('anchor_settings', function($table) {
					$table->increments('anchor_setting_id');
					$table->string('anchor_setting_email', 255);
					$table->string('anchor_setting_background', 7)->nullable();
					$table->string('anchor_setting_icon', 50)->nullable();
					$table->string('anchor_setting_size', 3)->nullable();
					$table->string('anchor_setting_icon_ref', 255)->nullable();
					$table->string('anchor_setting_icon_color', 7)->nullable();
					$table->string('anchor_setting_shadow', 50)->nullable();
					$table->string('anchor_setting_anim', 10)->nullable();
					$table->string('anchor_setting_anim_switch', 3)->nullable();
					$table->string('anchor_setting_opacity', 3)->nullable();
					$table->string('anchor_setting_border_radius', 3)->nullable();
					$table->string('anchor_setting_position', 20)->nullable();
					$table->string('anchor_setting_margin_vert', 3)->nullable();
					$table->string('anchor_setting_margin_horiz', 3)->nullable();
					$table->string('anchor_setting_back_enable', 3)->nullable();
					$table->integer('anchor_id')->unsigned();
					$table->foreign('anchor_id')->references('anchor_id')->on('anchor')->onDelete('cascade')->onUpdate('cascade');
					$table->nullableTimestamps();
					$table->softDeletes();
					$table->engine = 'InnoDB';
				});
			}
			DB::commit();
		} catch(\Exception $e) {
			exit($e->getMessage());
		}
	}

}
