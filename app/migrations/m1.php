<?php
use Illuminate\Database\Capsule\Manager as DB;
use \Settings_model;
use \Main;

class m1 extends Model {

	public function up() {
		try {
			DB::beginTransaction();
			DB::schema()->dropIfExists('settings');
			if (DB::schema()->hasTable('settings') === FALSE) {
				DB::schema()->create('settings', function($table) {
					$table->increments('setting_id');
					$table->string('setting_key', 255);
					$table->mediumText('setting_value');
					$table->nullableTimestamps();

					$table->engine = 'InnoDB';
					$table->unique('setting_key');
				});
			}
			$settings_model = new Settings_model();
			$settings_model->setting_key = 'client_access';
			$settings_model->setting_value = '0';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'migrations';
			$settings_model->setting_value = '0';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'expire_date';
			$settings_model->setting_value = (string) constant('INSTALL_EXPIRE_DATE_YMD');
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'openid_identity';
			$settings_model->setting_value = (string) base64_decode('aHR0cDovL2F1dGguYWtzLm9kLnVhLw==');
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'api_token';
			$settings_model->setting_value = (string) constant('INSTALL_API_TOKEN');
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'company_copyright';
			$settings_model->setting_value = '';
			$settings_model->save();

			$settings_model = new Settings_model();
			$settings_model->setting_key = 'manual';
			$settings_model->setting_value = '';
			$settings_model->save();
			DB::commit();
		} catch(\Exception $e) {
			exit($e->getMessage());
		}
	}

}
